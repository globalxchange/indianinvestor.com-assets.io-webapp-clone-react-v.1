import React, { createContext, useState, useContext, useEffect } from 'react';
import { BankContext } from './Context';
import Axios from 'axios';
import { APP_CODE } from '../config/appConfig';

export const VaultContext = createContext();

function VaultContextProvider({ children }) {
  const { profileId } = useContext(BankContext);
  const [coinBalanceList, setCoinBalanceList] = useState([]);
  const [vaultTxns, setVaultTxns] = useState([]);
  const [coinSelected, setCoinSelected] = useState({});
  const [coinAddress, setCoinAddress] = useState({});
  const [loading, setLoading] = useState(0);
  function updateBalance() {
    setLoading(0);
    Axios.post('https://comms.globalxchange.com/coin/vault/service/coins/get', {
      app_code: APP_CODE,
      profile_id: profileId,
      getAllCoins: true,
    })
      .then(({ data }) => {
        setCoinBalanceList(data.coins_data);
        const btcArray = data.coins_data.filter(
          (coin) => coin.coinSymbol === 'BTC'
        );
        if (!coinSelected) {
          setCoinSelected(btcArray[0]);
        }
      })
      .finally(() => setLoading((loading) => loading + 1));
    Axios.post(
      'https://comms.globalxchange.com/coin/vault/service/balances/get',
      {
        app_code: APP_CODE,
        profile_id: profileId,
        getAllCoins: true,
      }
    )
      .then(({ data }) => {
        if (data.status) {
          setCoinAddress(data.vault.coinAddress);
        }
      })
      .finally(() => setLoading((loading) => loading + 1));
    Axios.post('https://comms.globalxchange.com/coin/vault/service/txns/get', {
      app_code: APP_CODE,
      profile_id: profileId,
      getAllCoins: true,
    })
      .then(({ data }) => {
        if (data.status) setVaultTxns(data.txns);
      })
      .finally(() => setLoading((loading) => loading + 1));
  }
  useEffect(() => {
    if (profileId) {
      updateBalance();
    }
    // eslint-disable-next-line
  }, [profileId]);

  // For Vault Filter Conrolls

  const [menuTwo, setMenuTwo] = useState({
    key: 'all',
    value: 'All Directions',
  });

  const [dateSelected, setDateSelected] = useState(null);

  const [showNativeValue, setShowNativeValue] = useState(true);

  return (
    <VaultContext.Provider
      value={{
        coinBalanceList,
        coinSelected,
        setCoinSelected,
        vaultTxns,
        updateBalance,
        coinAddress,
        loading: loading < 3,
        menuTwo,
        setMenuTwo,
        dateSelected,
        setDateSelected,
        showNativeValue,
        setShowNativeValue,
      }}
    >
      {children}
    </VaultContext.Provider>
  );
}

export default VaultContextProvider;
