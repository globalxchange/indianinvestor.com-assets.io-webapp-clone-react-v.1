import React, { useContext, useRef, useEffect, useState } from 'react';
import Scrollbars from 'react-custom-scrollbars';
import Axios from 'axios';
import { useHistory } from 'react-router-dom';
import Skeleton from 'react-loading-skeleton';

import { BankContext } from '../context/Context';
import { NetWorthContext } from '../context/NetWorthContext';
import { FormatCurrency } from '../utils/FunctionTools';
import ViewAsUserItem from './ViewAsUserItem';

import guest from '../static/images/guest.jpg';
import unameIcon from '../static/images/formIcons/uname.svg';
import emailIcon from '../static/images/formIcons/email.svg';
import { APP_CODE } from '../config/appConfig';

function LayoutSidebarChangeUser({ setAdminSidebar }) {
  const history = useHistory();
  const {
    username,
    name,
    profileImg,
    setEmail,
    setUsername,
    setProfileImg,
    setProfileId,
    setName,
    defaultPrecission,
    closeSidebars,
  } = useContext(BankContext);
  const { totalBalance } = useContext(NetWorthContext);
  const [search, setSearch] = useState('');
  const [users, setUsers] = useState([]);
  const [type, setType] = useState('email');
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setLoading(true);
    Axios.get(
      `https://comms.globalxchange.com/gxb/apps/users/get?app_code=${APP_CODE}`
    )
      .then((res) => {
        const { data } = res;
        if (data.status) {
          setUsers(data.users);
        }
      })
      .finally(() => setLoading(false));
  }, []);

  return (
    <div className={`side-bar admin d-flex flex-column `}>
      <div className="profile d-flex">
        <img
          src={profileImg ? profileImg : guest}
          alt=""
          onClick={() => {
            closeSidebars();
            history.push('/app');
          }}
        />
        <div className="col my-auto" onClick={() => setAdminSidebar(false)}>
          <h5>{name ? name : username}&nbsp;</h5>
          <div className="getStartedBtn">
            ${FormatCurrency(totalBalance, defaultPrecission)}
          </div>
        </div>
      </div>
      <div className="userFind">
        <div className="searchWrap">
          <input
            type="text"
            className="searchInp"
            placeholder={`Type In The ${
              type === 'name' ? 'Username' : 'Email Id'
            }`}
            value={search}
            onChange={(e) => setSearch(e.target.value)}
          />
          <div className="box">
            {type === 'name' ? (
              <img src={emailIcon} alt="" onClick={() => setType('email')} />
            ) : (
              <img src={unameIcon} alt="" onClick={() => setType('name')} />
            )}
          </div>
        </div>
        <Scrollbars className="userList">
          {loading
            ? Array(10)
                .fill('')
                .map(() => (
                  <div className="viewAsUserItem">
                    <Skeleton className="profileImg" />
                    <div className="nameEmail">
                      <Skeleton className="name" width={200} />
                      <Skeleton className="email" width={160} />
                    </div>
                  </div>
                ))
            : users
                .filter((user) =>
                  user[type].toLowerCase().includes(search.toLowerCase())
                )
                .map((user) => {
                  return (
                    <ViewAsUserItem
                      user={user}
                      key={user._id}
                      onClick={() => {
                        setEmail(user.email);
                        setUsername(user.name);
                        setName(user.name);
                        setProfileImg('');
                        setAdminSidebar(false);
                        setProfileId('');
                      }}
                    />
                  );
                })}
        </Scrollbars>
      </div>
    </div>
  );
}

export default LayoutSidebarChangeUser;
