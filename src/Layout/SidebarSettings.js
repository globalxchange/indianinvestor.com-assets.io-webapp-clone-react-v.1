import React, { useState, useContext } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Scrollbars } from 'react-custom-scrollbars';
import selectedIcon from '../static/images/sidebar-icons/selected.svg';
import returnIcon from '../static/images/clipIcons/returnIcon.svg';
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { BankContext } from '../context/Context';
import EnterPinUnlock from '../components/EnterPinUnlock/EnterPinUnlock';
import { ChatContext } from '../context/ChatContext';

function SidebarSettings({ setLoginModalOpen, setTabItem }) {
  const [step, setStep] = useState(0);
  const {
    email,
    login,
    updateInterval,
    setUpdateInterval,
    populateModal,
    setOpenDefaultCoinSidebar,
    openDefaultCoinSidebar,
    setAdmin,
    defaultPrecission,
    setDefaultPrecission,
  } = useContext(BankContext);
  const { setTabSelected, setChatOn } = useContext(ChatContext);

  const [enterPin, setEnterPin] = useState(false);

  const stepsComponent = [
    <>
      <div className="settingsMenu" onClick={() => setStep(1)}>
        Preferences
        <FontAwesomeIcon icon={faAngleRight} />
      </div>
      {email ? (
        <div
          className="settingsMenu"
          onClick={() => {
            setTabSelected('profile');
            setChatOn(true);
          }}
        >
          My Account
          <FontAwesomeIcon icon={faAngleRight} />
        </div>
      ) : (
        <div
          className="settingsMenu"
          onClick={() => {
            try {
              setLoginModalOpen(true);
            } catch (error) {}
          }}
        >
          Login
          <FontAwesomeIcon icon={faAngleRight} />
        </div>
      )}
      <div className="settingsMenu disable" onClick={() => {}}>
        Accounting
        <FontAwesomeIcon icon={faAngleRight} />
      </div>
      {email ? (
        <>
          <div className="settingsMenu" onClick={() => setEnterPin(true)}>
            Admin
          </div>
          <div
            className="settingsMenu"
            onClick={() => {
              populateModal(
                'Are You Sure You Want To Logout',
                () => {},
                () => login()
              );
            }}
          >
            Logout
          </div>
        </>
      ) : (
        ''
      )}
    </>,
    <>
      <div className="settingsMenu" onClick={() => setStep(2)}>
        Refresh Rate
        <FontAwesomeIcon icon={faAngleRight} />
      </div>
      <div className="settingsMenu" onClick={() => setStep(3)}>
        Decimal Places
        <FontAwesomeIcon icon={faAngleRight} />
      </div>
      <div
        className="settingsMenu"
        onClick={() => setOpenDefaultCoinSidebar(!openDefaultCoinSidebar)}
      >
        Change Default Currency
        <FontAwesomeIcon icon={faAngleRight} />
      </div>
    </>,
    <>
      <div
        className="settingsMenu"
        onClick={() =>
          populateModal(
            'Confirm Update Of Data Refresh Manually',
            () => {},
            () => setUpdateInterval(0)
          )
        }
      >
        Manual{updateInterval === 0 ? <img src={selectedIcon} alt="" /> : ''}
      </div>
      <div
        className="settingsMenu"
        onClick={() =>
          populateModal(
            'Confirm Update Of Data Refresh Frequency To Ever 5 Seconds',
            () => {},
            () => setUpdateInterval(5)
          )
        }
      >
        5 Seconds{updateInterval === 5 ? <img src={selectedIcon} alt="" /> : ''}
      </div>
      <div
        className="settingsMenu"
        onClick={() =>
          populateModal(
            'Confirm Update Of Data Refresh Frequency To Ever 10 Seconds',
            () => {},
            () => setUpdateInterval(10)
          )
        }
      >
        10 Seconds
        {updateInterval === 10 ? <img src={selectedIcon} alt="" /> : ''}
      </div>
      <div
        className="settingsMenu"
        onClick={() =>
          populateModal(
            'Confirm Update Of Data Refresh Frequency To Ever 15 Seconds',
            () => {},
            () => setUpdateInterval(15)
          )
        }
      >
        15 Seconds
        {updateInterval === 15 ? <img src={selectedIcon} alt="" /> : ''}
      </div>
    </>,
    <>
      <div
        className="settingsMenu"
        onClick={() =>
          populateModal(
            'Confirm Update Decimal Places To Default',
            () => {},
            () => setDefaultPrecission(0)
          )
        }
      >
        Default
        {defaultPrecission === 0 ? <img src={selectedIcon} alt="" /> : ''}
      </div>
      <div
        className="settingsMenu"
        onClick={() =>
          populateModal(
            'Confirm Update Decimal Places To Max 5 Places After Decimal',
            () => {},
            () => setDefaultPrecission(5)
          )
        }
      >
        Max 5 Places After Decimal
        {defaultPrecission === 5 ? <img src={selectedIcon} alt="" /> : ''}
      </div>
      <div
        className="settingsMenu"
        onClick={() =>
          populateModal(
            'Confirm Update Decimal Places To Max 7 Places After Decimal',
            () => {},
            () => setDefaultPrecission(7)
          )
        }
      >
        Max 7 Places After Decimal
        {defaultPrecission === 7 ? <img src={selectedIcon} alt="" /> : ''}
      </div>
    </>,
  ];

  const stepTitle = [
    <>
      <div className={`tab-itm title`}>Control Panel</div>
      <div
        className={`tab-itm settings order-2`}
        onClick={() => {
          setTabItem('');
        }}
      >
        <img
          src={returnIcon}
          style={{ width: '20px', height: '20px' }}
          alt=""
        />
      </div>
    </>,
    <>
      <div className={`tab-itm title`}>Preferences</div>
      <div
        className={`tab-itm settings order-2`}
        onClick={() => {
          setStep(0);
        }}
      >
        <img
          src={returnIcon}
          style={{ width: '20px', height: '20px' }}
          alt=""
        />
      </div>
    </>,
    <>
      <div className={`tab-itm title`}>Refresh Rate</div>
      <div
        className={`tab-itm settings order-2`}
        onClick={() => {
          setStep(1);
        }}
      >
        <img
          src={returnIcon}
          style={{ width: '20px', height: '20px' }}
          alt=""
        />
      </div>
    </>,
    <>
      <div className={`tab-itm title`}>Decimal Places</div>
      <div
        className={`tab-itm settings order-2`}
        onClick={() => {
          setStep(1);
        }}
      >
        <img
          src={returnIcon}
          style={{ width: '20px', height: '20px' }}
          alt=""
        />
      </div>
    </>,
  ];

  return (
    <>
      <div className="assetPriceWrapper">
        <div className="tab-inrest-asset">
          <>{stepTitle[step]}</>
        </div>
        <Scrollbars
          className="rate-list-wrapper settings-wrapper"
          autoHide
          renderTrackHorizontal={() => <div />}
          renderThumbHorizontal={() => <div />}
          renderView={(props) => (
            <div {...props} className="rates-list settings-list" />
          )}
        >
          {stepsComponent[step]}
        </Scrollbars>
      </div>
      {enterPin && (
        <EnterPinUnlock
          onSucces={() => {
            setAdmin(true);
            setEnterPin(false);
          }}
          onClose={() => setEnterPin(false)}
        />
      )}
    </>
  );
}

export default SidebarSettings;
