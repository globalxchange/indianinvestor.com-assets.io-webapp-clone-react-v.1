import React, { useContext, useRef, useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Scrollbars } from 'react-custom-scrollbars';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faAngleLeft,
  faAngleRight,
  faKey,
  faLock,
} from '@fortawesome/free-solid-svg-icons';

import guest from '../static/images/guest.jpg';

import vaultsIcon from '../static/images/sidebar-icons/vaults.svg';
import vaultsLogo from '../static/images/sidebarFullLogos/vaults.svg';
import marketsIcon from '../static/images/markets.svg';
import marketsLogo from '../static/images/sidebarFullLogos/markets.svg';
import chatsIcon from '../static/images/sidebar-icons/chats.svg';
import chatsLogo from '../static/images/sidebarFullLogos/chats.svg';
import portfolioIcon from '../static/images/sidebar-icons/portfolio.svg';
import portfolioLogo from '../static/images/sidebarFullLogos/portfolio.svg';
import terminalsIcon from '../static/images/sidebar-icons/terminals.svg';
import terminalsLogo from '../static/images/sidebarFullLogos/terminals.svg';

import metaverseAppsIcon from '../static/images/sidebarIcons/metaverseApps.svg';
import metaverseAppsLogo from '../static/images/sidebarFullLogos/metaverseApps.svg';
import retiredIcon from '../static/images/sidebarIcons/retired.svg';
import retiredLogo from '../static/images/sidebarFullLogos/retired.svg';
import tradeStreamIcon from '../static/images/sidebarIcons/tradeStream.svg';
import tradeStreamLogo from '../static/images/sidebarFullLogos/tradeStream.svg';
import taxChainsIcon from '../static/images/sidebarIcons/taxChains.svg';
import taxChainsLogo from '../static/images/sidebarFullLogos/taxChains.svg';

import { BankContext } from '../context/Context';
import { NetWorthContext } from '../context/NetWorthContext';
import { FormatCurrency } from '../utils/FunctionTools';
import LoginModal from '../components/LoginModalNew';
import { APP_NAME, ICON_LOGO } from '../config/appConfig';
import Skeleton from 'react-loading-skeleton';
import SettingsModal from '../components/SettingsModal';
import { DISPLAY_CURRENCY, DISPLAY_CURRENCY_SYM } from '../config';

function LayoutSidebar({ active, chatOn, setChatOn, setAdminSidebar }) {
  const history = useHistory();
  const {
    email,
    username,
    name,
    profileImg,
    coinListObject,
    sidebarCollapse,
    setSidebarCollapse,
    defaultPrecission,
    closeSidebars,
  } = useContext(BankContext);
  const { totalBalance, loadingAppBalance } = useContext(NetWorthContext);

  const [loginModalOpen, setLoginModalOpen] = useState(false);
  const [onLoginPage, setOnLoginPage] = useState('');

  const onLogin = () => {
    if (onLoginPage) {
      active === 'index' && history.push(onLoginPage);
    }
    setLoginModalOpen(false);
  };

  const menuEndRef = useRef(null);

  const scrollToBottom = () => {
    if (menuEndRef && menuEndRef.current)
      menuEndRef.current.scrollIntoView({ behavior: 'smooth' });
  };

  useEffect(() => {
    if (active.includes('transactions')) {
      scrollToBottom();
    }
  }, [active]);

  const [sidebarSettings, setSidebarSettings] = useState(false);

  return (
    <>
      <div
        className={`side-bar d-flex flex-column ${
          sidebarCollapse ? 'collapse' : 'expand'
        }`}
      >
        {email ? (
          <div className="profile d-flex" onClick={() => setAdminSidebar(true)}>
            <img
              src={profileImg ? profileImg : guest}
              alt=""
              onClick={() => {
                setSidebarSettings(true);
              }}
            />
            <div className="col my-auto">
              <h5>{name ? name : username}&nbsp;</h5>
              {loadingAppBalance ? (
                <Skeleton className="getStartedBtn" />
              ) : (
                <div className="getStartedBtn">
                  {DISPLAY_CURRENCY_SYM}
                  {FormatCurrency(
                    totalBalance,
                    DISPLAY_CURRENCY,
                    defaultPrecission
                  )}
                </div>
              )}
            </div>
            <div
              className="toggle"
              onClick={() => setSidebarCollapse(!sidebarCollapse)}
            >
              <FontAwesomeIcon
                icon={sidebarCollapse ? faAngleRight : faAngleLeft}
              />
            </div>
          </div>
        ) : (
          <div className="profile d-flex">
            <img
              src={ICON_LOGO}
              alt=""
              onClick={() => {
                closeSidebars();
                setSidebarSettings(true);
              }}
            />
            <div className="col my-auto">
              <h5>{APP_NAME}&nbsp;</h5>
              <div
                className="getStartedBtn"
                onClick={() => {
                  history.push('/signup');
                }}
              >
                Get Started
              </div>
            </div>
            <div
              className="toggle"
              onClick={() => setSidebarCollapse(!sidebarCollapse)}
            >
              <FontAwesomeIcon
                icon={sidebarCollapse ? faAngleRight : faAngleLeft}
              />
            </div>
          </div>
        )}
        <Scrollbars
          className="menu-scrl"
          autoHide
          renderTrackHorizontal={() => <div />}
          renderThumbHorizontal={() => <div />}
          renderTrackVertical={() => <div />}
          renderThumbVertical={() => <div />}
          renderView={(props) => <div {...props} className="menu-side" />}
        >
          <div className="spacer" />
          <div
            onClick={() => {
              if (email) {
                history.push('/app');
                window.location.reload();
              } else {
                setLoginModalOpen(true);
                setOnLoginPage('/app');
              }
            }}
            className={`menu-itm${active === 'networth' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img
                src={sidebarCollapse ? portfolioIcon : portfolioLogo}
                alt=""
              />
            </h5>
            <div className="toolTip">Portfolio</div>
          </div>
          <Link
            to="/market"
            className={`menu-itm${active === 'index' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={sidebarCollapse ? marketsIcon : marketsLogo} alt="" />
            </h5>
            <div className="toolTip">Markets</div>
          </Link>
          <div
            className={`menu-itm${active.includes('vaults') ? ' active' : ''}`}
            onClick={() => {
              if (email) {
                history.push('/vault');
              } else {
                setLoginModalOpen(true);
                setOnLoginPage('/vault');
              }
            }}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={sidebarCollapse ? vaultsIcon : vaultsLogo} alt="" />
            </h5>
            <div className="toolTip">Vault</div>
          </div>
          {/* <Link
            to="/Terminal"
            className={`menu-itm${active === 'Terminal' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img
                src={sidebarCollapse ? terminalsIcon : terminalsLogo}
                alt=""
              />
            </h5>
            <div className="toolTip">Terminal</div>
          </Link> */}
          <div
            className={`menu-itm${chatOn ? ' active' : ''}`}
            onClick={() => setChatOn((prev) => !prev)}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={sidebarCollapse ? chatsIcon : chatsLogo} alt="" />
            </h5>
            <div className="toolTip">Chats</div>
          </div>
          <Link
            to="/tradestream"
            className={`menu-itm${active === 'tradestream' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img
                src={sidebarCollapse ? tradeStreamIcon : tradeStreamLogo}
                alt=""
              />
            </h5>
            <div className="toolTip">Trade Stream</div>
          </Link>
          <Link
            to="/retired"
            className={`menu-itm${active === 'retired' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img
                style={{ height: 39 }}
                src={sidebarCollapse ? retiredIcon : retiredLogo}
                alt=""
              />
            </h5>
            <div className="toolTip">Retired</div>
          </Link>
          <Link
            to="/taxchains"
            className={`menu-itm${active === 'taxchains' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img
                style={{ height: 39 }}
                src={sidebarCollapse ? taxChainsIcon : taxChainsLogo}
                alt=""
              />
            </h5>
            <div className="toolTip">TaxChains</div>
          </Link>
          <Link
            to="/metaverse"
            className={`menu-itm${active === 'metaverse' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img
                style={{ height: 39 }}
                src={sidebarCollapse ? metaverseAppsIcon : metaverseAppsLogo}
                alt=""
              />
            </h5>
            <div className="toolTip">Metaverse Apps</div>
          </Link>
          <div ref={menuEndRef} className="spacer" />
        </Scrollbars>
      </div>
      {loginModalOpen && (
        <LoginModal
          onClose={() => {
            setLoginModalOpen(false);
            setOnLoginPage('');
          }}
          onSuccess={onLogin}
        />
      )}
      {sidebarSettings && (
        <SettingsModal
          onClose={() => {
            setSidebarSettings(false);
          }}
          onSuccess={() => {
            setSidebarSettings(false);
            setLoginModalOpen(true);
          }}
        />
      )}
    </>
  );
}

export default LayoutSidebar;
