import React, { useState, useContext } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { Scrollbars } from 'react-custom-scrollbars';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faAngleLeft,
  faAngleRight,
  faLock,
} from '@fortawesome/free-solid-svg-icons';

import iced from '../static/images/logo.svg';
import markets from '../static/images/markets.svg';
import appstore from '../static/images/sidebar-icons/appstore.svg';
import classrooms from '../static/images/sidebar-icons/classrooms.svg';
import terminals from '../static/images/sidebar-icons/terminals.svg';
import portfolio from '../static/images/sidebar-icons/portfolio.svg';
import moneyMarkets from '../static/images/sidebar-icons/moneyMarkets.svg';
import planB from '../static/images/sidebar-icons/planB.svg';
import LoginModal from '../components/LoginModalNew';
import AssetPriceOrRates from './AssetPriceOrRates';
import { BankContext } from '../context/Context';
import { APP_NAME, ICON_LOGO } from '../config/appConfig';

function LayoutSidebarGuest({ active, videoShow }) {
  const history = useHistory();
  const [loginModalOpen, setLoginModalOpen] = useState(false);
  const [onLoginPage, setOnLoginPage] = useState('');
  const { sidebarCollapse, setSidebarCollapse, closeSidebars } = useContext(
    BankContext
  );
  const onLogin = () => {
    if (onLoginPage) {
      history.push(onLoginPage);
    }
  };
  return (
    <>
      <div
        key="guest"
        className={`side-bar d-flex flex-column ${
          sidebarCollapse || videoShow ? 'collapse' : 'expand'
        }`}
      >
        <div className="profile d-flex">
          <img
            src={ICON_LOGO}
            alt=""
            onClick={() => {
              closeSidebars();
              history.push('/app');
            }}
          />
          <div className="col my-auto">
            <h5>{APP_NAME}&nbsp;</h5>
            <div
              className="getStartedBtn"
              onClick={() => {
                history.push('/signup');
              }}
            >
              Get Started
            </div>
          </div>
          <div
            className="toggle"
            onClick={() => setSidebarCollapse(!sidebarCollapse)}
          >
            <FontAwesomeIcon
              icon={sidebarCollapse ? faAngleRight : faAngleLeft}
            />
          </div>
        </div>
        <Scrollbars
          className="menu-scrl"
          autoHide
          renderTrackHorizontal={() => <div />}
          renderThumbHorizontal={() => <div />}
          renderTrackVertical={() => <div />}
          renderThumbVertical={() => <div />}
          renderView={(props) => <div {...props} className="menu-side" />}
        >
          <Link
            to="/"
            className={`menu-itm${active === 'index' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={markets} alt="" />
              <span className="my-auto">Markets</span>
            </h5>
            <div className="toolTip">Markets</div>
          </Link>
          <Link
            to="/moneyMarkets"
            className={`menu-itm${active === 'moneyMarkets' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={moneyMarkets} alt="" />
              <span className="my-auto">Money Markets</span>
            </h5>
            <div className="toolTip">Money Markets</div>
          </Link>
          <div
            onClick={() => {
              setLoginModalOpen(true);
              setOnLoginPage('/app');
            }}
            className={`menu-itm${active === 'portfolio' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={portfolio} alt="" />
              <span className="my-auto">Portfolio</span>
            </h5>
            <div className="toolTip">Portfolio</div>
          </div>
          <a href="https://planb.assets.io/" className={`menu-itm`}>
            <h5 className="d-flex py-3 menu-itm">
              <img src={planB} alt="" />
              <span className="my-auto">PlanB</span>
            </h5>
            <div className="toolTip">PlanB</div>
          </a>
          <Link
            to="/Terminal"
            className={`menu-itm${active === 'Terminal' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={terminals} alt="" />
              <span className="my-auto">Terminal</span>
            </h5>
            <div className="toolTip">Terminal</div>
          </Link>
          <Link
            to="/Classrooms"
            className={`menu-itm${active === 'Classrooms' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={classrooms} alt="" />
              <span className="my-auto">Classrooms</span>
            </h5>
            <div className="toolTip">Classrooms</div>
          </Link>
          <Link
            to="/desktopapps"
            className={`menu-itm${active === 'desktopApps' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={appstore} alt="" />
              <span className="my-auto">Native Apps</span>
            </h5>
            <div className="toolTip">Native Apps</div>
          </Link>
        </Scrollbars>

        <AssetPriceOrRates
          setSidebarCollapse={setSidebarCollapse}
          isIndex={active === 'index'}
          setLoginModalOpen={setLoginModalOpen}
        />
        <div
          className="logoutBtn"
          onClick={() => {
            setLoginModalOpen(true);
            setOnLoginPage(false);
          }}
        >
          <FontAwesomeIcon icon={faLock} />
        </div>
      </div>
      {loginModalOpen ? (
        <LoginModal
          onClose={() => {
            setLoginModalOpen(false);
            setOnLoginPage();
          }}
          onSuccess={onLogin}
        />
      ) : (
        ''
      )}
    </>
  );
}

export default LayoutSidebarGuest;
