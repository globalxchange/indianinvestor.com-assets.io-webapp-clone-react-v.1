import React, { useContext, useState } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import { Link, useHistory } from 'react-router-dom';

import settingsIcon from '../static/images/sidebar-icons/settings.svg';
import markets from '../static/images/markets.svg';
import selectIcon from '../static/images/sidebar-icons/selected.svg';
import guest from '../static/images/guest.jpg';
import defi from '../static/images/sidebar-icons/defi.svg';
import chats from '../static/images/sidebar-icons/chats.svg';
import moneyMarkets from '../static/images/sidebar-icons/moneyMarkets.svg';
import planB from '../static/images/sidebar-icons/planB.svg';
import appstore from '../static/images/sidebar-icons/appstore.svg';
import funds from '../static/images/sidebar-icons/funds.svg';
import altVault from '../static/images/sidebar-icons/altVault.svg';
import classrooms from '../static/images/sidebar-icons/classrooms.svg';
import portfolio from '../static/images/sidebar-icons/portfolio.svg';
import vaults from '../static/images/sidebar-icons/vaults.svg';
import terminals from '../static/images/sidebar-icons/terminals.svg';
import allPlatforms from '../static/images/allPlatforms.svg';
import { BankContext } from '../context/Context';
import LoginModal from '../components/LoginModalNew';
import SidebarSettings from './SidebarSettings';
import { NetWorthContext } from '../context/NetWorthContext';
import { FormatCurrency } from '../utils/FunctionTools';
import { APP_NAME, ICON_LOGO } from '../config/appConfig';

function LayoutSidebarCoins({ active, chatOn, setChatOn }) {
  const {
    email,
    username,
    name,
    profileImg,
    coinList,
    defaultCoin,
    setDefaultCoin,
    setOpenDefaultCoinSidebar,
    defaultPrecission,
    closeSidebars,
  } = useContext(BankContext);
  const { totalBalance } = useContext(NetWorthContext);
  const history = useHistory();
  const [loginModalOpen, setLoginModalOpen] = useState(false);
  const [onLoginPage, setOnLoginPage] = useState('');
  const [tabItem, setTabItem] = useState('Interest Rates');

  const onLogin = () => {
    if (onLoginPage) {
      history.push(onLoginPage);
    } else {
      setLoginModalOpen(false);
    }
  };

  const coinListSorted = coinList;
  return (
    <>
      <div className="side-bar d-flex flex-column">
        <div className="profile d-flex">
          <img
            src={email ? profileImg || guest : ICON_LOGO}
            alt=""
            onClick={() => {
              closeSidebars();
              history.push('/app');
            }}
          />
          <div className="col my-auto">
            <h5>{email ? (name ? name : username) : APP_NAME}&nbsp;</h5>
            {email ? (
              <div className="getStartedBtn">
                ${FormatCurrency(totalBalance, 'USD', defaultPrecission)}
              </div>
            ) : (
              <div
                className="getStartedBtn"
                onClick={() => {
                  setLoginModalOpen(true);
                  setOnLoginPage(false);
                }}
              >
                Get Started
              </div>
            )}
          </div>
        </div>
        <Scrollbars
          className="menu-scrl"
          autoHide
          renderTrackHorizontal={() => <div />}
          renderThumbHorizontal={() => <div />}
          renderView={(props) => <div {...props} className="menu-side" />}
        >
          <div className="spacer" />
          <Link
            to="/markets"
            className={`menu-itm${active === 'index' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={markets} alt="" />
              <span className="my-auto">Markets</span>
            </h5>
            <div className="toolTip">Markets</div>
          </Link>
          <Link
            to="/moneyMarkets"
            className={`menu-itm${active === 'moneyMarkets' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={moneyMarkets} alt="" />
              <span className="my-auto">Money Markets</span>
            </h5>
            <div className="toolTip">Money Markets</div>
          </Link>
          <div
            onClick={() => {
              if (email) {
                history.push('/app');
              } else {
                setLoginModalOpen(true);
                setOnLoginPage('/app');
              }
            }}
            className={`menu-itm${active === 'networth' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={portfolio} alt="" />
              <span className="my-auto">Portfolio</span>
            </h5>
            <div className="toolTip">Portfolio</div>
          </div>
          <div
            className={`menu-itm${active.includes('vaults') ? ' active' : ''}`}
            onClick={() => {
              if (email) {
                history.push('/vault');
              } else {
                setLoginModalOpen(true);
                setOnLoginPage('/vault');
              }
            }}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={vaults} alt="" />
              <span className="my-auto">Vault</span>
            </h5>
            <div className="toolTip">Vault</div>
          </div>
          <div
            onClick={() => {
              if (email) {
                history.push('/earning');
              } else {
                setLoginModalOpen(true);
                setOnLoginPage('/earning');
              }
            }}
            className={`menu-itm${active === 'earn' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={defi} alt="" />
              <span className="my-auto">Defi Trust</span>
            </h5>
            <div className="toolTip">Defi Trust</div>
          </div>
          <div
            className={`menu-itm${
              active.includes('investmentVault') ? ' active' : ''
            }`}
            onClick={() => {
              if (email) {
                history.push('/investmentVault');
              } else {
                setLoginModalOpen(true);
                setOnLoginPage('/investmentVault');
              }
            }}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={funds} alt="" />
              <span className="my-auto">Funds</span>
            </h5>
            <div className="toolTip">Funds</div>
          </div>
          <div
            className={`menu-itm${
              active.includes('investmentVault') ? ' active' : ''
            }`}
            onClick={() => {
              if (email) {
                history.push('/alt-vault');
              } else {
                setLoginModalOpen(true);
                setOnLoginPage('/alt-vault');
              }
            }}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={altVault} alt="" />
              <span className="my-auto">Alt-Vault</span>
            </h5>
            <div className="toolTip">Alt-Vault</div>
          </div>
          <Link
            to="/Terminal"
            className={`menu-itm${active === 'Terminal' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={terminals} alt="" />
              <span className="my-auto">Terminal</span>
            </h5>
            <div className="toolTip">Terminal</div>
          </Link>
          <div
            className={`menu-itm${chatOn ? ' active' : ''}`}
            onClick={() => setChatOn((prev) => !prev)}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={chats} alt="" />
              <span className="my-auto">Chats</span>
            </h5>
            <div className="toolTip">Chats</div>
          </div>
          <Link
            to="/Classrooms"
            className={`menu-itm${active === 'Classrooms' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={classrooms} alt="" />
              <span className="my-auto">Classrooms</span>
            </h5>
            <div className="toolTip">Classrooms</div>
          </Link>
          <Link
            to="/desktopapps"
            className={`menu-itm${active === 'desktopApps' ? ' active' : ''}`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={appstore} alt="" />
              <span className="my-auto">Native Apps</span>
            </h5>
            <div className="toolTip">Native Apps</div>
          </Link>
          <a
            href="https://planb.assets.io/"
            target="_blank"
            className={`menu-itm`}
          >
            <h5 className="d-flex py-3 menu-itm">
              <img src={planB} alt="" />
              <span className="my-auto">PlanB</span>
            </h5>
            <div className="toolTip">PlanB</div>
          </a>
          <div className="spacer" />
        </Scrollbars>
        {tabItem === 'Settings' ? (
          <SidebarSettings
            tabItem={tabItem}
            setTabItem={setTabItem}
            defTab={
              <div
                className={`tab-itm title order-1 ${
                  tabItem === 'Interest Rates'
                }`}
                onClick={() => setTabItem('Interest Rates')}
              >
                Set New Display Currency
              </div>
            }
          />
        ) : (
          <>
            <div className="assetPriceWrapper">
              <div className="tab-inrest-asset">
                <div
                  className={`tab-itm title order-1 ${
                    tabItem === 'Interest Rates'
                  }`}
                  onClick={() => setTabItem('Interest Rates')}
                >
                  Set New Display Currency
                </div>
                <div
                  className={`tab-itm settings order-3 ${
                    tabItem === 'Settings'
                  }`}
                  onClick={() => setTabItem('Settings')}
                >
                  <img src={settingsIcon} alt="" />
                </div>
              </div>
              <Scrollbars
                autoHide
                renderThumbHorizontal={() => <div />}
                renderView={(props) => (
                  <div {...props} className="coins-list" />
                )}
                className="defCoinList"
              >
                <div
                  className="coin"
                  key="all"
                  onClick={() => {
                    setDefaultCoin({
                      coin: null,
                      name: 'Default Coin',
                      img: allPlatforms,
                    });
                    setOpenDefaultCoinSidebar(false);
                  }}
                >
                  <img className="coin-logo mr-2" src={allPlatforms} alt="" />
                  <div className="coin-name">Default Currency</div>
                  {defaultCoin.coin === null ? (
                    <img className="select" src={selectIcon} alt="" />
                  ) : (
                    ''
                  )}
                </div>
                {coinListSorted
                  .filter((coin) => coin.coinSymbol === 'USD')
                  .map((coin) => (
                    <div
                      className="coin"
                      key={coin.coinName}
                      onClick={() => {
                        setDefaultCoin({
                          coin: coin.coinSymbol,
                          name: coin.coinName,
                          img: coin.coinImage,
                        });
                        setOpenDefaultCoinSidebar(false);
                      }}
                    >
                      <img
                        className="coin-logo mr-2"
                        src={coin.coinImage}
                        alt=""
                      />
                      <div className="coin-name">{coin.coinName}</div>
                      {defaultCoin.coin === coin.coinSymbol ? (
                        <img className="select" src={selectIcon} alt="" />
                      ) : (
                        ''
                      )}
                    </div>
                  ))}
              </Scrollbars>
            </div>
          </>
        )}
      </div>
      {loginModalOpen ? (
        <LoginModal
          onClose={() => {
            setLoginModalOpen(false);
          }}
          onSuccess={onLogin}
        />
      ) : (
        ''
      )}
    </>
  );
}

export default LayoutSidebarCoins;
