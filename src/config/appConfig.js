import logoMain from '../static/images/appSpecific/mainLogo.svg';
import iconLogo from '../static/images/appSpecific/iconLogo.svg';
import mobileLogo from '../static/images/appSpecific/mobileLogo.svg';

export const APP_CODE = 'indianinvestor';

export const APP_NAME = 'IndianInvestor';

export const PRIMARY_COLOR = '#4caf50';

export const MAIN_LOGO = logoMain;

export const ICON_LOGO = iconLogo;

export const MOBILE_LOGO = mobileLogo;
