import React, { useEffect, useState, useContext } from 'react';
import Axios from 'axios';
import Scrollbars from 'react-custom-scrollbars';
import { useHistory, useParams } from 'react-router-dom';
import Layout from '../Layout/Index';
import search from '../static/images/search.svg';
import WhatIsAssetPostItem from '../components/WhatIsAssetPostItem/WhatIsAssetPostItem';
import Skeleton from 'react-loading-skeleton';
import { BankContext } from '../context/Context';
import EnterPinUnlock from '../components/EnterPinUnlock/EnterPinUnlock';
import { APP_NAME } from '../config/appConfig';

function WhatIsAssets() {
  const { videoId } = useParams();
  const { setVideoShow, videoShow, chatOn } = useContext(BankContext);
  const [searchStr, setSearchStr] = useState('');
  const [videoList, setVideoList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [pinModal, setPinModal] = useState(false);
  useEffect(() => {
    setLoading(true);
    Axios.get(
      'https://fxagency.apimachine.com/video/navbar/5fb318108efdfb791cb29ce5'
    )
      .then(({ data }) => {
        if (data.status && data.data) {
          setVideoList(data.data);
          if (videoId) {
            const vid = data.data?.filter((video) => video._id === videoId);
            vid && vid[0] && setVideoShow(vid[0]);
            Axios.post(
              'https://vod-backend.globalxchange.io/get_user_profiled_video_stream_link',
              {
                video_id: vid[0]?.video,
              }
            ).then(({ data }) => {
              vid && vid[0] && setVideoShow({ ...vid[0], videoUrl: data });
            });
          }
        }
      })
      .finally(() => {
        setLoading(false);
      });
  }, [videoId]);
  const history = useHistory();
  return (
    <Layout className="whatIsAssets" active="tradestream" hideFooter>
      <div className="posts">
        <div className="head">
          <svg
            className="logoSvg"
            viewBox="0 0 329 43"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M0.000691543 42.7578L0 14.2848L13.6468 0.475098H41.7935L27.8055 14.6258L13.988 14.2848L14.1588 29.6288L0.000691543 42.7578Z"
              fill="#464B4E"
            />
            <path
              d="M21.8386 36.854L21.6655 22.3139L34.6544 22.1406L49.2017 37.0269L21.8386 36.854Z"
              fill="#464B4E"
            />
            <path
              d="M55.0024 34.1615H73.5929V28.6752H61.9214V9.7004H55.0024V34.1615ZM100.156 34.1615H107.355L96.522 9.7004H89.7078L78.91 34.1615H85.9688L87.8907 29.409H98.2342L100.156 34.1615ZM89.9175 24.3072L93.0625 16.4796L96.2075 24.3072H89.9175ZM123.154 34.6507C130.423 34.6507 133.987 31.0165 133.987 26.7533C133.987 17.7376 120.114 20.5332 120.114 16.7941C120.114 15.5711 121.163 14.5926 124.063 14.5926C126.195 14.5926 128.501 15.2216 130.842 16.4796L132.974 11.3428C130.563 9.94501 127.278 9.21118 124.098 9.21118C116.83 9.21118 113.265 12.7755 113.265 17.1436C113.265 26.2291 127.173 23.3986 127.173 27.2774C127.173 28.4655 126.055 29.2693 123.189 29.2693C120.359 29.2693 117.284 28.2908 115.117 26.8581L112.846 31.96C115.152 33.5674 119.136 34.6507 123.154 34.6507ZM151.021 34.6507C158.29 34.6507 161.854 31.0165 161.854 26.7533C161.854 17.7376 147.981 20.5332 147.981 16.7941C147.981 15.5711 149.029 14.5926 151.93 14.5926C154.061 14.5926 156.368 15.2216 158.709 16.4796L160.841 11.3428C158.429 9.94501 155.145 9.21118 151.965 9.21118C144.696 9.21118 141.132 12.7755 141.132 17.1436C141.132 26.2291 155.04 23.3986 155.04 27.2774C155.04 28.4655 153.922 29.2693 151.056 29.2693C148.226 29.2693 145.151 28.2908 142.984 26.8581L140.713 31.96C143.019 33.5674 147.003 34.6507 151.021 34.6507ZM185.423 34.1615H192.831L187.554 26.4737C190.594 25.006 192.342 22.3503 192.342 18.751C192.342 13.1599 188.148 9.7004 181.474 9.7004H170.292V34.1615H177.211V27.6618H180.985L185.423 34.1615ZM185.353 18.751C185.353 20.9874 183.92 22.3153 181.055 22.3153H177.211V15.1517H181.055C183.92 15.1517 185.353 16.4796 185.353 18.751ZM213.736 34.6507C221.563 34.6507 227.329 29.2693 227.329 21.9309C227.329 14.5926 221.563 9.21118 213.736 9.21118C205.908 9.21118 200.143 14.5926 200.143 21.9309C200.143 29.2693 205.908 34.6507 213.736 34.6507ZM213.736 28.9198C210.032 28.9198 207.131 26.1592 207.131 21.9309C207.131 17.7027 210.032 14.9421 213.736 14.9421C217.44 14.9421 220.34 17.7027 220.34 21.9309C220.34 26.1592 217.44 28.9198 213.736 28.9198ZM248.53 34.6507C256.358 34.6507 262.123 29.2693 262.123 21.9309C262.123 14.5926 256.358 9.21118 248.53 9.21118C240.702 9.21118 234.937 14.5926 234.937 21.9309C234.937 29.2693 240.702 34.6507 248.53 34.6507ZM248.53 28.9198C244.826 28.9198 241.926 26.1592 241.926 21.9309C241.926 17.7027 244.826 14.9421 248.53 14.9421C252.234 14.9421 255.135 17.7027 255.135 21.9309C255.135 26.1592 252.234 28.9198 248.53 28.9198ZM299.434 34.1615L299.364 9.7004H293.668L285.281 23.8179L276.685 9.7004H270.989V34.1615H277.384V21.5466L283.639 31.7503H286.714L293.004 21.1971L293.074 34.1615H299.434ZM318.167 34.6507C325.436 34.6507 329 31.0165 329 26.7533C329 17.7376 315.127 20.5332 315.127 16.7941C315.127 15.5711 316.175 14.5926 319.076 14.5926C321.207 14.5926 323.514 15.2216 325.855 16.4796L327.987 11.3428C325.575 9.94501 322.291 9.21118 319.111 9.21118C311.842 9.21118 308.278 12.7755 308.278 17.1436C308.278 26.2291 322.186 23.3986 322.186 27.2774C322.186 28.4655 321.068 29.2693 318.202 29.2693C315.372 29.2693 312.297 28.2908 310.13 26.8581L307.859 31.96C310.165 33.5674 314.149 34.6507 318.167 34.6507Z"
              fill="#464B4E"
            />
          </svg>
        </div>
        <label className="searchWrapper">
          <input
            type="text"
            value={searchStr}
            onChange={(e) => setSearchStr(e.target.value)}
            placeholder="What Do You Want To Learn..."
          />
          <img src={search} alt="" />
        </label>
        <p className="subText">
          {APP_NAME} Is The Worlds First Wealth Transition Platform Where You
          You Can Digitalize Your Analog Assets By Turning Them Into
          Collatorized Fixed Income Instruments. Select Which Area Interests You
          The Most
        </p>
        <Scrollbars
          className="scrlWrap"
          renderThumbHorizontal={() => <div />}
          renderThumbVertical={() => <div />}
          renderView={(props) => <div {...props} className="postList" />}
        >
          {loading ? (
            <Skeleton height={120} count={5} />
          ) : (
            videoList
              .filter((vidData) =>
                vidData.title.toLowerCase().includes(searchStr.toLowerCase())
              )
              .map((vidData) => (
                <WhatIsAssetPostItem vidData={vidData} key={vidData.video} />
              ))
          )}
        </Scrollbars>
      </div>
      {!(videoShow || chatOn) && (
        <div className="playlists">
          <div className="headTitle">Playlists</div>
          <Scrollbars
            className="scrlWrap"
            renderThumbHorizontal={() => <div />}
            renderThumbVertical={() => <div />}
            renderView={(props) => <div {...props} className="playList" />}
          >
            <div className="helpCard">
              <div className="front">
                <h5>I Am Completely New</h5>
              </div>
              <div className="back">
                <h5>We Get It... This Stuff Is Confusing</h5>
                <p>
                  That Is Why We Have Prepared A Quick Start Training. Once You
                  Go Through It, You Will Have Created A Free {APP_NAME}{' '}
                  Account, Procured Some Cryptocurrency And Even Started To Grow
                  Your Digital Portfolio.
                </p>
                <div
                  onClick={() => setPinModal(true)}
                  to="/Classrooms/more"
                  className="btnLetsGo"
                >
                  Coming Soon
                </div>
              </div>
            </div>
            <div className="helpCard">
              <div className="front">
                <h5>Managing My Porfolio</h5>
              </div>
              <div className="back">
                <h5>We Get It... This Stuff Is Confusing</h5>
                <p>
                  That Is Why We Have Prepared A Quick Start Training. Once You
                  Go Through It, You Will Have Created A Free {APP_NAME}{' '}
                  Account, Procured Some Cryptocurrency And Even Started To Grow
                  Your Digital Portfolio.
                </p>
                <div
                  onClick={() => setPinModal(true)}
                  to="/Classrooms/more"
                  className="btnLetsGo"
                >
                  Coming Soon
                </div>
              </div>
            </div>
            <div className="helpCard">
              <div className="front">
                <h5>Insitutional Fund Management</h5>
              </div>
              <div className="back">
                <h5>We Get It... This Stuff Is Confusing</h5>
                <p>
                  That Is Why We Have Prepared A Quick Start Training. Once You
                  Go Through It, You Will Have Created A Free {APP_NAME}{' '}
                  Account, Procured Some Cryptocurrency And Even Started To Grow
                  Your Digital Portfolio.
                </p>
                <div
                  onClick={() => setPinModal(true)}
                  to="/Classrooms/more"
                  className="btnLetsGo"
                >
                  Coming Soon
                </div>
              </div>
            </div>
            <div className="helpCard">
              <div className="front">
                <h5>Creating Passive Income</h5>
              </div>
              <div className="back">
                <h5>We Get It... This Stuff Is Confusing</h5>
                <p>
                  That Is Why We Have Prepared A Quick Start Training. Once You
                  Go Through It, You Will Have Created A Free {APP_NAME}{' '}
                  Account, Procured Some Cryptocurrency And Even Started To Grow
                  Your Digital Portfolio.
                </p>
                <div
                  onClick={() => setPinModal(true)}
                  to="/Classrooms/more"
                  className="btnLetsGo"
                >
                  Coming Soon
                </div>
              </div>
            </div>
          </Scrollbars>
        </div>
      )}
      {pinModal && (
        <EnterPinUnlock
          onSucces={() => {
            setPinModal(false);
            history.push('/Classrooms/more');
          }}
          onClose={() => setPinModal(false)}
        />
      )}
    </Layout>
  );
}

export default WhatIsAssets;
