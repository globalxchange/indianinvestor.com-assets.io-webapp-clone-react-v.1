import React, {
  useContext,
  useState,
  useEffect,
  useRef,
  Fragment,
  createRef,
} from 'react';
import Scrollbars from 'react-custom-scrollbars';
import { Redirect, useHistory, useParams } from 'react-router-dom';
import moment from 'moment';
import Skeleton from 'react-loading-skeleton';
import classNames from './vaultsPage.module.scss';
import Layout from '../../Layout/Layout';
import { ASSET_CLASSES } from '../../config/constants';
import carretDown from '../../static/images/icons/carretDown.svg';
import { BankContext } from '../../context/Context';
import SelectAppModal from '../../components/SelectAppModal';
import {
  useShareTokensVaultsList,
  useUserApps,
  useUserDetails,
  useVaultTxnDetails,
} from '../../queryHooks';
import { APP_CODE } from '../../config/appConfig';
import VaultSelectModal from '../../components/VaultSelectModal';
import { FormatCurrency, YesterdayToday } from '../../utils/FunctionTools';
import VaultFab from '../../components/VaultsPage/VaultFab';
import searchIcon from '../../static/images/search.svg';
import VaultPageContextProvider, {
  VaultPageContext,
} from '../../context/VaultPageContext';
import credit from '../../static/images/txnIcons/credit.svg';
import debit from '../../static/images/txnIcons/debit.svg';
import chat from '../../static/images/txnIcons/chat.svg';
import edit from '../../static/images/txnIcons/edit.svg';
import txnHash from '../../static/images/txnIcons/txnHash.svg';
import indIcon from '../../static/images/txnIcons/ind.svg';
import connect from '../../static/images/txnIcons/connect.svg';
import tokenSwap from '../../static/images/txnIcons/tokenSwap.svg';
import OnOutsideClick from '../../utils/OnOutsideClick';
import VaultSelectBondsModal from '../../components/VaultSelectBondsModal';
import VaultSelectMoneyMarketModal from '../../components/VaultSelectMoneyMarketModal';
import VaultControllPanel from '../../components/VaultControllPanel';
import VaultSelectSharesModal from '../../components/VaultSelectSharesModal';
import VaultControllPanelForex from '../../components/VaultControllPanelForex';
import { DISPLAY_CURRENCY, DISPLAY_CURRENCY_SYM } from '../../config';

function VaultsPage() {
  return (
    <VaultPageContextProvider>
      <VaultsPageIn />
    </VaultPageContextProvider>
  );
}

function VaultsPageIn() {
  const { email } = useContext(BankContext);
  const {
    assetClass,
    setAssetClass,
    appSelected,
    setAppSelected,
    vaultSelected,
    setVaultSelected,
    cpanelOpen,
    setCpanelOpen,
    vaultsListFxCrypto: vaultsList,
    vaultsListFxCryptoLoading: loading,
  } = useContext(VaultPageContext);
  const { data: appList = [] } = useUserApps(email);
  const history = useHistory();
  const { appCodeParam } = useParams();
  useEffect(() => {
    let appSelect;
    if (appCodeParam) {
      appSelect = appList.filter((app) => app.app_code === appCodeParam)[0];
    } else {
      appSelect = appList.filter((app) => app.app_code === APP_CODE)[0];
    }
    if (appSelect) {
      setAppSelected(appSelect);
    }
  }, [appList]);

  if (!email) {
    return <Redirect to="/" />;
  }

  function getContent() {
    switch (assetClass?.name) {
      case 'moneyMarkets':
        return (
          <div className={classNames.contentArea}>
            <VaultHeaderMoneyMarkets
              email={email}
              vaultSelected={vaultSelected}
              setVaultSelected={setVaultSelected}
              loading={loading}
            />
            <VaultControlls
              appSelected={appSelected}
              setAppSelected={setAppSelected}
              email={email}
            />
            <VaultTransactionsMoneyMarkets email={email} />
          </div>
        );
      case 'bonds':
        return (
          <div className={classNames.contentArea}>
            <VaultHeaderBonds
              email={email}
              vaultSelected={vaultSelected}
              setVaultSelected={setVaultSelected}
              loading={loading}
            />
            <VaultControllsBonds
              appSelected={appSelected}
              setAppSelected={setAppSelected}
              email={email}
            />
            <VaultTransactionsBonds email={email} />
          </div>
        );
      case 'shares':
        return (
          <div className={classNames.contentArea}>
            <VaultHeaderShares
              email={email}
              vaultSelected={vaultSelected}
              setVaultSelected={setVaultSelected}
              loading={loading}
            />
            <VaultControllsShares
              email={email}
              appSelected={appSelected}
              setAppSelected={setAppSelected}
            />
            <VaultTransactionsShares email={email} />
          </div>
        );
      default:
        return (
          <div className={classNames.contentArea}>
            <VaultHeaderCryproForex
              appSelected={appSelected}
              setAppSelected={setAppSelected}
              assetClass={assetClass}
              email={email}
              vaultSelected={vaultSelected}
              setVaultSelected={setVaultSelected}
              vaultsList={vaultsList}
              loading={loading}
            />
            <VaultControlls
              appSelected={appSelected}
              setAppSelected={setAppSelected}
              email={email}
            />
            <VaultTransactions email={email} loading={loading} />
          </div>
        );
    }
  }

  function getCpanel() {
    switch (cpanelOpen) {
      case 'moneyMarkets':
        return <VaultControllPanel onClose={() => setCpanelOpen(false)} />;
      case 'fiat':
        return (
          <VaultControllPanelForex
            onClose={() => setCpanelOpen(false)}
            appSelected={appSelected}
          />
        );
      default:
        return false;
    }
  }

  return (
    <Layout
      active={`vaults`}
      className={classNames.vaults}
      hideFooter
      sidebarCustom={getCpanel()}
    >
      <div className={classNames.assetClassWrap}>
        <div className={classNames.assetClass}>
          {ASSET_CLASSES.map((assetClassCard, i) => (
            <div
              key={`${assetClassCard.name}-${i}`}
              className={`${classNames.assetClassCard} ${
                classNames[assetClassCard === assetClass]
              } ${classNames}`}
              onClick={() => {
                if (assetClassCard.name) {
                  setAssetClass(assetClassCard);
                  history.push(`/vault/${assetClassCard.name}`);
                }
              }}
            >
              <img
                src={assetClassCard.icon}
                alt=""
                className={classNames.icon}
              />
            </div>
          ))}
        </div>
      </div>
      {getContent()}
      <VaultFab vaultSelected={vaultSelected} />
    </Layout>
  );
}

function VaultHeaderCryproForex({
  appSelected,
  setAppSelected,
  assetClass,
  email,
  vaultSelected,
  setVaultSelected,
  vaultsList,
  loading,
}) {
  const [openVaultModel, setOpenVaultModel] = useState(false);
  const { coinParam } = useParams();
  const history = useHistory();
  useEffect(() => {
    const vaultParam = vaultsList.filter(
      (vault) => vault.coinSymbol === coinParam
    )[0];
    const vault = vaultsList.filter(
      (vault) => vault.coinSymbol === vaultSelected?.coinSymbol
    )[0];
    const vaultInr = vaultsList.filter(
      (vault) => vault.coinSymbol === DISPLAY_CURRENCY
    )[0];
    const vaultDef = vaultsList[0];
    if (!loading && vaultsList) {
      if (vaultParam) {
        setVaultSelected(vaultParam);
      } else if (vault) {
        setVaultSelected(vault);
      } else if (vaultInr) {
        setVaultSelected(vaultInr);
      } else if (vaultDef) {
        setVaultSelected(vaultDef);
      } else {
        setVaultSelected();
      }
    }
  }, [vaultsList, coinParam]);

  return (
    <>
      <div className={classNames.vaultHeader}>
        <div className={classNames.vaultNbalance}>
          <div className={classNames.vault}>
            {loading ? (
              <Skeleton width={150} />
            ) : (
              <>{vaultSelected?.coinName} Vault</>
            )}
          </div>
          <div className={classNames.balance}>
            {loading ? (
              <Skeleton width={250} />
            ) : (
              <>
                {FormatCurrency(
                  vaultSelected?.coinValue,
                  vaultSelected?.coinSymbol
                )}{' '}
                <small>
                  {DISPLAY_CURRENCY_SYM}
                  {FormatCurrency(
                    vaultSelected?.coinValue_DC,
                    DISPLAY_CURRENCY
                  )}
                </small>
              </>
            )}
          </div>
        </div>
        <div className={classNames.coinSelect}>
          {loading ? (
            <>
              <Skeleton
                className={`${classNames.coinWrap} ${classNames.false}`}
              />
              <Skeleton
                className={`${classNames.coinWrap} ${classNames.false}`}
              />
              <Skeleton
                className={`${classNames.coinWrap} ${classNames.false}`}
              />
            </>
          ) : (
            <>
              {vaultsList
                ?.slice(0, 3)
                .filter(
                  (vault) => vault.coinSymbol === vaultSelected?.coinSymbol
                )[0] ? (
                ''
              ) : (
                <div className={`${classNames.coinWrap} ${classNames.true}`}>
                  <img src={vaultSelected?.coinImage} alt="" />
                </div>
              )}
              {vaultsList?.slice(0, 3).map((vault) => (
                <div
                  key={vault.coinSymbol}
                  className={`${classNames.coinWrap} ${
                    classNames[vault.coinSymbol === vaultSelected?.coinSymbol]
                  }`}
                  onClick={() => {
                    setVaultSelected(vault);
                    history.push(
                      `/vault/${assetClass.name}/${appSelected.app_code}/${vault.coinSymbol}`
                    );
                  }}
                >
                  <img src={vault?.coinImage} alt="" />
                </div>
              ))}
            </>
          )}
          <div
            className={classNames.coinWrap}
            onClick={() => {
              setOpenVaultModel(true);
            }}
          >
            <img
              src={searchIcon}
              alt=""
              style={{
                width: 26,
                height: 26,
              }}
            />
          </div>
        </div>
      </div>
      {openVaultModel && (
        <VaultSelectModal
          appSelected={appSelected}
          setAppSelected={setAppSelected}
          assetClass={assetClass}
          email={email}
          vaultSelected={vaultSelected}
          setVaultSelected={setVaultSelected}
          onSuccess={() => setOpenVaultModel(false)}
          onClose={() => setOpenVaultModel(false)}
        />
      )}
    </>
  );
}

function VaultHeaderBonds({ email, vaultSelected, setVaultSelected }) {
  const [openVaultModel, setOpenVaultModel] = useState(false);

  const { vaultsListBonds: vaultsList, vaultsListBondsLoading: loading } =
    useContext(VaultPageContext);

  useEffect(() => {
    try {
      setVaultSelected(vaultsList?.balances[0]);
    } catch (error) {}
  }, [vaultsList]);

  return (
    <>
      <div className={classNames.vaultHeader}>
        <div className={classNames.vaultNbalance}>
          <div className={classNames.vault}>
            {loading ? (
              <Skeleton width={150} />
            ) : (
              <>{vaultSelected?.coinName} Bond Earnings Vault</>
            )}
          </div>
          <div className={classNames.balance}>
            {loading ? (
              <Skeleton width={250} />
            ) : (
              <>
                {FormatCurrency(
                  vaultSelected?.coinValue,
                  vaultSelected?.coinSymbol
                )}{' '}
                <small>
                  {DISPLAY_CURRENCY_SYM}
                  {FormatCurrency(
                    vaultSelected?.value_in_displayCurrency,
                    DISPLAY_CURRENCY
                  )}
                </small>
              </>
            )}
          </div>
        </div>
        <div className={classNames.coinSelect}>
          {loading ? (
            <>
              <Skeleton
                className={`${classNames.coinWrap} ${classNames.false}`}
              />
              <Skeleton
                className={`${classNames.coinWrap} ${classNames.false}`}
              />
              <Skeleton
                className={`${classNames.coinWrap} ${classNames.false}`}
              />
            </>
          ) : (
            <>
              {vaultsList?.balances
                ?.slice(0, 3)
                .filter(
                  (vault) => vault.coinSymbol === vaultSelected?.coinSymbol
                )[0] ? (
                ''
              ) : (
                <div className={`${classNames.coinWrap} ${classNames.true}`}>
                  <img src={vaultSelected?.coinImage} alt="" />
                </div>
              )}
              {vaultsList?.balances?.slice(0, 3).map((vault) => (
                <div
                  key={vault.coinSymbol}
                  className={`${classNames.coinWrap} ${
                    classNames[vault.coinSymbol === vaultSelected?.coinSymbol]
                  }`}
                  onClick={() => {
                    setVaultSelected(vault);
                  }}
                >
                  <img src={vault?.coinImage} alt="" />
                </div>
              ))}
            </>
          )}
          <div
            className={classNames.coinWrap}
            onClick={() => {
              setOpenVaultModel(true);
            }}
          >
            <img
              src={searchIcon}
              alt=""
              style={{
                width: 26,
                height: 26,
              }}
            />
          </div>
        </div>
      </div>
      {openVaultModel && (
        <VaultSelectBondsModal
          email={email}
          vaultSelected={vaultSelected}
          setVaultSelected={setVaultSelected}
          onSuccess={() => setOpenVaultModel(false)}
          onClose={() => setOpenVaultModel(false)}
        />
      )}
    </>
  );
}

function VaultHeaderMoneyMarkets({
  email,
  vaultSelected,
  setVaultSelected,
  loading,
}) {
  const { coinListObject } = useContext(BankContext);
  const [openVaultModel, setOpenVaultModel] = useState(false);
  const { vaultsListMM: vaultsList, vaultsListLoadingMM: vaultsListLoading } =
    useContext(VaultPageContext);
  useEffect(() => {
    try {
      setVaultSelected(vaultsList[0]);
    } catch (error) {}
  }, [vaultsList]);

  return (
    <>
      <div className={classNames.vaultHeader}>
        <div className={classNames.vaultNbalance}>
          <div className={classNames.vault}>
            {loading || vaultsListLoading ? (
              <Skeleton width={150} />
            ) : (
              <>{vaultSelected?.coinName} MoneyMarket Earnings Vault</>
            )}
          </div>
          <div className={classNames.balance}>
            {loading || vaultsListLoading ? (
              <Skeleton width={250} />
            ) : (
              <>
                {FormatCurrency(
                  vaultSelected?.coinValue,
                  vaultSelected?.coinSymbol
                )}{' '}
                <small>
                  {DISPLAY_CURRENCY_SYM}
                  {FormatCurrency(
                    vaultSelected?.coinValueUSD /
                      coinListObject?.[DISPLAY_CURRENCY]?.coinPriceUSD,
                    DISPLAY_CURRENCY
                  )}
                </small>
              </>
            )}
          </div>
        </div>
        <div className={classNames.coinSelect}>
          {loading || vaultsListLoading ? (
            <>
              <Skeleton
                className={`${classNames.coinWrap} ${classNames.false}`}
              />
              <Skeleton
                className={`${classNames.coinWrap} ${classNames.false}`}
              />
              <Skeleton
                className={`${classNames.coinWrap} ${classNames.false}`}
              />
            </>
          ) : (
            <>
              {vaultsList
                ?.slice(0, 3)
                .filter(
                  (vault) => vault.coinSymbol === vaultSelected?.coinSymbol
                )[0] ? (
                ''
              ) : (
                <div className={`${classNames.coinWrap} ${classNames.true}`}>
                  <img src={vaultSelected?.coinImage} alt="" />
                </div>
              )}
              {vaultsList?.slice(0, 3).map((vault) => (
                <div
                  key={vault.coinSymbol}
                  className={`${classNames.coinWrap} ${
                    classNames[vault.coinSymbol === vaultSelected?.coinSymbol]
                  }`}
                  onClick={() => {
                    setVaultSelected(vault);
                  }}
                >
                  <img src={vault?.coinImage} alt="" />
                </div>
              ))}
            </>
          )}
          <div
            className={classNames.coinWrap}
            onClick={() => {
              setOpenVaultModel(true);
            }}
          >
            <img
              src={searchIcon}
              alt=""
              style={{
                width: 26,
                height: 26,
              }}
            />
          </div>
        </div>
      </div>
      {openVaultModel && (
        <VaultSelectMoneyMarketModal
          email={email}
          onSuccess={() => setOpenVaultModel(false)}
          onClose={() => setOpenVaultModel(false)}
        />
      )}
    </>
  );
}

function VaultHeaderShares({
  email,
  vaultSelected,
  setVaultSelected,
  loading,
}) {
  const { coinListObject } = useContext(BankContext);
  const [openVaultModel, setOpenVaultModel] = useState(false);
  const {
    vaultsListShares: vaultsList,
    vaultsListLoadingShares: vaultsListLoading,
  } = useContext(VaultPageContext);
  useEffect(() => {
    try {
      setVaultSelected(vaultsList[0]);
    } catch (error) {}
  }, [vaultsList]);

  return (
    <>
      <div className={classNames.vaultHeader}>
        <div className={classNames.vaultNbalance}>
          <div className={classNames.vault}>
            {loading || vaultsListLoading ? (
              <Skeleton width={150} />
            ) : (
              <>
                {vaultSelected?.tokens[0]?.token_profile_data?.coinName}{' '}
                ShareToken Vault
              </>
            )}
          </div>
          <div className={classNames.balance}>
            {loading || vaultsListLoading ? (
              <Skeleton width={250} />
            ) : (
              <>
                {FormatCurrency(
                  vaultSelected?.tokens[0]?.value,
                  vaultSelected?.tokens[0]?.token
                )}{' '}
                <small>
                  {DISPLAY_CURRENCY_SYM}
                  {FormatCurrency(
                    (vaultSelected?.tokens[0]?.value_in_asset *
                      coinListObject[vaultSelected?.tokens[0]?.asset]
                        ?.coinPriceUSD) /
                      coinListObject?.[DISPLAY_CURRENCY]?.coinPriceUSD,
                    vaultSelected?.tokens[0]?.asset
                  )}
                </small>
              </>
            )}
          </div>
        </div>
        <div className={classNames.coinSelect}>
          {loading || vaultsListLoading ? (
            <>
              <Skeleton
                className={`${classNames.coinWrap} ${classNames.false}`}
              />
              <Skeleton
                className={`${classNames.coinWrap} ${classNames.false}`}
              />
              <Skeleton
                className={`${classNames.coinWrap} ${classNames.false}`}
              />
            </>
          ) : (
            <>
              {vaultsList
                ?.slice(0, 3)
                .filter(
                  (vault) =>
                    vault?.tokens[0]?.token_profile_data?.coinSymbol ===
                    vaultSelected?.tokens[0]?.token_profile_data?.coinSymbol
                )[0] ? (
                ''
              ) : (
                <div className={`${classNames.coinWrap} ${classNames.true}`}>
                  <img
                    src={
                      vaultSelected?.tokens[0]?.token_profile_data?.coinImage
                    }
                    alt=""
                  />
                </div>
              )}
              {vaultsList?.slice(0, 3).map((vault) => (
                <div
                  key={vault.coinSymbol}
                  className={`${classNames.coinWrap} ${
                    classNames[
                      vault?.tokens[0]?.token_profile_data?.coinSymbol ===
                        vaultSelected?.tokens[0]?.token_profile_data?.coinSymbol
                    ]
                  }`}
                  onClick={() => {
                    setVaultSelected(vault);
                  }}
                >
                  <img
                    src={vault?.tokens[0]?.token_profile_data?.coinImage}
                    alt=""
                  />
                </div>
              ))}
            </>
          )}
          <div
            className={classNames.coinWrap}
            onClick={() => {
              setOpenVaultModel(true);
            }}
          >
            <img
              src={searchIcon}
              alt=""
              style={{
                width: 26,
                height: 26,
              }}
            />
          </div>
        </div>
      </div>
      {openVaultModel && (
        <VaultSelectSharesModal
          email={email}
          onSuccess={() => setOpenVaultModel(false)}
          onClose={() => setOpenVaultModel(false)}
          vaultSelected={vaultSelected}
          setVaultSelected={setVaultSelected}
        />
      )}
    </>
  );
}

function VaultControlls({ appSelected, setAppSelected, email }) {
  const [appModal, setAppModal] = useState(false);
  return (
    <>
      <div className={classNames.vaultFilter}>
        <div
          className={classNames.appSelectWrap}
          onClick={() => setAppModal(true)}
        >
          {appSelected ? (
            <div className={classNames.appSelected}>
              <img
                src={appSelected?.app_icon}
                alt=""
                className={classNames.icon}
              />
              <span className={classNames.appName}>
                {appSelected?.app_name}
              </span>
              <img src={carretDown} alt="" className={classNames.caret} />
            </div>
          ) : (
            <div className={classNames.appSelected}>
              <Skeleton className={classNames.icon} circle />
              <Skeleton width={150} className={classNames.appName} />
            </div>
          )}
        </div>
        <div className={classNames.allTxnWrap}>
          <div className={classNames.txnSelected}>
            <span className={classNames.label}>All Transactions</span>
            <img src={carretDown} alt="" className={classNames.caret} />
          </div>
        </div>
        <div className={classNames.allDirectionWrap}>
          <div className={classNames.dirSelected}>
            <span className={classNames.label}>All Directions</span>
            <img src={carretDown} alt="" className={classNames.caret} />
          </div>
        </div>
        <div className={classNames.searchWrap}>
          <div className={classNames.searchHead}>
            <input
              type="text"
              name="search"
              className={classNames.searcInp}
              placeholder="Search Transactions"
            />
            <img src={searchIcon} alt="" className={classNames.searchIcon} />
          </div>
        </div>
      </div>
      {appModal && (
        <SelectAppModal
          onClose={() => setAppModal(false)}
          onSuccess={() => setAppModal(false)}
          appSelected={appSelected}
          setAppSelected={setAppSelected}
          email={email}
        />
      )}
    </>
  );
}

function VaultControllsBonds({ appSelected }) {
  return (
    <>
      <div className={classNames.vaultFilter}>
        <div className={classNames.appSelectWrap}>
          {appSelected ? (
            <div className={classNames.appSelected} style={{ opacity: 0.7 }}>
              <img
                src={appSelected?.app_icon}
                alt=""
                className={classNames.icon}
              />
              <span className={classNames.appName}>
                {appSelected?.app_name}
              </span>
              <img src={carretDown} alt="" className={classNames.caret} />
            </div>
          ) : (
            <div className={classNames.appSelected}>
              <Skeleton className={classNames.icon} circle />
              <Skeleton width={150} className={classNames.appName} />
            </div>
          )}
        </div>
        <div className={classNames.allTxnWrap}>
          <div className={classNames.txnSelected}>
            <span className={classNames.label}>All Transactions</span>
            <img src={carretDown} alt="" className={classNames.caret} />
          </div>
        </div>
        <div className={classNames.allDirectionWrap}>
          <div className={classNames.dirSelected}>
            <span className={classNames.label}>All Directions</span>
            <img src={carretDown} alt="" className={classNames.caret} />
          </div>
        </div>
        <div className={classNames.searchWrap}>
          <div className={classNames.searchHead}>
            <input
              type="text"
              name="search"
              className={classNames.searcInp}
              placeholder="Search Transactions"
            />
            <img src={searchIcon} alt="" className={classNames.searchIcon} />
          </div>
        </div>
      </div>
    </>
  );
}

function VaultControllsShares({ email, appSelected, setAppSelected }) {
  const [appModal, setAppModal] = useState(false);
  const { data } = useShareTokensVaultsList(email);
  const { vaultSelected } = useContext(VaultPageContext);
  useEffect(() => {
    if (data?.appsData) {
      setAppSelected(Object.values(data?.appsData)[0]);
    }
  }, [data?.appsData]);

  return (
    <>
      <div className={classNames.vaultFilter}>
        <div
          className={classNames.appSelectWrap}
          onClick={() => setAppModal(true)}
        >
          {appSelected ? (
            <div className={classNames.appSelected}>
              <img
                src={appSelected?.app_icon}
                alt=""
                className={classNames.icon}
              />
              <span className={classNames.appName}>
                {appSelected?.app_name}
              </span>
              <img src={carretDown} alt="" className={classNames.caret} />
            </div>
          ) : (
            <div className={classNames.appSelected}>
              <Skeleton className={classNames.icon} circle />
              <Skeleton width={150} className={classNames.appName} />
            </div>
          )}
        </div>
        <div className={classNames.allTxnWrap}>
          <div className={classNames.txnSelected}>
            <span className={classNames.label}>All Transactions</span>
            <img src={carretDown} alt="" className={classNames.caret} />
          </div>
        </div>
        <div className={classNames.allDirectionWrap}>
          <div className={classNames.dirSelected}>
            <span className={classNames.label}>All Directions</span>
            <img src={carretDown} alt="" className={classNames.caret} />
          </div>
        </div>
        <div className={classNames.searchWrap}>
          <div className={classNames.searchHead}>
            <input
              type="text"
              name="search"
              className={classNames.searcInp}
              placeholder="Search Transactions"
            />
            <img src={searchIcon} alt="" className={classNames.searchIcon} />
          </div>
        </div>
      </div>
      {appModal && (
        <SelectAppModal
          filter={data?.appsData && Object.keys(data?.appsData)}
          onClose={() => setAppModal(false)}
          onSuccess={() => setAppModal(false)}
          appSelected={appSelected}
          setAppSelected={setAppSelected}
          email={email}
        />
      )}
    </>
  );
}

function VaultTransactions({ loading }) {
  const { txnListFxCrypto: txnList, txnListLoadingFxCrypto: txnListLoading } =
    useContext(VaultPageContext);
  const date = useRef();
  return (
    <Scrollbars className={classNames.vaultsView}>
      {txnListLoading || loading
        ? Array(3)
            .fill('')
            .map((_, i) => (
              <>
                <Skeleton className={classNames.day} width={200} key={i} />
                {Array(4)
                  .fill('')
                  .map((_, i) => (
                    <div className={classNames.vaultItmWrap}>
                      <div className={classNames.vaultsItm}>
                        <Skeleton className={classNames.img} />
                        <div className={classNames.nameDate}>
                          <Skeleton className={classNames.name} width={350} />
                          <Skeleton className={classNames.date} width={300} />
                        </div>
                        <div className={classNames.credit}>
                          <Skeleton className={classNames.value} width={80} />
                        </div>
                        <div className={classNames.debit}>
                          <Skeleton className={classNames.value} width={80} />
                        </div>
                        <div className={classNames.balance}>
                          <Skeleton className={classNames.value} width={80} />
                        </div>
                      </div>
                    </div>
                  ))}
              </>
            ))
        : txnList?.map((txn) => {
            function sameDay() {
              if (moment(txn.timestamp).format('MMDDYYYY') === date.current) {
                return <></>;
              } else {
                date.current = moment(txn.timestamp).format('MMDDYYYY');
                return (
                  <div className={classNames.day}>
                    {YesterdayToday(txn.timestamp)}
                  </div>
                );
              }
            }
            return (
              <Fragment key={txn._id}>
                {sameDay()}
                <VaultItemForexCrypto key={txn._id} txn={txn} />
              </Fragment>
            );
          })}
    </Scrollbars>
  );
}

function VaultTransactionsBonds({ email }) {
  const { txnListBonds: txnList, txnListLoadingBonds: txnListLoading } =
    useContext(VaultPageContext);
  const date = useRef();
  return (
    <Scrollbars className={classNames.vaultsView}>
      {txnListLoading
        ? Array(3)
            .fill('')
            .map((_, i) => (
              <>
                <Skeleton className={classNames.day} width={200} key={i} />
                {Array(4)
                  .fill('')
                  .map((_, i) => (
                    <div className={classNames.vaultItmWrap}>
                      <div className={classNames.vaultsItm}>
                        <Skeleton className={classNames.img} />
                        <div className={classNames.nameDate}>
                          <Skeleton className={classNames.name} width={350} />
                          <Skeleton className={classNames.date} width={300} />
                        </div>
                        <div className={classNames.credit}>
                          <Skeleton className={classNames.value} width={80} />
                        </div>
                        <div className={classNames.debit}>
                          <Skeleton className={classNames.value} width={80} />
                        </div>
                        <div className={classNames.balance}>
                          <Skeleton className={classNames.value} width={80} />
                        </div>
                      </div>
                    </div>
                  ))}
              </>
            ))
        : txnList?.map((txn) => {
            function sameDay() {
              if (moment(txn.timestamp).format('MMDDYYYY') === date.current) {
                return <></>;
              } else {
                date.current = moment(txn.timestamp).format('MMDDYYYY');
                return (
                  <div className={classNames.day}>
                    {YesterdayToday(txn.timestamp)}
                  </div>
                );
              }
            }
            return (
              <Fragment key={txn._id}>
                {sameDay()}
                <VaultItemBonds key={txn._id} txn={txn} />
              </Fragment>
            );
          })}
    </Scrollbars>
  );
}

function VaultTransactionsMoneyMarkets({ email }) {
  const { txnListMM: txnList, txnListLoadingMM: txnListLoading } =
    useContext(VaultPageContext);
  const date = useRef();
  return (
    <Scrollbars className={classNames.vaultsView}>
      {txnListLoading
        ? Array(3)
            .fill('')
            .map((_, i) => (
              <>
                <Skeleton className={classNames.day} width={200} key={i} />
                {Array(4)
                  .fill('')
                  .map((_, i) => (
                    <div className={classNames.vaultItmWrap}>
                      <div className={classNames.vaultsItm}>
                        <Skeleton className={classNames.img} />
                        <div className={classNames.nameDate}>
                          <Skeleton className={classNames.name} width={350} />
                          <Skeleton className={classNames.date} width={300} />
                        </div>
                        <div className={classNames.credit}>
                          <Skeleton className={classNames.value} width={80} />
                        </div>
                        <div className={classNames.debit}>
                          <Skeleton className={classNames.value} width={80} />
                        </div>
                        <div className={classNames.balance}>
                          <Skeleton className={classNames.value} width={80} />
                        </div>
                      </div>
                    </div>
                  ))}
              </>
            ))
        : txnList?.map((txn) => {
            function sameDay() {
              if (moment(txn.timestamp).format('MMDDYYYY') === date.current) {
                return <></>;
              } else {
                date.current = moment(txn.timestamp).format('MMDDYYYY');
                return (
                  <div className={classNames.day}>
                    {YesterdayToday(txn.timestamp)}
                  </div>
                );
              }
            }
            return (
              <Fragment key={txn._id}>
                {sameDay()}
                <VaultItemMoneyMarkets key={txn._id} txn={txn} />
              </Fragment>
            );
          })}
    </Scrollbars>
  );
}

function VaultTransactionsShares({ loading }) {
  const { txnListShares: txnList, txnListLoadingShares: txnListLoading } =
    useContext(VaultPageContext);
  const date = useRef();
  return (
    <Scrollbars className={classNames.vaultsView}>
      {txnListLoading || loading
        ? Array(3)
            .fill('')
            .map((_, i) => (
              <>
                <Skeleton className={classNames.day} width={200} key={i} />
                {Array(4)
                  .fill('')
                  .map((_, i) => (
                    <div className={classNames.vaultItmWrap}>
                      <div className={classNames.vaultsItm}>
                        <Skeleton className={classNames.img} />
                        <div className={classNames.nameDate}>
                          <Skeleton className={classNames.name} width={350} />
                          <Skeleton className={classNames.date} width={300} />
                        </div>
                        <div className={classNames.credit}>
                          <Skeleton className={classNames.value} width={80} />
                        </div>
                        <div className={classNames.debit}>
                          <Skeleton className={classNames.value} width={80} />
                        </div>
                        <div className={classNames.balance}>
                          <Skeleton className={classNames.value} width={80} />
                        </div>
                      </div>
                    </div>
                  ))}
              </>
            ))
        : txnList?.map((txn) => {
            function sameDay() {
              if (moment(txn.timestamp).format('MMDDYYYY') === date.current) {
                return <></>;
              } else {
                date.current = moment(txn.timestamp).format('MMDDYYYY');
                return (
                  <div className={classNames.day}>
                    {YesterdayToday(txn.timestamp)}
                  </div>
                );
              }
            }
            return (
              <Fragment key={txn._id}>
                {sameDay()}
                <VaultItemForexCrypto key={txn._id} txn={txn} />
              </Fragment>
            );
          })}
    </Scrollbars>
  );
}

function VaultItemForexCrypto({ txn }) {
  const { assetClass } = useContext(VaultPageContext);
  const [txnOpen, setTxnOpen] = useState(false);

  const elRefs = useRef([]);
  if (elRefs.current.length !== 3) {
    // add or remove refs
    elRefs.current = Array(3)
      .fill()
      .map((_, i) => elRefs.current[i] || createRef());
  }

  const refOut = useRef();
  OnOutsideClick(
    undefined,
    () => {
      setTxnOpen((txnOpen) => !txnOpen);
    },
    refOut,
    elRefs
  );

  return (
    <div
      className={`${classNames.vaultItmWrap} ${classNames[txnOpen]}`}
      ref={refOut}
    >
      <div className={classNames.vaultsItm}>
        <img
          src={txn?.withdraw ? credit : debit}
          alt=""
          className={classNames.img}
        />
        <div className={classNames.nameDate}>
          <div className={classNames.name}>{txn.pid || txn.reason}</div>
          <div className={classNames.date}>
            {moment(txn.timestamp).format('MMMM Do YYYY [at] h:mm:ss A zz')}
          </div>
        </div>
        <div className={classNames.buttons} ref={elRefs.current[0]}>
          <div className={classNames.btnNotes}>
            <span>Notes</span>
          </div>
          <div className={classNames.btnEdit}>
            <img src={edit} alt="" className={classNames.btnIcon} />
          </div>
          <div className={classNames.btnTxn}>
            <img src={txnHash} alt="" className={classNames.btnIcon} />
          </div>
          <div className={classNames.btnChat}>
            <img src={chat} alt="" className={classNames.btnIcon} />
          </div>
        </div>
        <div className={classNames.credit}>
          <span className={classNames.expand}>Expand</span>
          <span className={classNames.value}>
            {FormatCurrency(
              txn?.deposit
                ? assetClass.name === 'fiat'
                  ? txn?.amt_credited
                  : txn.amount
                : 0,
              txn?.coin
            )}
          </span>
        </div>
        <div className={classNames.debit}>
          <span className={classNames.expand}>Expand</span>
          <span className={classNames.value}>
            {FormatCurrency(
              txn?.withdraw
                ? assetClass.name === 'fiat'
                  ? txn?.amt_credited
                  : txn.amount
                : 0,
              txn?.coin
            )}
          </span>
        </div>
        <div className={classNames.balance}>
          <span className={classNames.expand}>Expand</span>
          <span className={classNames.value}>
            {FormatCurrency(txn?.updated_balance, txn?.coin)}
          </span>
        </div>
      </div>
      {txnOpen && <TransactionExpand txn={txn} forwardedRef={elRefs} />}
    </div>
  );
}
function VaultItemBonds({ txn }) {
  const [txnOpen, setTxnOpen] = useState(false);

  const elRefs = useRef([]);
  if (elRefs.current.length !== 3) {
    // add or remove refs
    elRefs.current = Array(3)
      .fill()
      .map((_, i) => elRefs.current[i] || createRef());
  }

  const refOut = useRef();
  OnOutsideClick(
    undefined,
    () => {
      setTxnOpen((txnOpen) => !txnOpen);
    },
    refOut,
    elRefs
  );

  return (
    <div
      className={`${classNames.vaultItmWrap} ${classNames[txnOpen]}`}
      ref={refOut}
    >
      <div className={classNames.vaultsItm}>
        <img
          src={txn?.withdraw ? credit : debit}
          alt=""
          className={classNames.img}
        />
        <div className={classNames.nameDate}>
          <div className={classNames.name}>
            {txn?.withdraw
              ? `Withdrawal Of ${txn.coin} Earnings`
              : `${txn.coin} Bond Interest Payment`}
          </div>
          <div className={classNames.date}>
            {moment(txn.timestamp).format('MMMM Do YYYY [at] h:mm:ss A zz')}
          </div>
        </div>
        <div className={classNames.buttons} ref={elRefs.current[0]}>
          <div className={classNames.btnNotes}>
            <span>Notes</span>
          </div>
          <div className={classNames.btnEdit}>
            <img src={edit} alt="" className={classNames.btnIcon} />
          </div>
          <div className={classNames.btnTxn}>
            <img src={txnHash} alt="" className={classNames.btnIcon} />
          </div>
          <div className={classNames.btnChat}>
            <img src={chat} alt="" className={classNames.btnIcon} />
          </div>
        </div>
        <div className={classNames.credit}>
          <span className={classNames.expand}>Expand</span>
          <span className={classNames.value}>
            {FormatCurrency(
              txn?.deposit ? txn?.credited_interest : 0,
              txn?.coin
            )}
          </span>
        </div>
        <div className={classNames.debit}>
          <span className={classNames.expand}>Expand</span>
          <span className={classNames.value}>
            {FormatCurrency(txn?.withdraw ? txn?.amount : 0, txn?.coin)}
          </span>
        </div>
        <div className={classNames.balance}>
          <span className={classNames.expand}>Expand</span>
          <span className={classNames.value}>
            {FormatCurrency(txn?.updated_vault_balance, txn?.coin)}
          </span>
        </div>
      </div>
    </div>
  );
}
function VaultItemMoneyMarkets({ txn }) {
  const [txnOpen, setTxnOpen] = useState(false);

  const elRefs = useRef([]);
  if (elRefs.current.length !== 3) {
    // add or remove refs
    elRefs.current = Array(3)
      .fill()
      .map((_, i) => elRefs.current[i] || createRef());
  }

  const refOut = useRef();
  OnOutsideClick(
    undefined,
    () => {
      setTxnOpen((txnOpen) => !txnOpen);
    },
    refOut,
    elRefs
  );

  return (
    <div
      className={`${classNames.vaultItmWrap} ${classNames[txnOpen]}`}
      ref={refOut}
    >
      <div className={classNames.vaultsItm}>
        <img
          src={txn?.withdraw ? credit : debit}
          alt=""
          className={classNames.img}
        />
        <div className={classNames.nameDate}>
          <div className={classNames.name}>
            {txn?.withdraw
              ? `Withdrawal Of ${txn.coin} Earnings`
              : `${txn.coin} Liquid Interest Payment`}
          </div>
          <div className={classNames.date}>
            {moment(txn.timestamp).format('MMMM Do YYYY [at] h:mm:ss A zz')}
          </div>
        </div>
        <div className={classNames.buttons} ref={elRefs.current[0]}>
          <div className={classNames.btnNotes}>
            <span>Notes</span>
          </div>
          <div className={classNames.btnEdit}>
            <img src={edit} alt="" className={classNames.btnIcon} />
          </div>
          <div className={classNames.btnTxn}>
            <img src={txnHash} alt="" className={classNames.btnIcon} />
          </div>
          <div className={classNames.btnChat}>
            <img src={chat} alt="" className={classNames.btnIcon} />
          </div>
        </div>
        <div className={classNames.credit}>
          <span className={classNames.expand}>Expand</span>
          <span className={classNames.value}>
            {FormatCurrency(
              txn?.deposit ? txn?.credited_interest : 0,
              txn?.coin
            )}
          </span>
        </div>
        <div className={classNames.debit}>
          <span className={classNames.expand}>Expand</span>
          <span className={classNames.value}>
            {FormatCurrency(txn?.withdraw ? txn?.amount : 0, txn?.coin)}
          </span>
        </div>
        <div className={classNames.balance}>
          <span className={classNames.expand}>Expand</span>
          <span className={classNames.value}>
            {FormatCurrency(txn?.updated_interest, txn?.coin)}
          </span>
        </div>
      </div>
    </div>
  );
}

function TransactionExpand({ txn, forwardedRef }) {
  const { vaultSelected } = useContext(VaultPageContext);
  const { email } = useContext(BankContext);
  const [isLiveRate, setIsLiveRate] = useState(true);
  const { data: txnDetails, isLoading: txnDetailLoading } = useVaultTxnDetails(
    txn?.identifier,
    email,
    txn.deposit
  );
  return (
    <div className={classNames.vaultsExpand}>
      <hr />
      <div className={classNames.viewInAnother}>
        <div className={classNames.viewCurrency}>
          <span className={classNames.view}>View In Another Currency</span>
          <div className={classNames.btnCoin} ref={forwardedRef.current[1]}>
            <img src={indIcon} alt="" className={classNames.btnIcon} />
            <span>{DISPLAY_CURRENCY}</span>
          </div>
          <div className={classNames.rateLiveHistorical}>
            <span className={classNames[isLiveRate]}>Live Rate</span>
            <div
              className={classNames.btnOutline}
              ref={forwardedRef.current[2]}
              style={{ opacity: 0.5 }}
              // onClick={() => setIsLiveRate(!isLiveRate)}
            >
              <div
                className={`${classNames.btnBall} ${classNames[!isLiveRate]}`}
              />
            </div>
            <span className={classNames[!isLiveRate]}>Historical Rate</span>
          </div>
        </div>
        <div className={classNames.credit}>
          <span className={classNames.value}>
            &#x20b9;
            {FormatCurrency(
              txn?.deposit
                ? txn?.amount * vaultSelected?.price?.[DISPLAY_CURRENCY]
                : 0,
              DISPLAY_CURRENCY
            )}
          </span>
        </div>
        <div className={classNames.debit}>
          <span className={classNames.value}>
            &#x20b9; {/* Rupee Sign */}
            {FormatCurrency(
              txn?.withdraw
                ? txn?.amount * vaultSelected?.price?.[DISPLAY_CURRENCY]
                : 0,
              DISPLAY_CURRENCY
            )}
          </span>
        </div>
        <div className={classNames.balance}>
          <span className={classNames.value}>
            &#x20b9;
            {FormatCurrency(
              txn?.updated_balance * vaultSelected?.price?.[DISPLAY_CURRENCY],
              DISPLAY_CURRENCY
            )}
          </span>
        </div>
      </div>
      <hr />
      {txnDetailLoading ? (
        <div className={classNames.txnType}>
          <Skeleton width={240} className={classNames.text} />
          <Skeleton width={240} className={classNames.txnIcon} />
        </div>
      ) : (
        <div className={classNames.txnType}>
          <div className={classNames.text}>Transaction Type</div>
          <img
            src={txnDetails?.type === 'gx_transfer' ? connect : tokenSwap}
            alt=""
            className={classNames.txnIcon}
          />
        </div>
      )}
      <hr />
      <div className={classNames.from}>
        <div className={classNames.textFromTo}>From</div>
        <div className={classNames.txnFromTo}>
          <UserCard user={txnDetails?.fromUser} loading={txnDetailLoading} />
          <div className={classNames.credit}>
            <span className={classNames.value}>
              {FormatCurrency(
                txn?.withdraw ? 0 : txnDetails?.from_txn_data?.amount,
                txnDetails?.from_txn_data?.coin
              )}
            </span>
          </div>
          <div className={classNames.debit}>
            <span className={classNames.value}>
              {FormatCurrency(
                txn?.deposit ? 0 : txnDetails?.from_txn_data?.amount,
                txnDetails?.from_txn_data?.coin
              )}
            </span>
          </div>
          <div className={classNames.balance}>
            <span className={classNames.value}>
              {txn?.withdraw
                ? FormatCurrency(
                    txnDetails?.from_txn_data?.updated_balance,
                    txnDetails?.from_txn_data?.coin
                  )
                : ''}
            </span>
          </div>
        </div>
      </div>
      <div className={classNames.to}>
        <div className={classNames.textFromTo}>To</div>
        <div className={classNames.txnFromTo}>
          <UserCard user={txnDetails?.toUser} loading={txnDetailLoading} />
          <div className={classNames.credit}>
            <span className={classNames.value}>
              {FormatCurrency(
                txn?.deposit ? 0 : txnDetails?.to_txn_data?.amount,
                txnDetails?.to_txn_data?.coin
              )}
            </span>
          </div>
          <div className={classNames.debit}>
            <span className={classNames.value}>
              {FormatCurrency(
                txn?.withdraw ? 0 : txnDetails?.to_txn_data?.amount,
                txnDetails?.to_txn_data?.coin
              )}
            </span>
          </div>
          <div className={classNames.balance}>
            <span className={classNames.value}>
              {txn?.deposit
                ? FormatCurrency(
                    txnDetails?.to_txn_data?.updated_balance,
                    txnDetails?.to_txn_data?.coin
                  )
                : ''}
            </span>
          </div>
        </div>
      </div>
    </div>
  );
}

function UserCard({ user, loading }) {
  const { coinListObject } = useContext(BankContext);
  const { data: userDetail, isLoading: userDeatailLoading } = useUserDetails(
    user?.email
  );
  return (
    <div className={classNames.userCard}>
      {userDeatailLoading || loading ? (
        <>
          <div className={classNames.user}>
            <Skeleton circle className={classNames.dp} />
            <div className={classNames.nameAppCode}>
              <Skeleton className={classNames.name} width={200} />
              <Skeleton className={classNames.appCode} width={160} />
            </div>
          </div>
          <Skeleton className={classNames.liquidity} width={150} />
          <div className={classNames.coin}>
            <Skeleton circle className={classNames.coinIcon} />
            <Skeleton className={classNames.appCode} width={160} />
          </div>
        </>
      ) : (
        <>
          <div className={classNames.user}>
            <img
              src={userDetail?.profile_img}
              alt=""
              className={classNames.dp}
            />
            <div className={classNames.nameAppCode}>
              <div className={classNames.name}>{userDetail?.name}</div>
              <div className={classNames.appCode}>{user?.app_code}</div>
            </div>
          </div>
          <div className={classNames.liquidity}>Liquid</div>
          <div className={classNames.coin}>
            <img
              src={coinListObject[user?.coin]?.coinImage}
              alt=""
              className={classNames.coinIcon}
            />
            <span>{user?.coin}</span>
          </div>
        </>
      )}
    </div>
  );
}

export default VaultsPage;
