import { faCopy } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Axios from 'axios';
import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';

import { ReactComponent as Logo } from '../static/images/logoMobile.svg';
import android from '../static/images/android.svg';
import ios from '../static/images/ios.svg';
import phone from '../static/images/clipIcons/phone.svg';
import mail from '../static/images/clipIcons/mail.svg';
import useWindowDimensions from '../utils/WindowSize';
import SendAppLink from '../components/SendAppLink/SendAppLink';
import { APP_CODE, MAIN_LOGO } from '../config/appConfig';

function GetMobileAppPage() {
  const { height } = useWindowDimensions();
  const [step, setStep] = useState();
  const [platform, setPlatform] = useState('');
  const [resLink, setResLink] = useState('');
  const [copied, setCopied] = useState(false);
  const [type, setType] = useState();
  useEffect(() => {
    Axios.get(
      `https://comms.globalxchange.com/gxb/apps/mobile/app/links/logs/get?app_code=${APP_CODE}`
    ).then(({ data }) => {
      setResLink(data.logs[0]);
    });
  }, []);

  function getContent() {
    switch (step) {
      case 0:
        return (
          <div className="getAppContent">
            <div class="mainTitle">Select Device</div>
            <div
              className="btnItem"
              onClick={() => {
                setStep(1);
                setPlatform('android');
              }}
            >
              <img src={android} alt="" />
              <span>Android</span>
            </div>
            <div
              className="btnItem"
              onClick={() => {
                setStep(1);
                setPlatform('ios');
              }}
            >
              <img src={ios} alt="" />
              <span>iPhone</span>
            </div>
          </div>
        );
      case 1:
        return (
          <div className="getAppContent">
            <div class="mainTitle">Next Step</div>
            <div
              className="btnItem"
              onClick={() => {
                setStep(2);
              }}
            >
              <span>Download App On This Device</span>
            </div>
            <div
              className="btnItem"
              onClick={() => {
                setStep(4);
              }}
            >
              <span>Send Me The Download Link</span>
            </div>
          </div>
        );
      case 2:
        return (
          <div className="getAppContent">
            <div class="mainTitle">Download App On This Phone</div>
            <div
              className="btnItem"
              onClick={() => {
                setStep(3);
              }}
            >
              <span>Copy Download Link</span>
            </div>
            <a
              href={resLink && resLink[`${platform}_app_link`]}
              target="_blank"
              rel="noopener noreferrer"
              className="btnItem"
            >
              <span>Open Download Link</span>
            </a>
          </div>
        );
      case 3:
        return (
          <div className="getAppContent">
            <div class="mainTitle">Download Link</div>
            <div
              className="btnItem"
              onClick={() => {
                navigator.clipboard
                  .writeText(resLink && resLink[`${platform}_app_link`])
                  .then(() => {
                    setCopied(true);
                    setTimeout(() => {
                      setCopied(false);
                    }, 2000);
                  });
              }}
            >
              <span>
                {copied
                  ? 'Copied To Clipboard'
                  : resLink && resLink[`${platform}_app_link`]}
              </span>
              <FontAwesomeIcon icon={faCopy} />
            </div>
          </div>
        );
      case 4:
        return (
          <div className="getAppContent">
            <div class="mainTitle">Select Method</div>
            <div
              className="btnItem"
              onClick={() => {
                setStep(5);
                setType('phone');
              }}
            >
              <img src={phone} alt="" />
              <span>Text Me The Link</span>
            </div>
            <div
              className="btnItem"
              onClick={() => {
                setStep(5);
                setType('mail');
              }}
            >
              <img src={mail} alt="" />
              <span>Email Me The Link</span>
            </div>
          </div>
        );
      case 5:
        return <SendAppLink platform={platform} type={type} />;
      default:
        return (
          <div className="home">
            <Logo className="logo" />
            <div className="text">
              Please Download The Mobile App For The Best Trading Experience.
              Full Clients Are Also Available On Destop Version Of This Website
            </div>
            <div className="btGetApp" onClick={() => setStep(0)}>
              Get Mobile App
            </div>
          </div>
        );
    }
  }
  return (
    <div className="getMobileApp" style={{ height }}>
      {step !== undefined && (
        <div className="navbar">
          <img src={MAIN_LOGO} alt="" className="logo" />
        </div>
      )}
      {getContent()}
    </div>
  );
}

export default GetMobileAppPage;
