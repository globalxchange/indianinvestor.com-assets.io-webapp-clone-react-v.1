import React from 'react';
import MainLayout from '../Layout/ChatLayout/MainLayout';
import { MOBILE_LOGO, ICON_LOGO } from '../config/appConfig';
import markets from '../static/images/clipIcons/markets.svg';
import mobileIcon from '../static/images/clipIcons/mobileIcon.svg';
import { Link, useHistory } from 'react-router-dom';

function HomeMobile() {
  const history = useHistory();
  return (
    <MainLayout className="homeMobile">
      <div className="head">
        <img src={MOBILE_LOGO} alt="" />
      </div>
      <div className="btns">
        <div
          className="btnIdea"
          onClick={() => {
            history.push('/app');
          }}
        >
          <img src={ICON_LOGO} alt="" /> For Earners
        </div>
        <div
          className="btnTalk"
          onClick={() => {
            history.push('/app');
          }}
        >
          <img src={markets} alt="" /> For Merchants
        </div>
      </div>
      <Link to="/getapp" className="footer">
        <img src={mobileIcon} alt="" />
        Download Mobile App
      </Link>
    </MainLayout>
  );
}

export default HomeMobile;
