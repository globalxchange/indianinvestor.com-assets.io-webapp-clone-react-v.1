import React from 'react';
import classNames from './taxChainsPage.module.scss';
import Layout from '../../Layout/Layout';
import retired from '../../static/images/sidebarFullLogos/taxChains.svg';

function TaxChainsPage() {
  return (
    <Layout
      active={`taxchains`}
      className={classNames.taxChainsPage}
      hideFooter
    >
      <div className={classNames.content}>
        <img src={retired} alt="" className={classNames.logo} />
      </div>
      <div className={classNames.footer}>
        TaxChains Is Coming To IndianInvestor.com
      </div>
    </Layout>
  );
}

export default TaxChainsPage;
