import React, {
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';
import Skeleton from 'react-loading-skeleton';
import { Link, useHistory, useParams } from 'react-router-dom';
import classNames from './marketsPage.module.scss';
import clock from '../../static/images/marketsSidebar/clock.svg';
import tradeStream from '../../static/images/sidebarIcons/tradeStream.svg';
import searchIcon from '../../static/images/search.svg';
import angleDown from '../../static/images/angleDown.svg';
import {
  useBankerList,
  useCustomBondsList,
  useMarketCoinsList,
  useMMList,
  useShareTokensDetail,
  useShareTokensList,
  useUserDetails,
} from '../../queryHooks';
import { ASSET_CLASSES, SIDEBAR_FOOTERS } from '../../config/constants';
import { BankContext } from '../../context/Context';
import { ICON_LOGO } from '../../config/appConfig';
import {
  FormatCurrency,
  FormatNumber,
  GetSortOrder,
} from '../../utils/FunctionTools';
import MarketAssetDetail from '../../components/MarketAssetDetail';
import TerminalsSidebar from '../../components/TerminalsSidebar';
import SelectBondAssetModal from '../../components/SelectBondAssetModal';
import doubleTik from '../../static/images/clipIcons/doubleTik.svg';
import BondDetailModal from '../../components/BondDetailModal';
import SelectBondIssuerModal from '../../components/SelectBondIssuerModal';
import LoginModal from '../../components/LoginModalNew';
import { DISPLAY_CURRENCY, DISPLAY_CURRENCY_SYM } from '../../config';

function MarketsPage() {
  const history = useHistory();
  const { assetClassName, coin } = useParams();
  const [assetClass, setAssetClass] = useState(ASSET_CLASSES[0]);
  const [index, setIndex] = useState(2);
  const { profileImg, email } = useContext(BankContext);
  const [streamOpen, setStreamOpen] = useState(false);
  const [coinSelected, setCoinSelected] = useState();
  const [search, setSearch] = useState('');
  const ref = useRef();

  const { data: userDetail, isLoading: userDeatailLoading } =
    useUserDetails(email);

  const { data: shareToken } = useShareTokensDetail(coin);

  useEffect(() => {
    const classSelected = ASSET_CLASSES.filter(
      (asCls) => asCls.name === assetClassName
    );
    if (classSelected[0]) setAssetClass(classSelected[0]);
  }, [assetClassName]);

  useEffect(() => {
    if (assetClassName === 'shares' && shareToken?.token_profile_data) {
      setCoinSelected(shareToken?.token_profile_data);
    }
  }, [coin, shareToken, assetClassName]);

  useEffect(() => {
    ref.current.addEventListener('scroll', scrollHandle);
    return () => {
      ref.current.removeEventListener('scroll', scrollHandle);
    };
  }, []);
  const [active, setActive] = useState(false);
  const scrollHandle = (event) => {
    setActive(
      Boolean(
        event?.path[0]?.scrollTop >=
          event?.path[0].childNodes[1].childNodes[0].childNodes[1]?.offsetTop
      )
    );
  };

  function selectTable() {
    switch (assetClass?.name) {
      case 'crypto':
        return (
          <MarketTableCrypto
            streamOpen={streamOpen}
            setCoinSelected={setCoinSelected}
            assetClass={assetClass}
            active={active}
            search={search}
          />
        );
      case 'fiat':
        return (
          <MarketTableForex
            streamOpen={streamOpen}
            setCoinSelected={setCoinSelected}
            assetClass={assetClass}
            search={search}
          />
        );
      case 'shares':
        return (
          <MarketTableShares
            streamOpen={streamOpen}
            setCoinSelected={setCoinSelected}
            assetClass={assetClass}
            search={search}
          />
        );
      case 'bonds':
        return (
          <MarketTableBonds
            streamOpen={streamOpen}
            setCoinSelected={setCoinSelected}
            assetClass={assetClass}
            active={active}
            search={search}
          />
        );
      case 'moneyMarkets':
        return (
          <MarketTableMoneyMarkets
            streamOpen={streamOpen}
            setCoinSelected={setCoinSelected}
            assetClass={assetClass}
            active={active}
            search={search}
          />
        );

      default:
        return '';
    }
  }

  const [loginModalOpen, setLoginModalOpen] = useState(false);

  return (
    <div className={classNames.marketsPage}>
      <div className={classNames.pageWrap} ref={ref}>
        <div className={classNames.assetClassWrap}>
          <div className={classNames.profileSearch}>
            <Link to={email ? '/app' : '/'} className={classNames.profile}>
              <img src={userDetail?.profile_img || ICON_LOGO} alt="" />
            </Link>
            <label className={classNames.assetFinder}>
              <img src={searchIcon} alt="" />
              <input
                type="text"
                placeholder="Asset Finder"
                value={search}
                onChange={(e) => setSearch(e.target.value)}
              />
            </label>
          </div>
          <div className={classNames.assetClass}>
            {ASSET_CLASSES
              // .filter(
              //   (asset) =>
              //     (asset?.label?.toLowerCase() || '').includes(
              //       search?.toLowerCase()
              //     ) ||
              //     (asset?.name?.toLowerCase() || '').includes(search.toLowerCase)
              // )
              .sort(GetSortOrder('marketOrder'))
              .map((assetClassCard, i) => (
                <div
                  key={i}
                  className={`${classNames.assetClassCard} ${
                    classNames[assetClassCard === assetClass]
                  } ${classNames}`}
                  onClick={() => {
                    if (assetClassCard.marketEnable) {
                      setAssetClass(assetClassCard);
                      history.push(`/market/${assetClassCard.name}`);
                    }
                  }}
                >
                  <img
                    src={assetClassCard.icon}
                    alt=""
                    className={classNames.icon}
                  />
                </div>
              ))}
          </div>
          <div
            className={`${classNames.streamOpen} ${
              classNames[Boolean(streamOpen)]
            }`}
            onClick={() => setStreamOpen((prev) => !prev)}
          >
            {streamOpen ? (
              <svg
                viewBox="0 0 18 18"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M10.6317 9L17.6617 1.96993C18.1127 1.51912 18.1128 0.789171 17.6618 0.338256C17.2109 -0.112694 16.481 -0.112799 16.0301 0.338221L9 7.36829L1.96993 0.338256C1.51913 -0.112658 0.78914 -0.112799 0.338226 0.338186C-0.112724 0.789101 -0.112759 1.51895 0.338226 1.9699L7.36822 9L0.33819 16.03C-0.111705 16.4799 -0.111705 17.2119 0.33819 17.6617C0.78907 18.1127 1.51898 18.1128 1.9699 17.6617L8.99993 10.6316L16.03 17.6617C16.4808 18.1126 17.2108 18.1127 17.6616 17.6617C18.1126 17.2108 18.1127 16.4809 17.6617 16.0299L10.6317 9Z"
                  fill="#1A2663"
                />
                <path
                  d="M10.6317 9L17.6617 1.96993C18.1127 1.51912 18.1128 0.789171 17.6618 0.338256C17.2109 -0.112693 16.481 -0.112799 16.0301 0.338221L9 7.36825V10.6317L16.03 17.6618C16.4808 18.1127 17.2108 18.1128 17.6617 17.6618C18.1127 17.2109 18.1128 16.481 17.6617 16.03L10.6317 9Z"
                  fill="#1A2663"
                />
              </svg>
            ) : (
              <img src={tradeStream} alt="" />
            )}
          </div>
        </div>
        <div className={classNames.pageContent}>
          {coinSelected?.coinSymbol || coin ? (
            <MarketAssetDetail
              coin={coinSelected}
              coinSymbol={coin}
              onClose={() => {
                setCoinSelected();
                history.push(`/market/${assetClass.name}`);
              }}
              assetClass={assetClass}
              streamOpen={streamOpen}
              setStreamOpen={setStreamOpen}
            />
          ) : (
            <div className={classNames.tableArea}>
              <div className={classNames.topBlank}>
                <div className={classNames.assetClassCard}>
                  <img
                    src={assetClass.icon}
                    alt={assetClass.name}
                    className={classNames.assetLogo}
                  />
                  <div className={classNames.assetActions}>
                    {/* <div className={classNames.action}>Learn More</div> */}
                    <div
                      className={classNames.action}
                      onClick={() => !email && setLoginModalOpen(true)}
                    >
                      Buy {assetClass.label}
                    </div>
                    <div className={classNames.action}>Speak To An Advisor</div>
                  </div>
                </div>
                <div className={classNames.cardList}>
                  {Array(5)
                    .fill('')
                    .map(() => (
                      <div className={classNames.card}></div>
                    ))}
                </div>
              </div>
              {selectTable()}
            </div>
          )}
        </div>
      </div>
      <div
        className={`${classNames.sidebarArea} ${
          classNames[Boolean(streamOpen)]
        }`}
      >
        <div className={classNames.sidebarContent}>
          {assetClassName === 'shares' && coin ? (
            <TerminalsSidebar
              shareToken={shareToken}
              streamOpen={streamOpen}
              setStreamOpen={setStreamOpen}
            />
          ) : (
            <img
              className={classNames.fullLogo}
              src={SIDEBAR_FOOTERS[index].fullLogo}
              alt=""
            />
          )}
        </div>

        {assetClassName === 'shares' && coin ? (
          ''
        ) : (
          <div className={classNames.sidebarFooter}>
            {SIDEBAR_FOOTERS.map((menu, i) => (
              <div
                className={`${classNames.footerMenu} ${
                  classNames[i === index]
                } ${menu.disable && classNames.disable}`}
                onClick={() => !menu.disable && setIndex(i)}
              >
                <img src={menu.icon} alt="" />
                <span>{menu.label}</span>
              </div>
            ))}
          </div>
        )}
      </div>
      {loginModalOpen && (
        <LoginModal
          onClose={() => setLoginModalOpen(false)}
          onSuccess={() => setLoginModalOpen(false)}
        />
      )}
    </div>
  );
}

function MarketTableCrypto({
  streamOpen,
  setCoinSelected,
  assetClass,
  active,
  search,
}) {
  const headRef = useRef();
  const { data: cryptoList = [], isLoading } = useMarketCoinsList('crypto');
  // const [search, setSearch] = useState('');
  const [filterOpen, setFilterOpen] = useState(false);
  const history = useHistory();

  return (
    <div className={classNames.marketTable}>
      <div
        className={`${classNames.header} ${classNames[!streamOpen]} ${
          classNames[active ? 'active' : '']
        }`}
        ref={headRef}
      >
        <div className={classNames.assets}>Asset</div>
        <div className={classNames.price}>Price</div>
        <div className={classNames.roiPercent}>24 Hr Change</div>
        <div className={classNames.marCap}>Market Cap</div>
        <div className={classNames.volume}>Trading Volume</div>
        <div className={classNames.supply}>Supply</div>
        <img
          className={`${classNames.btnFilter} ${
            classNames[filterOpen.toString()]
          }`}
          src={angleDown}
          alt=""
          onClick={() => setFilterOpen(!filterOpen)}
        />
      </div>
      {filterOpen && (
        <div className={classNames.filtersWrap}>
          <div className={classNames.filters}>
            <div className={classNames.filter}>
              <img src={clock} alt="" />
              <span>24 Hrs</span>
            </div>
            <div className={classNames.filter}>
              <img
                src={
                  'https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/rupee.png'
                }
                alt=""
              />
              <span>{DISPLAY_CURRENCY}</span>
            </div>
            <div className={classNames.filter}>
              + <span>Add Filter</span>
            </div>
          </div>
          {/* <input
            type="text"
            className={classNames.input}
            placeholder="Search Cryptocurrencies...."
            value={search}
            onChange={(e) => setSearch(e.target.value)}
          /> */}
        </div>
      )}
      <div className={classNames.marketsList}>
        {isLoading
          ? Array(8)
              .fill('')
              .map((_, i) => (
                <div
                  className={`${classNames.marketItem}  ${
                    classNames[!streamOpen]
                  }`}
                  key={i}
                >
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                </div>
              ))
          : cryptoList
              ?.filter(
                (coin) =>
                  coin.coinName.toLowerCase().includes(search.toLowerCase()) ||
                  coin.coinSymbol.toLowerCase().includes(search.toLowerCase())
              )
              ?.map((coin, i) => (
                <div
                  className={`${classNames.marketItem}  ${
                    classNames[!streamOpen]
                  }`}
                  key={coin._id}
                  onClick={() => {
                    setCoinSelected(coin);
                    history.push(
                      `/market/${assetClass.name}/${coin.coinSymbol}`
                    );
                  }}
                >
                  <div className={classNames.assets}>
                    <img src={coin.coinImage} alt="" />
                    <span className={classNames.name}>{coin.coinName}</span>
                  </div>
                  <div className={classNames.price}>
                    {DISPLAY_CURRENCY_SYM}
                    {FormatCurrency(
                      coin?.price?.[DISPLAY_CURRENCY],
                      DISPLAY_CURRENCY
                    )}
                  </div>
                  <div
                    className={`${classNames.roiPercent} ${
                      classNames[coin?._24hrchange < 0]
                    }`}
                  >
                    {FormatNumber(coin?._24hrchange, 2)}%
                  </div>
                  {/* <div
                    className={`${classNames.roiInr} ${
                      classNames[coin?._24hrchangeDC < 0]
                    }`}
                  >
                    {DISPLAY_CURRENCY_SYM}{FormatCurrency(coin?._24hrchangeDC, DISPLAY_CURRENCY)}
                  </div> */}
                  <div className={classNames.marCap}>
                    {DISPLAY_CURRENCY_SYM}
                    {FormatCurrency(coin?.mkt_cap_DC, DISPLAY_CURRENCY)}
                  </div>
                  <div className={classNames.volume}>
                    {DISPLAY_CURRENCY_SYM}
                    {FormatCurrency(coin?.volume24hr_DC, DISPLAY_CURRENCY)}
                  </div>
                  <div className={classNames.supply}>
                    {FormatNumber(coin?.total_supply)}&nbsp;
                    {coin.coinSymbol}
                  </div>
                </div>
              ))}
      </div>
    </div>
  );
}

function MarketTableForex({ streamOpen, setCoinSelected, assetClass, search }) {
  const history = useHistory();
  const { data: fiatList = [], isLoading } = useMarketCoinsList('fiat');
  // const [search, setSearch] = useState('');
  const [filterOpen, setFilterOpen] = useState(false);
  return (
    <div className={classNames.marketTable}>
      <div className={`${classNames.header} ${classNames[!streamOpen]}`}>
        <div className={classNames.assets}>Asset</div>
        <div className={classNames.price}>Price</div>
        <div className={classNames.roiPercent}>24 Hr Change</div>
        <div className={classNames.volume}>Trading Volume</div>
        <img
          className={`${classNames.btnFilter} ${
            classNames[filterOpen.toString()]
          }`}
          src={angleDown}
          alt=""
          onClick={() => setFilterOpen(!filterOpen)}
        />
      </div>
      {filterOpen && (
        <div className={classNames.filtersWrap}>
          <div className={classNames.filters}>
            <div className={classNames.filter}>
              <img src={clock} alt="" />
              <span>24 Hrs</span>
            </div>
            <div className={classNames.filter}>
              <img
                src={
                  'https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/rupee.png'
                }
                alt=""
              />
              <span>{DISPLAY_CURRENCY}</span>
            </div>
            <div className={classNames.filter}>
              + <span>Add Filter</span>
            </div>
          </div>
          {/* <input
            type="text"
            className={classNames.input}
            placeholder="Search Forex Currencies...."
            value={search}
            onChange={(e) => setSearch(e.target.value)}
          /> */}
        </div>
      )}
      <div className={classNames.marketsList}>
        {isLoading
          ? Array(8)
              .fill('')
              .map((_, i) => (
                <div
                  className={`${classNames.marketItem}  ${
                    classNames[!Boolean(streamOpen)]
                  }`}
                  key={i}
                >
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                </div>
              ))
          : fiatList
              ?.filter(
                (coin) =>
                  coin.coinName.toLowerCase().includes(search.toLowerCase()) ||
                  coin.coinSymbol.toLowerCase().includes(search.toLowerCase())
              )
              .map((coin, i) => (
                <div
                  className={`${classNames.marketItem}  ${
                    classNames[!Boolean(streamOpen)]
                  }`}
                  key={coin._id}
                  onClick={() => {
                    setCoinSelected(coin);
                    history.push(
                      `/market/${assetClass.name}/${coin.coinSymbol}`
                    );
                  }}
                >
                  <div className={classNames.assets}>
                    <img src={coin.coinImage} alt="" />
                    <span className={classNames.name}>{coin.coinName}</span>
                  </div>
                  <div className={classNames.price}>
                    {DISPLAY_CURRENCY_SYM}
                    {FormatCurrency(
                      coin?.price?.[DISPLAY_CURRENCY],
                      DISPLAY_CURRENCY
                    )}
                  </div>
                  <div
                    className={`${classNames.roiPercent} ${
                      classNames[coin?._24hrchange < 0]
                    }`}
                  >
                    {FormatNumber(coin?._24hrchange, 2)}%
                  </div>
                  <div className={classNames.volume}>
                    {DISPLAY_CURRENCY_SYM}
                    {FormatCurrency(coin?.volume24hr_DC, DISPLAY_CURRENCY)}
                  </div>
                </div>
              ))}
      </div>
    </div>
  );
}

function MarketTableShares({
  streamOpen,
  setCoinSelected,
  assetClass,
  search,
}) {
  const history = useHistory();
  const { data: sharesList = [], isLoading } = useShareTokensList();
  // const [search, setSearch] = useState('');
  const [filterOpen, setFilterOpen] = useState(false);
  return (
    <div className={classNames.marketTable}>
      <div
        className={`${classNames.header} ${classNames[!Boolean(streamOpen)]}`}
      >
        <div className={classNames.assets}>Asset</div>
        <div className={classNames.price}>Price</div>
        <div className={classNames.roiPercent}>24 Hr Change</div>
        <div className={classNames.supply}>Supply</div>
        <div className={classNames.marCap}>Market Cap</div>
        <div className={classNames.volume}>Round</div>
        <img
          className={`${classNames.btnFilter} ${
            classNames[filterOpen.toString()]
          }`}
          src={angleDown}
          alt=""
          onClick={() => setFilterOpen(!filterOpen)}
        />
      </div>
      {filterOpen && (
        <div className={classNames.filtersWrap}>
          <div className={classNames.filters}>
            <div className={classNames.filter}>
              <img src={clock} alt="" />
              <span>24 Hrs</span>
            </div>
            <div className={classNames.filter}>
              <img
                src={
                  'https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/rupee.png'
                }
                alt=""
              />
              <span>{DISPLAY_CURRENCY}</span>
            </div>
            <div className={classNames.filter}>
              + <span>Add Filter</span>
            </div>
          </div>
          {/* <input
            type="text"
            className={classNames.input}
            placeholder="Search ShareTokens...."
            value={search}
            onChange={(e) => setSearch(e.target.value)}
          /> */}
        </div>
      )}
      <div className={classNames.marketsList}>
        {isLoading
          ? Array(8)
              .fill('')
              .map((_, i) => (
                <div
                  className={`${classNames.marketItem}  ${
                    classNames[!Boolean(streamOpen)]
                  }`}
                  key={i}
                >
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                </div>
              ))
          : sharesList
              ?.filter(
                (path) =>
                  path?.token_profile_data?.coinSymbol
                    ?.toLowerCase()
                    .includes(search.toLowerCase()) ||
                  path?.token_profile_data?.coinName
                    ?.toLowerCase()
                    .includes(search.toLowerCase())
              )
              ?.map((path, i) => (
                <div
                  className={`${classNames.marketItem}  ${
                    classNames[!Boolean(streamOpen)]
                  }`}
                  key={path._id}
                  onClick={() => {
                    setCoinSelected({
                      coinSymbol: path?.token_profile_data?.coinSymbol,
                      coinName: path?.token_profile_data?.coinName,
                      coinImage: path?.token_profile_data?.coinImage,
                    });
                    history.push(
                      `/market/${assetClass.name}/${path?.token_profile_data?.coinSymbol}`
                    );
                  }}
                >
                  <div className={classNames.assets}>
                    <img src={path?.token_profile_data?.coinImage} alt="" />
                    <div className={classNames.names}>
                      <span className={classNames.name}>
                        {path?.token_profile_data?.coinName}
                      </span>
                      <div className={classNames.symbol}>
                        {path?.token_profile_data?.coinSymbol}
                        <img src={path?.asset_metaData?.coinImage} alt="" />
                        <strong>{path?.asset_metaData?.coinName}</strong>
                      </div>
                    </div>
                  </div>
                  <div className={classNames.price}>
                    {DISPLAY_CURRENCY_SYM}
                    {FormatCurrency(path?.token_price, DISPLAY_CURRENCY)}
                  </div>
                  <div
                    className={`${classNames.roiPercent} ${classNames[0 < 0]}`}
                    style={{ opacity: 0.4 }}
                  >
                    {FormatNumber(0, 2)}%
                  </div>
                  <div className={classNames.supply}>
                    {FormatNumber(path?.token_totalSupply)}&nbsp;
                  </div>
                  <div className={classNames.marCap}>
                    {DISPLAY_CURRENCY_SYM}
                    {FormatCurrency(path?.asset_balance, DISPLAY_CURRENCY)}
                  </div>
                  <div className={classNames.volume}>
                    <div className={classNames.value}>
                      {path?.pathSeries && path?.pathSeries[0]?.seriesName}
                    </div>
                    <div className={classNames.symbol}>
                      {path?.tokenHoldersCount} Share Holders
                    </div>
                  </div>
                </div>
              ))}
      </div>
    </div>
  );
}

function MarketTableBonds({ streamOpen, active, search }) {
  const { data: bankerList = [] } = useBankerList();
  const { coinListObject } = useContext(BankContext);
  // const [search, setSearch] = useState('');
  const getBankerDetail = useCallback(
    (bankerTag) =>
      bankerList.filter((banker) => banker?.bankerTag === bankerTag)[0],
    [bankerList]
  );
  const [bondAsset, setBondAsset] = useState({
    name: 'All Bond Assets ',
    coin: undefined,
    icon: doubleTik,
  });
  const [assetIssuer, setAssetIssuer] = useState({
    displayName: 'All Issuers',
    bankerTag: undefined,
    profilePicURL: doubleTik,
  });
  const { data: bondsData, isLoading } = useCustomBondsList({
    include: {
      coins: bondAsset?.coin ? [bondAsset?.coin] : undefined,
      created_by: assetIssuer?.bankerTag ? [assetIssuer?.bankerTag] : undefined,
    },
  });
  const [assetModal, setAssetModal] = useState(false);
  const [issuerModalOpen, setIssuerModalOpen] = useState(false);
  const [coinSelected, setCoinSelected] = useState();
  const [filterOpen, setFilterOpen] = useState(false);
  return (
    <div className={classNames.marketTable}>
      <div
        className={`${classNames.header} ${classNames[!streamOpen]} ${
          classNames[active ? 'active' : '']
        }`}
      >
        <div className={classNames.assets}>Issuer</div>
        <div className={classNames.assets}>Asset</div>
        <div className={classNames.price}>Bond Cost</div>
        <div className={classNames.marCap}>Length</div>
        <div className={classNames.supply}>Daily Interest</div>
        <div className={classNames.supply}>Monthly Interest</div>
        <img
          className={`${classNames.btnFilter} ${
            classNames[filterOpen.toString()]
          }`}
          src={angleDown}
          alt=""
          onClick={() => setFilterOpen(!filterOpen)}
        />
      </div>
      {filterOpen && (
        <div className={classNames.filtersWrap}>
          <div className={classNames.filters}>
            <div
              className={classNames.filter}
              onClick={() => setIssuerModalOpen(true)}
            >
              <div className={classNames.label}>Issuer</div>
              <div className={classNames.value}>
                <img src={assetIssuer.profilePicURL} alt="" />
                <span>{assetIssuer.displayName}</span>
              </div>
            </div>
            <div className={classNames.filter}>
              <div className={classNames.label}>Dispaly Currency</div>
              <div className={classNames.value}>
                <img
                  src="https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/rupee.png"
                  alt=""
                />
                <span>{DISPLAY_CURRENCY}</span>
              </div>
            </div>
            <div
              className={classNames.filter}
              onClick={() => setAssetModal(true)}
            >
              <div className={classNames.label}>Bond Asset</div>
              <div className={classNames.value}>
                <img src={bondAsset.icon} alt="" />
                <span>{bondAsset.name}</span>
              </div>
            </div>
            <div className={classNames.filter} style={{ opacity: 0.3 }}>
              + <span>Add Filter</span>
            </div>
          </div>
          {/* <input
            type="text"
            className={classNames.input}
            placeholder="Search Bonds...."
            value={search}
            onChange={(e) => setSearch(e.target.value)}
          /> */}
        </div>
      )}
      <div className={classNames.marketsList}>
        {isLoading
          ? Array(8)
              .fill('')
              .map((_, i) => (
                <div
                  className={`${classNames.marketItemBond}  ${
                    classNames[!streamOpen]
                  }`}
                  key={i}
                >
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                </div>
              ))
          : bondsData?.custom_bonds
              ?.filter(
                (coin) =>
                  coin?.bankerData?.displayName
                    ?.toLowerCase()
                    .includes(search.toLowerCase()) ||
                  bondsData?.currenciesData?.[
                    coin?.coinsData[0]?.coin
                  ]?.coinName
                    ?.toLowerCase()
                    .includes(search.toLowerCase())
              )
              ?.map((coin, i) => (
                <div
                  className={`${classNames.marketItemBond}  ${
                    classNames[!streamOpen]
                  }`}
                  key={coin._id}
                  onClick={() => {
                    setCoinSelected({
                      coin,
                      coinOb:
                        bondsData?.currenciesData?.[coin?.coinsData[0]?.coin],
                      banker: getBankerDetail(coin?.bankerData?.bankerTag),
                    });
                  }}
                >
                  {/* Banker */}
                  <div className={classNames.assets}>
                    <img src={coin?.bankerData?.profilePicURL} alt="" />
                    <div className={classNames.names}>
                      <div className={classNames.name}>
                        {coin?.bankerData?.displayName}
                      </div>
                      <span>
                        {getBankerDetail(coin?.bankerData?.bankerTag)?.country}
                      </span>
                    </div>
                  </div>
                  {/* Asset */}
                  <div className={classNames.assets}>
                    <img
                      src={
                        bondsData?.currenciesData?.[coin?.coinsData[0]?.coin]
                          ?.icon
                      }
                      alt=""
                    />
                    <div className={classNames.names}>
                      <div className={classNames.name}>
                        {
                          bondsData?.currenciesData?.[coin?.coinsData[0]?.coin]
                            ?.name
                        }
                      </div>
                      <span>
                        (
                        {
                          bondsData?.currenciesData?.[coin?.coinsData[0]?.coin]
                            ?.coin
                        }
                        )
                      </span>
                    </div>
                  </div>
                  {/* Unit Cost */}
                  <div className={classNames.price}>
                    {FormatCurrency(
                      coin?.coinsData[0]?.bondCost,
                      coin?.coinsData[0]?.coin
                    )}
                    <span>
                      ({DISPLAY_CURRENCY_SYM}
                      {FormatCurrency(
                        coin?.coinsData[0]?.bondCost *
                          (coin?.coinsData[0]?.coin === DISPLAY_CURRENCY
                            ? 1
                            : coinListObject &&
                              coinListObject[coin?.coinsData[0]?.coin]?.price?.[
                                DISPLAY_CURRENCY
                              ]),
                        DISPLAY_CURRENCY
                      )}
                      )
                    </span>
                  </div>
                  {/* Length */}
                  <div className={classNames.marCap}>
                    {FormatNumber(coin?.days)} Days
                    <span>{FormatNumber(coin?.months, 0)} Months</span>
                  </div>
                  {/* Daily Interest */}
                  <div className={classNames.supply}>
                    {FormatNumber(coin?.daily_interest_rate, 2)}%
                    <span>
                      (
                      {FormatCurrency(
                        coin?.coinsData[0]?.perDayEarnings,
                        coin?.coinsData[0]?.coin
                      )}
                      &nbsp;
                      {coin?.coinsData[0]?.coin})
                    </span>
                  </div>
                  {/* Monthly Interest */}
                  <div className={classNames.supply}>
                    {FormatNumber(coin?.monthly_interest_rate, 2)}%
                    <span>
                      (
                      {FormatCurrency(
                        coin?.coinsData[0]?.monthlyEarnings,
                        coin?.coinsData[0]?.coin
                      )}
                      &nbsp;
                      {coin?.coinsData[0]?.coin})
                    </span>
                  </div>
                </div>
              ))}
      </div>
      {assetModal && (
        <SelectBondAssetModal
          onClose={() => setAssetModal(false)}
          onSuccess={() => setAssetModal(false)}
          bondAsset={bondAsset}
          setBondAsset={setBondAsset}
        />
      )}
      {issuerModalOpen && (
        <SelectBondIssuerModal
          onClose={() => setIssuerModalOpen(false)}
          onSuccess={() => setIssuerModalOpen(false)}
          assetIssuer={assetIssuer}
          setAssetIssuer={setAssetIssuer}
        />
      )}
      {coinSelected && (
        <BondDetailModal
          onClose={() => setCoinSelected(false)}
          data={coinSelected}
        />
      )}
    </div>
  );
}

function MarketTableMoneyMarkets({
  streamOpen,
  setCoinSelected,
  assetClass,
  active,
  search,
}) {
  const { data: mmList = [], isLoading } = useMMList();
  // const [search, setSearch] = useState('');
  const history = useHistory();
  const [filterOpen, setFilterOpen] = useState(false);
  return (
    <div className={classNames.marketTable}>
      <div
        className={`${classNames.header} ${classNames[!streamOpen]} ${
          classNames[active ? 'active' : '']
        }`}
      >
        <div className={classNames.assets}>Asset</div>
        <div className={classNames.price}>Price</div>
        <div className={classNames.volume}>Daily Rate</div>
        <div className={classNames.volume}>Monthly Rate</div>
        <div className={classNames.volume}>Annual Rate</div>
        <div className={classNames.roiPercent}>24 Hr Change</div>
        <img
          className={`${classNames.btnFilter} ${
            classNames[filterOpen.toString()]
          }`}
          src={angleDown}
          alt=""
          onClick={() => setFilterOpen(!filterOpen)}
        />
      </div>
      {filterOpen && (
        <div className={classNames.filtersWrap}>
          <div className={classNames.filters}>
            <div className={classNames.filter}>
              <img src={clock} alt="" />
              <span>24 Hrs</span>
            </div>
            <div className={classNames.filter}>
              <img
                src={
                  'https://apimachine-s3.s3.us-east-2.amazonaws.com/coinImages/rupee.png'
                }
                alt=""
              />
              <span>{DISPLAY_CURRENCY}</span>
            </div>
            <div className={classNames.filter}>
              + <span>Add Filter</span>
            </div>
          </div>
          {/* <input
            type="text"
            className={classNames.input}
            placeholder="Search Cryptocurrencies...."
            value={search}
            onChange={(e) => setSearch(e.target.value)}
          /> */}
        </div>
      )}
      <div className={classNames.marketsList}>
        {isLoading
          ? Array(8)
              .fill('')
              .map((_, i) => (
                <div
                  className={`${classNames.marketItem}  ${
                    classNames[!streamOpen]
                  }`}
                  key={i}
                >
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                  <Skeleton width={200} />
                </div>
              ))
          : mmList
              ?.filter(
                (coin) =>
                  coin?.coin_metdata?.coinName
                    .toLowerCase()
                    .includes(search.toLowerCase()) ||
                  coin?.coin_metdata?.coinSymbol
                    .toLowerCase()
                    .includes(search.toLowerCase())
              )
              ?.map((coin, i) => (
                <div
                  className={`${classNames.marketItem}  ${
                    classNames[!streamOpen]
                  }`}
                  key={coin._id}
                  onClick={() => {
                    setCoinSelected(coin?.coin_metdata);
                    history.push(
                      `/market/${assetClass.name}/${coin?.coin_metdata?.coinSymbol}`
                    );
                  }}
                >
                  {/* Asset */}
                  <div className={classNames.assets}>
                    <img src={coin?.coin_metdata?.coinImage} alt="" />
                    <span className={classNames.name}>
                      {coin?.coin_metdata?.coinName}
                    </span>
                  </div>
                  {/* Price */}
                  <div className={classNames.price}>
                    {DISPLAY_CURRENCY_SYM}
                    {FormatCurrency(coin?.app_price_dc, DISPLAY_CURRENCY)}
                  </div>
                  {/* Daily Rate */}
                  <div className={classNames.volume}>
                    {FormatNumber(coin?.interest_rate, 4)}%
                  </div>
                  {/* Monthly Rate */}
                  <div className={classNames.volume}>
                    {FormatNumber(coin?.monthly_interest_rate, 4)}%
                  </div>
                  {/* Annual Rate */}
                  <div className={classNames.volume}>
                    {FormatNumber(coin?.yearly_interest_rate, 4)}%
                  </div>
                  {/* 24 Hr Rate Change */}
                  <div
                    className={`${classNames.roiPercent} ${
                      classNames[coin?.changeData?.interestRate?._24hr < 0]
                    }`}
                  >
                    {FormatNumber(coin?.changeData?.interestRate?._24hr, 2)}%
                  </div>
                </div>
              ))}
      </div>
    </div>
  );
}

export default MarketsPage;
