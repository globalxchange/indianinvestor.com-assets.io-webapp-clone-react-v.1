import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import classNames from './AboutBusinessPage.module.scss';
import Navbar from '../../components/Navbar';
import useWindowDimensions from '../../utils/WindowSize';
import { ReactComponent as LeftArrowWhite } from '../../static/images/leftArrowWhite.svg';
import bgBusiness from '../../static/images/bg/bgBusiness.jpeg';
import info from '../../static/images/info.svg';
import play from '../../static/images/play.svg';
import shareTokensWhite from '../../static/images/shareTokensWhite.svg';

function AboutBusinessPage() {
  const history = useHistory();
  const { width } = useWindowDimensions();
  if (width < 768) {
    return <HomeMobile />;
  }
  return (
    <div className={classNames.homePage}>
      <Navbar />
      <div className={classNames.contentArea}>
        <div className={classNames.bgWrap}>
          <img src={bgBusiness} alt="" className={classNames.bg} />
        </div>
        <div className={classNames.textArea}>
          <div className={classNames.title}>
            Investor Relations <wbr /> For Modern Startups
          </div>
          <p className={classNames.desc}>
            The Capitalized App Allows You To Define, Build, And Manage You
            Company’s Capitalization Operations Across Multiple Rounds.
          </p>
          <div className={classNames.btns}>
            <Link to="" className={classNames.btnApp}>
              <img src={info} alt="" />
              Request Demo
            </Link>
            <Link to="/businesses/pre-register" className={classNames.btnColor}>
              <img src={play} alt="" />
              Pre-Register
            </Link>
          </div>
        </div>
      </div>
      <div className={classNames.footerBtn}>
        <img src={shareTokensWhite} alt="" className={classNames.ftIcon} />
      </div>
    </div>
  );

  function HomeMobile() {
    return (
      <div className={classNames.homePageMobile}>
        <Navbar />
        <div className={classNames.title}>
          The Future <wbr />
          <span>Is Indian </span>
        </div>
        <p className={classNames.subTitle}>
          Now You Can Invest Into That Future. IndianInvest Alllows You To
          Invest Into Bonds, ShareTokens, Cryptocurrencies, And Much More...
        </p>
        <div className={classNames.cardForUser}>
          <div className={classNames.cardTitle}>For Investors</div>
          <div className={classNames.cardSubTitle}>
            Start Tracking &amp; Growing Your Net-Worth
          </div>
          <div
            className={classNames.bar}
            style={{
              background: '#4CAF50',
            }}
          />
        </div>
        <div className={classNames.cardForUser}>
          <div className={classNames.cardTitle}>For Startups</div>
          <div className={classNames.cardSubTitle}>
            Start Tracking &amp; Growing Your Net-Worth
          </div>
          <div
            className={classNames.bar}
            style={{
              background: '#F47217',
            }}
          />
        </div>
        <div className={classNames.cardForUser}>
          <div className={classNames.cardTitle}>For Educators</div>
          <div className={classNames.cardSubTitle}>
            Start Tracking &amp; Growing Your Net-Worth
          </div>
          <div
            className={classNames.bar}
            style={{
              background: '#0DABD1',
            }}
          />
        </div>
        <Link to="/pre-register" className={classNames.footerBtn}>
          Complete Pre-Registration
          <LeftArrowWhite />
        </Link>
      </div>
    );
  }
}

export default AboutBusinessPage;
