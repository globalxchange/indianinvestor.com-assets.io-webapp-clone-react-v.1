import React, { useContext, useState } from 'react';
import classNames from './retiredAppsPage.module.scss';
import Layout from '../../Layout/Layout';
import retired from '../../static/images/sidebarFullLogos/retired.svg';

function RetiredAppsPage() {
  return (
    <Layout
      active={`retired`}
      className={classNames.retiredAppsPage}
      hideFooter
    >
      <div className={classNames.content}>
        <img src={retired} alt="" className={classNames.logo} />
      </div>
      <div className={classNames.footer}>
        The Retired App Store Is Coming To IndianInvestor.com
      </div>
    </Layout>
  );
}

export default RetiredAppsPage;
