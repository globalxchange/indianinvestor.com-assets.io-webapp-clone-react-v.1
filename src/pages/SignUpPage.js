import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import SignupMobile from '../components/LoginModal/Mobile/SignupMobile';
import signupSideImage from '../static/images/signupSideImage.jpg';
import useWindowDimensions from '../utils/WindowSize';

function SignUpPage() {
  const history = useHistory();
  const { height } = useWindowDimensions();
  const [emailChecked, setEmailChecked] = useState(true);
  return (
    <div className="signUpPage" style={{ height }}>
      <div className="loginSideWrap">
        <SignupMobile
          emailChecked={emailChecked}
          setIsLogin={() => {
            history.push('/app');
          }}
        />
        {/* <div className="footer">
          <div className="" onClick={() => setEmailChecked(true)}>
            Click Here To Get A Custom Spend Email Address
          </div>
        </div> */}
      </div>
      <img src={signupSideImage} alt="" className="sideFullImg" />
    </div>
  );
}

export default SignUpPage;
