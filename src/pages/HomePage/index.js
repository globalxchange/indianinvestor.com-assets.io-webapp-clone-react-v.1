import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import classNames from './HomePage.module.scss';
import bgImg from '../../static/images/bg/bg.png';
import MainNavbar from '../../components/MainNavbar';
import useWindowDimensions from '../../utils/WindowSize';
import { ReactComponent as LeftArrowWhite } from '../../static/images/leftArrowWhite.svg';

function HomePage() {
  const { width } = useWindowDimensions();
  if (width < 768) {
    return <HomeMobile />;
  }
  return (
    <div className={classNames.homePage}>
      <MainNavbar />
      <div className={classNames.contentArea}>
        <div className={classNames.textArea}>
          <div className={classNames.title}>
            The Only Investment <br />
            App You Will Ever Need
          </div>
          <ul className={classNames.desc}>
            <li>
              Create, Monitor, and Grow All Your Investments In One Simple To
              Use App
            </li>
            <li>Access Exclusive Investment Opportunities Into Startups </li>
            <li>Learn How To Invest From Experts Across The World</li>
          </ul>
          <div className={classNames.btns}>
            <Link to="/signup" className={classNames.btnApp}>
              Create Account
            </Link>
            <Link to="/market" className={classNames.btnColor}>
              Explore Markets
            </Link>
          </div>
        </div>
        <div className={classNames.bgWrap}>
          <img src={bgImg} alt="" className={classNames.bgImg} />
        </div>
      </div>
    </div>
  );

  function HomeMobile() {
    return (
      <div className={classNames.homePageMobile}>
        <MainNavbar />
        <div className={classNames.title}>
          The Future <wbr />
          <span>Is Indian </span>
        </div>
        <p className={classNames.subTitle}>
          Now You Can Invest Into That Future. IndianInvest Alllows You To
          Invest Into Bonds, ShareTokens, Cryptocurrencies, And Much More...
        </p>
        <div className={classNames.cardForUser}>
          <div className={classNames.cardTitle}>For Investors</div>
          <div className={classNames.cardSubTitle}>
            Start Tracking &amp; Growing Your Net-Worth
          </div>
          <div
            className={classNames.bar}
            style={{
              background: '#4CAF50',
            }}
          />
        </div>
        <div className={classNames.cardForUser}>
          <div className={classNames.cardTitle}>For Startups</div>
          <div className={classNames.cardSubTitle}>
            Start Tracking &amp; Growing Your Net-Worth
          </div>
          <div
            className={classNames.bar}
            style={{
              background: '#F47217',
            }}
          />
        </div>
        <div className={classNames.cardForUser}>
          <div className={classNames.cardTitle}>For Educators</div>
          <div className={classNames.cardSubTitle}>
            Start Tracking &amp; Growing Your Net-Worth
          </div>
          <div
            className={classNames.bar}
            style={{
              background: '#0DABD1',
            }}
          />
        </div>
        <Link to="/pre-register" className={classNames.footerBtn}>
          Complete Pre-Registration
          <LeftArrowWhite />
        </Link>
      </div>
    );
  }
}

export default HomePage;
