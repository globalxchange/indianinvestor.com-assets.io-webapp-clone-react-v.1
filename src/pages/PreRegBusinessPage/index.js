import React from 'react';
import classNames from './PreRegBusinessPage.module.scss';
import Navbar from '../../components/Navbar';
import PreregisterBox from '../../components/PreregisterBox';

function PreRegBusinessPage() {
  return (
    <div className={classNames.preRegBusinessPage}>
      <Navbar />
      <PreregisterBox />
    </div>
  );
}

export default PreRegBusinessPage;
