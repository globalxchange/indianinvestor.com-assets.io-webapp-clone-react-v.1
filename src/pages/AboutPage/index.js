import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import classNames from './AboutPage.module.scss';
import bgImg from '../../static/images/bg/bg.png';
import Navbar from '../../components/Navbar';
import useWindowDimensions from '../../utils/WindowSize';
import { ReactComponent as LeftArrowWhite } from '../../static/images/leftArrowWhite.svg';
import { ASSET_CLASSES } from '../../config/constants';
import { GetSortOrder } from '../../utils/FunctionTools';

function AboutPage() {
  const history = useHistory();
  const { width } = useWindowDimensions();
  if (width < 768) {
    return <HomeMobile />;
  }
  return (
    <div className={classNames.homePage}>
      <Navbar />
      <div className={classNames.contentArea}>
        <div className={classNames.textArea}>
          <div className={classNames.title}>The Future Is Indian</div>
          <p className={classNames.desc}>
            Create, Monitor, and Grow All Your Investments In One Simple To Use
            App. Access Exclusive Investment Opportunities Into Startups. Learn
            How To Invest From Experts Across The World
          </p>
          <div className={classNames.btns}>
            <Link to="/signup" className={classNames.btnApp}>
              Get Started
            </Link>
            <Link to="" className={classNames.btnColor}>
              Learn More
            </Link>
          </div>
        </div>
        <div className={classNames.investments}>
          {ASSET_CLASSES.filter((asset) => asset.aboutOrder)
            .sort(GetSortOrder('aboutOrder'))
            .map((asset, i) => (
              <div
                className={classNames.card}
                key={asset.name}
                onClick={() => {
                  if (asset.marketEnable) {
                    history.push(`/market/${asset.name}`);
                  }
                }}
              >
                <img src={asset.icon} alt="" />
              </div>
            ))}
        </div>
      </div>
    </div>
  );

  function HomeMobile() {
    return (
      <div className={classNames.homePageMobile}>
        <Navbar />
        <div className={classNames.title}>
          The Future <wbr />
          <span>Is Indian </span>
        </div>
        <p className={classNames.subTitle}>
          Now You Can Invest Into That Future. IndianInvest Alllows You To
          Invest Into Bonds, ShareTokens, Cryptocurrencies, And Much More...
        </p>
        <div className={classNames.cardForUser}>
          <div className={classNames.cardTitle}>For Investors</div>
          <div className={classNames.cardSubTitle}>
            Start Tracking &amp; Growing Your Net-Worth
          </div>
          <div
            className={classNames.bar}
            style={{
              background: '#4CAF50',
            }}
          />
        </div>
        <div className={classNames.cardForUser}>
          <div className={classNames.cardTitle}>For Startups</div>
          <div className={classNames.cardSubTitle}>
            Start Tracking &amp; Growing Your Net-Worth
          </div>
          <div
            className={classNames.bar}
            style={{
              background: '#F47217',
            }}
          />
        </div>
        <div className={classNames.cardForUser}>
          <div className={classNames.cardTitle}>For Educators</div>
          <div className={classNames.cardSubTitle}>
            Start Tracking &amp; Growing Your Net-Worth
          </div>
          <div
            className={classNames.bar}
            style={{
              background: '#0DABD1',
            }}
          />
        </div>
        <Link to="/pre-register" className={classNames.footerBtn}>
          Complete Pre-Registration
          <LeftArrowWhite />
        </Link>
      </div>
    );
  }
}

export default AboutPage;
