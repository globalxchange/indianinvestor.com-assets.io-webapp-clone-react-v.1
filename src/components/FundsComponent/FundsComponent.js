import React from 'react';
import LeaderBoardDrawer from '../LeaderBoardDrawer/LeaderBoardDrawer';

function FundsComponent() {
  return (
    <div className="fundsComponent">
      <LeaderBoardDrawer drawerOpen={true} setDrawerOpen={() => {}} />
    </div>
  );
}

export default FundsComponent;
