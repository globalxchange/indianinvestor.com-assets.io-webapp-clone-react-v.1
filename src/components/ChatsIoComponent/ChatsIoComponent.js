import React, { useContext, useEffect, useState } from 'react';
import ChatsIoComponent from '@teamforce/chats.io-plugin';
import '@teamforce/chats.io-plugin/dist/index.css';

import { BankContext } from '../../context/Context';
import ChatsIoBots from '../ChatsIoBots/ChatsIoBots';
import MyAccountChatComponent from '../SettingsChatComponents/MyAccountChatComponent';
import { APP_CODE, MAIN_LOGO } from '../../config/appConfig';
import { ChatContext } from '../../context/ChatContext';

function ChatsIoComponentM() {
  const { email, token, setChatOn, bondDetail } = useContext(BankContext);
  const { tabSelected, setTabSelected } = useContext(ChatContext);
  useEffect(() => {
    if (bondDetail) {
      setTabSelected('bot');
    }
  }, [bondDetail]);
  return (
    <ChatsIoComponent
      tabOpen={tabSelected}
      email={email}
      token={token}
      primaryColor="#464b4e"
      secondaryColor="#e7e7e7"
      appCode={APP_CODE}
      logo={MAIN_LOGO}
      profileComponent={<MyAccountChatComponent />}
      botComponent={<ChatsIoBots />}
      onClose={() => setChatOn(false)}
      right
    />
  );
}

export default ChatsIoComponentM;
