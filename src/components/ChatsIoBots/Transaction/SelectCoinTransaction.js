import Axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import moment from 'moment';
import Scrollbars from 'react-custom-scrollbars';
import { BankContext } from '../../../context/Context';
import { FormatCurrency, FormatNumber } from '../../../utils/FunctionTools';
import allCoin from '../../../static/images/clipIcons/allCoin.svg';
import * as d3 from 'd3';
import { NetWorthContext } from '../../../context/NetWorthContext';
import { useHistory } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { APP_NAME, ICON_LOGO } from '../../../config/appConfig';

function SelectCoinTransaction({
  transactionType,
  handleSubmitMessage,
  setHideOptions,
}) {
  const history = useHistory();
  const {
    coinList,
    email,
    token,
    coinListObject,
    icedContracts,
    populateModal,
    tostShowOn,
    defaultPrecission,
    bondDetail,
    setBondDetail,
  } = useContext(BankContext);
  const { userApps } = useContext(NetWorthContext);
  const [coin, setCoin] = useState('');
  const [txnList, setTxnList] = useState([]);
  const [contractList, setContractList] = useState([]);
  const [contractsAll, setcontractsAll] = useState([]);
  const colors = d3.scaleOrdinal(d3.schemeTableau10);
  const [action, setAction] = useState('');
  useEffect(() => {
    if (transactionType === 'Bonds') {
      let contractArray = [];
      icedContracts.forEach((contract) => {
        contractArray = [...contractArray, ...contract.contracts];
      });
      setcontractsAll(contractArray);
      coin.coinSymbol
        ? setContractList(
            contractArray.filter(
              (contract) => contract.coin === coin.coinSymbol
            )
          )
        : setContractList(contractArray);
    } else if (transactionType === 'Trade') {
      setTxnList([]);
    } else {
      setTxnList([]);
      Axios.get(
        `https://comms.globalxchange.com/coin/vault/service/path/${transactionType.toLowerCase()}/txn/get?email=${email}&coin=${
          coin.coinSymbol
        }`
      ).then(({ data }) => {
        setTxnList(data.txns);
      });
    }
  }, [email, coin, transactionType]);

  const [moreOptionOpen, setMoreOptionOpen] = useState(false);
  const [bondStatus, setBondStatus] = useState('active');
  useEffect(() => {
    if (bondDetail?.id && bondDetail?.coin) {
      const coinArr = coinList.filter(
        (coin) => coin.coinSymbol === bondDetail?.coin
      );
      const tempArr = contractList.filter((txn) => txn._id === bondDetail?.id);
      setCoin(coinArr[0]);
      setTimeout(() => {
        setTxnSelected(tempArr[0]);
      }, 200);
      setMoreOptionOpen(true);
      if (coinArr[0] && tempArr[0]) setBondDetail();
    }
  }, [bondDetail, contractsAll]);
  const [txnSelected, setTxnSelected] = useState('');
  const redeemBond = (app_code, app) => {
    Axios.post('https://comms.globalxchange.com/coin/iced/contracts/redeem', {
      email: email, // from which user iced contract
      token: token,
      app_code, // to which app
      profile_id: app.profile_id, // optional, to whose profile
      contract_id: txnSelected._id, // _id of the contract
      // to_account_email: 'mani@nvestbank.com', // optional, to whose account
    }).then(({ data }) => {
      tostShowOn(data.message);
    });
  };

  useEffect(() => {
    setTxnSelected('');
  }, [coin, transactionType]);
  return (
    <div className={`chatLearn ${Boolean(coin)}`}>
      {coin && !moreOptionOpen && (
        <div className="coinHead" onClick={() => setCoin('')}>
          <img src={coin.coinImage || allCoin} alt="" className="thumb p-4" />
          <div className="textContent">
            <div className="pubTitle">{coin.coinName || 'All Currencies'}</div>
            {transactionType === 'Bonds' && (
              <div className="pubDesc">
                {coin.coinSymbol
                  ? contractsAll.filter(
                      (contract) => contract.coin === coin.coinSymbol
                    ).length
                  : contractsAll.length}{' '}
                Bonds
              </div>
            )}
          </div>
        </div>
      )}
      {txnSelected ? (
        transactionType === 'Bonds' ? (
          <>
            <div
              className="bondsItemHead"
              onClick={() => {
                setTxnSelected('');
                setHideOptions(false);
                setMoreOptionOpen(false);
              }}
            >
              <div className="coinPrice">
                <div className="img">
                  <img src={ICON_LOGO} alt="" />
                  {APP_NAME}
                </div>
                <div className="bondTitle">
                  <span>
                    {FormatCurrency(
                      txnSelected.current_voc,
                      txnSelected.coin,
                      defaultPrecission
                    )}
                  </span>
                  &nbsp;{txnSelected.coin}
                </div>
              </div>
              <div className="labels">
                <span>
                  {txnSelected?.completed_stats?.days} / {txnSelected?.days}
                  Day Bond
                </span>
                <span>
                  <b>
                    $
                    <span>
                      {FormatCurrency(
                        coinListObject[txnSelected.coin] &&
                          coinListObject[txnSelected.coin].price &&
                          coinListObject[txnSelected.coin].price.USD &&
                          txnSelected.current_voc *
                            coinListObject[txnSelected.coin].price.USD,
                        'USD',
                        defaultPrecission
                      )}
                    </span>{' '}
                    USD |{' '}
                  </b>
                  <span className>
                    {' '}
                    {Math.sign(coinListObject[txnSelected.coin]._24hrchange) ==
                    0
                      ? ''
                      : Math.sign(
                          coinListObject[txnSelected.coin]._24hrchange
                        ) == -1
                      ? '-'
                      : '+'}
                    {FormatNumber(
                      (coinListObject &&
                        coinListObject[txnSelected.coin] &&
                        coinListObject[txnSelected.coin]._24hrchange *
                          Math.sign(
                            coinListObject[txnSelected.coin]._24hrchange
                          )) ||
                        0,
                      2
                    )}
                    % In 24Hrs
                  </span>
                </span>
              </div>
              <div className="rates">
                <div className="ratesItem text-left">
                  <div className="value">
                    <span>{FormatNumber(txnSelected.base_interest, 1)}%</span>
                  </div>
                  <div className="label">Base Rate</div>
                </div>
                <div className="ratesItem text-center disabledValue">
                  <div className="value">
                    <span>0.00%</span>
                  </div>
                  <div className="label">Capital Appreciation</div>
                </div>
                <div className="ratesItem text-right disabledValue">
                  <div className="value">
                    <span>0.00</span>
                  </div>
                  <div className="label"> Fixed Income</div>
                </div>
              </div>
            </div>
            {action === 'Redeem' ? (
              <div className="whatToDo">
                <div className="whatDoTitle">
                  Which Of Your Apps Do You Want To Redeem To?
                </div>
                <Scrollbars
                  className="appList"
                  renderTrackHorizontal={() => <div />}
                  renderThumbHorizontal={() => <div />}
                  renderTrackVertical={() => <div />}
                  renderThumbVertical={() => <div />}
                >
                  {userApps.map((app) => (
                    <div
                      className="app"
                      onClick={() =>
                        populateModal(
                          'Are You Sure Want To Redeem This Bond',
                          () => {},
                          () => redeemBond(app.app_code, app)
                        )
                      }
                    >
                      <img src={app.app_icon} alt="" />
                      <div className="label">{app.app_name}</div>
                    </div>
                  ))}
                </Scrollbars>
                <div className="btnGoBack" onClick={() => setAction('')}>
                  Go Back
                </div>
              </div>
            ) : moreOptionOpen ? (
              <div className="whatToDo">
                <div className="whatDoTitle">Select One Of The Following</div>
                <Scrollbars
                  className="actionsList"
                  renderTrackHorizontal={() => <div />}
                  renderThumbHorizontal={() => <div />}
                  renderTrackVertical={() => <div />}
                  renderThumbVertical={() => <div />}
                >
                  <div
                    className={`btnAction ${
                      txnSelected.status === 'completed' ||
                      txnSelected.status === 'redeemed'
                        ? ''
                        : 'disable'
                    }`}
                    onClick={() => {
                      if (
                        txnSelected.status === 'completed' ||
                        txnSelected.status === 'redeemed'
                      ) {
                        setAction('Redeem');
                      }
                    }}
                  >
                    Redeem
                    <FontAwesomeIcon icon={faAngleRight} className="angle" />
                  </div>
                  <div className="btnAction disable">
                    Change Payout Destination
                    <FontAwesomeIcon icon={faAngleRight} className="angle" />
                  </div>
                  <div className="btnAction disable">
                    Change Payout Frequency
                    <FontAwesomeIcon icon={faAngleRight} className="angle" />
                  </div>
                  <div className="btnAction disable">
                    Change Payout Notification
                    <FontAwesomeIcon icon={faAngleRight} className="angle" />
                  </div>
                  <div className="btnAction disable">
                    Transfer
                    <FontAwesomeIcon icon={faAngleRight} className="angle" />
                  </div>
                </Scrollbars>
              </div>
            ) : (
              <div className="whatToDo">
                <div className="whatDoTitle">Select One Of The Following</div>
                <Scrollbars
                  className="actionsList"
                  renderTrackHorizontal={() => <div />}
                  renderThumbHorizontal={() => <div />}
                  renderTrackVertical={() => <div />}
                  renderThumbVertical={() => <div />}
                >
                  <div
                    className="btnAction"
                    onClick={() => history.push(`/bonds/${txnSelected._id}`)}
                  >
                    Bond Hash
                    <FontAwesomeIcon icon={faAngleRight} className="angle" />
                  </div>
                  <div
                    className="btnAction"
                    onClick={() => {
                      setHideOptions(!moreOptionOpen);
                      setMoreOptionOpen(!moreOptionOpen);
                    }}
                  >
                    Interact With Bond
                    <FontAwesomeIcon icon={faAngleRight} className="angle" />
                  </div>
                </Scrollbars>
              </div>
            )}
          </>
        ) : (
          <>
            <div className="pubHead">Transaction Details</div>
            <Scrollbars className="categoryListDetail">
              <div className="categoryItm" onClick={() => setTxnSelected('')}>
                <img
                  src={
                    coinListObject &&
                    coinListObject[coin?.coinSymbol]?.coinImage
                  }
                  alt=""
                  className="thumb"
                />
                <div className="textContent">
                  <div className="pubTitle">{txnSelected._id}</div>
                  <div className="pubDesc">
                    {moment(txnSelected.timestamp).format(
                      'MMMM Do YYYY [at] h:mm:ss A zz'
                    )}
                  </div>
                </div>
              </div>
              <div className="itmDetail">
                {Object.keys(txnSelected).map((key) => (
                  <div className="listItem">
                    <div className="key">{key}</div>
                    <p className="value" onClick={() => {}}>
                      {JSON.stringify(txnSelected[key])}
                    </p>
                  </div>
                ))}
              </div>
              <div className="btnsDispute">
                <div
                  className="btnItm"
                  onClick={() =>
                    handleSubmitMessage(
                      `Hey I have An Issue With The ${transactionType}  ${JSON.stringify(
                        txnSelected
                      )}`,
                      () => {}
                    )
                  }
                >
                  Dispute
                </div>
                <div className="btnItm disable">Cancel</div>
              </div>
            </Scrollbars>
          </>
        )
      ) : coin ? (
        <>
          {transactionType === 'Bonds' && (
            <div className="pubTabs">
              <div
                className={`tab ${bondStatus === 'active'}`}
                onClick={() => setBondStatus('active')}
              >
                Active
              </div>
              <div
                className={`tab ${bondStatus === 'completed'}`}
                onClick={() => setBondStatus('completed')}
              >
                Completed
              </div>
              <div
                className={`tab ${bondStatus === 'redeemed'}`}
                onClick={() => setBondStatus('redeemed')}
              >
                Redeemed
              </div>
            </div>
          )}
          <Scrollbars className="categoryList">
            {transactionType === 'Bonds'
              ? contractList
                  ?.filter((txn) => txn.status === bondStatus)
                  ?.map((txn, i) => (
                    <div
                      className="bondsItem"
                      onClick={() => setTxnSelected(txn)}
                      style={{
                        borderLeftColor: colors(i),
                        borderLeftWidth: 10,
                      }}
                    >
                      <div className="coinPrice">
                        <div className="img">
                          <img src={ICON_LOGO} alt="" />
                          {APP_NAME}
                        </div>
                        <div className="bondTitle">
                          <span>
                            {FormatCurrency(
                              txn.current_voc,
                              txn.coin,
                              defaultPrecission
                            )}
                          </span>
                          &nbsp;{txn.coin}
                        </div>
                      </div>
                      <div className="labels">
                        <span>
                          {txn?.completed_stats?.days} / {txn?.days} Day Bond
                        </span>
                        <span>
                          <b>
                            $
                            <span>
                              {FormatCurrency(
                                coinListObject[txn.coin] &&
                                  coinListObject[txn.coin].price &&
                                  coinListObject[txn.coin].price.USD &&
                                  txn.current_voc *
                                    coinListObject[txn.coin].price.USD,
                                'USD',
                                defaultPrecission
                              )}
                            </span>{' '}
                            USD |{' '}
                          </b>
                          <span className>
                            {Math.sign(coinListObject[txn.coin]._24hrchange) ==
                            0
                              ? ''
                              : Math.sign(
                                  coinListObject[txn.coin]._24hrchange
                                ) == -1
                              ? '-'
                              : '+'}
                            {FormatNumber(
                              (coinListObject &&
                                coinListObject[txn.coin] &&
                                coinListObject[txn.coin]._24hrchange *
                                  Math.sign(
                                    coinListObject[txn.coin]._24hrchange
                                  )) ||
                                0,
                              2
                            )}
                            % In 24Hrs
                          </span>
                        </span>
                      </div>
                      <div className="rates">
                        <div className="ratesItem text-left">
                          <div className="value">
                            <span>{FormatNumber(txn.base_interest, 1)}%</span>
                          </div>
                          <div className="label">Base Rate</div>
                        </div>
                        <div className="ratesItem text-center disabledValue">
                          <div className="value">
                            <span>0.00%</span>
                          </div>
                          <div className="label">Capital Appreciation</div>
                        </div>
                        <div className="ratesItem text-right disabledValue">
                          <div className="value">
                            <span>0.00</span>
                          </div>
                          <div className="label"> Fixed Income</div>
                        </div>
                      </div>
                    </div>
                  ))
              : txnList?.map((txn) => (
                  <div
                    className="categoryItm"
                    onClick={() => setTxnSelected(txn)}
                  >
                    <img
                      src={
                        coinListObject &&
                        coinListObject[coin.coinSymbol].coinImage
                      }
                      alt=""
                      className="thumb p-4"
                    />
                    <div className="textContent">
                      <div className="pubTitle">
                        {FormatCurrency(
                          txn.buy_amount,
                          txn.coin,
                          defaultPrecission
                        )}{' '}
                        {txn.coin}
                      </div>
                      <div className="pubDesc">
                        {moment(txn.timestamp).format(
                          'MMMM Do YYYY [at] h:mm:ss A zz'
                        )}
                      </div>
                    </div>
                  </div>
                ))}
          </Scrollbars>
        </>
      ) : (
        <>
          <div className="pubHead">
            Select Coin
            <div className="seeAll" onClick={() => setCoin({})}>
              See All
            </div>
          </div>
          <Scrollbars className="categoryList">
            {coinList.map((coin) => (
              <div className="categoryItm" onClick={() => setCoin(coin)}>
                <img src={coin.coinImage} alt="" className="thumb p-4" />
                <div className="textContent">
                  <div className="pubTitle">{coin.coinName}</div>
                  {transactionType === 'Bonds' && (
                    <div className="pubDesc">
                      {
                        contractsAll.filter(
                          (contract) => contract.coin === coin.coinSymbol
                        ).length
                      }{' '}
                      Bonds
                    </div>
                  )}
                </div>
              </div>
            ))}
          </Scrollbars>
        </>
      )}
    </div>
  );
}

export default SelectCoinTransaction;
