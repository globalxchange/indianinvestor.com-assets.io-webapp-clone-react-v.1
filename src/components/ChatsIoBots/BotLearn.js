import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import Scrollbars from 'react-custom-scrollbars';
import { APP_CODE } from '../../config/appConfig';
import ChatLearn from './Learn/ChatLearn';

function BotLearn() {
  const [publications, setPublications] = useState([]);
  const [selectedPublication, setSelectedPublication] = useState();
  const [categories, setCategories] = useState([]);
  useEffect(() => {
    Axios.get(
      `https://fxagency.apimachine.com/publication/appcode/?appcode=${APP_CODE}`
    ).then(({ data }) => {
      if (data.status) {
        setPublications(data.data);
        setSelectedPublication(data.data && data.data[0]);
      }
    });
  }, []);
  useEffect(() => {
    setCategories([]);
    selectedPublication &&
      Axios.get(
        `https://fxagency.apimachine.com/category/publication/${selectedPublication._id}`
      ).then(({ data }) => {
        if (data.status) setCategories(data.data);
      });
  }, [selectedPublication]);
  return (
    <>
      <div className="actionBtns">
        <div className="head">Select Transaction Type</div>
        <Scrollbars
          className="botOptionWrapper"
          renderView={(props) => (
            <div {...props} className="botOptionList"></div>
          )}
          renderTrackHorizontal={() => <div />}
          renderThumbHorizontal={() => <div />}
          renderTrackVertical={() => <div />}
          renderThumbVertical={() => <div />}
        >
          {publications.map((pub) => (
            <div
              key={pub._id}
              className={`option ${selectedPublication === pub}`}
              onClick={() => {
                setSelectedPublication();
                setTimeout(() => {
                  setSelectedPublication(pub);
                }, 100);
              }}
            >
              <img src={pub.profile_pic} alt="" />
              <span>{pub.name}</span>
            </div>
          ))}
        </Scrollbars>
      </div>
      <ChatLearn categories={categories} />
    </>
  );
}

export default BotLearn;
