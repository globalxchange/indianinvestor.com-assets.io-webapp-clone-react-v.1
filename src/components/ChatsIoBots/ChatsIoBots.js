import React, { useContext, useEffect } from 'react';
import Scrollbars from 'react-custom-scrollbars';
import { ChatsIoContext } from '../../context/ChatsIoContext';
import { BankContext } from '../../context/Context';
import { ChatContext } from '../../context/ChatContext';
import AddMarketAssetComponent from '../ChatsIoBots/Actions/AddMarketAssetComponent';
import BotActions from './BotActions';
import BotLearn from './BotLearn';
import BotTransactions from './BotTransactions';

function ChatsIoBots() {
  const { optionType, setOptionType } = useContext(ChatsIoContext);
  const { chatPathData, setChatPathData, trustDepositOpen } = useContext(
    ChatContext
  );
  const { bondDetail } = useContext(BankContext);
  useEffect(() => {
    if (trustDepositOpen) setOptionType('action');
  }, [trustDepositOpen]);
  useEffect(() => {
    if (bondDetail) setOptionType('transactions');
  }, [bondDetail]);
  useEffect(() => {
    return () => {
      setChatPathData();
    };
  }, []);
  function getComponent() {
    switch (true) {
      case Boolean(chatPathData):
        return <AddMarketAssetComponent setHideOptions={() => {}} />;
      case optionType === 'action':
        return <BotActions />;
      case optionType === 'transactions':
        return <BotTransactions />;
      case optionType === 'learn':
        return <BotLearn />;
      default:
        break;
    }
  }
  return (
    <div className="chatsIoBots">
      {!Boolean(chatPathData) && (
        <Scrollbars
          autoHide
          className="btnsScrlWrapper"
          renderView={(props) => <div {...props} className="btnsScrlList" />}
          renderThumbHorizontal={() => <div />}
          renderThumbVertical={() => <div />}
        >
          <div
            className={`btnSwitch ${optionType === 'action'}`}
            onClick={() => {
              setOptionType('');
              setTimeout(() => {
                setOptionType('action');
              }, 100);
            }}
          >
            Transact
          </div>
          <div
            className={`btnSwitch ${optionType === 'transactions'}`}
            onClick={() => {
              setOptionType('');
              setTimeout(() => {
                setOptionType('transactions');
              }, 100);
            }}
          >
            Transactions
          </div>
          <div
            className={`btnSwitch ${optionType === 'learn'}`}
            onClick={() => {
              setOptionType('');
              setTimeout(() => {
                setOptionType('learn');
              }, 100);
            }}
          >
            Learn
          </div>
        </Scrollbars>
      )}
      {getComponent()}
    </div>
  );
}

export default ChatsIoBots;
