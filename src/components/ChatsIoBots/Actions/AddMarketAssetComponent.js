import React, { useContext, useEffect, useState } from 'react';
import Scrollbars from 'react-custom-scrollbars';
import Axios from 'axios';
import { useHistory } from 'react-router';

import fiat from '../../../static/images/clipIcons/fiat.svg';
import crypto from '../../../static/images/clipIcons/crypto.svg';
import { BankContext } from '../../../context/Context';
import { FormatCurrency } from '../../../utils/FunctionTools';
import { ChatContext } from '../../../context/ChatContext';
import LoadingAnim from '../../LoadingAnim/LoadingAnim';
import { InvestmentVaultContext } from '../../../context/InvestmentVaultContext';
import { VaultContext } from '../../../context/VaultContext';

function AddMarketAssetComponent({ setHideOptions }) {
  const history = useHistory();
  const { chatPathData } = useContext(ChatContext);
  const [coinType, setCoinType] = useState('');
  const [coinSelect, setCoinSelect] = useState('');
  const [app, setApp] = useState('');
  const {
    coinList,
    email,
    tostShowOn,
    coinListObject,
    token,
    defaultPrecission,
  } = useContext(BankContext);
  const { updateBalance } = useContext(InvestmentVaultContext);
  const vaultContext = useContext(VaultContext);
  useEffect(() => {
    setCoinSelect('');
  }, [coinType]);
  useEffect(() => {
    setApp('');
  }, [coinSelect?.coinSymbol]);
  useEffect(() => {
    if (coinType) setHideOptions(true);
    else setHideOptions(false);
  }, [coinType]);
  const [appBalanceList, setAppBalanceList] = useState([]);
  useEffect(() => {
    Axios.get(
      `https://comms.globalxchange.com/gxb/apps/registered/user?email=${email}`
    ).then(({ data }) => {
      if (data.status) {
        setAppBalanceList(data.userApps);
      }
    });
  }, [email]);
  const [successRes, setSuccessRes] = useState('');
  const [loading, setLoading] = useState(false);

  const [coinBalanceList, setCoinBalanceList] = useState([]);
  useEffect(() => {
    setLoading(true);
    Axios.post('https://comms.globalxchange.com/coin/vault/service/coins/get', {
      app_code: app.app_code,
      profile_id: app.profile_id,
    })
      .then(({ data }) => {
        setCoinBalanceList(data.coins_data);
      })
      .finally(() => setLoading(false));
  }, [app]);
  useEffect(() => {
    const tmpArr = coinBalanceList.filter(
      (coin) => coin.coinSymbol === coinSelect.coinSymbol
    );
    if (tmpArr.length && coinSelect !== tmpArr[0]) {
      setCoinSelect(tmpArr[0]);
    }
  }, [coinBalanceList, coinSelect]);

  // Coin
  const [tokenValue, setTokenValue] = useState('');
  const [coinValue, setCoinValue] = useState('');
  const [covertValue, setCovertValue] = useState('');
  const [previewRes, setPreviewRes] = useState();
  const [isTokenValue, setIsTokenValue] = useState(false);

  function buyCoin(preview) {
    setLoading(true);
    Axios.post('https://comms.globalxchange.com/coin/investment/path/execute', {
      email,
      token,
      app_code: app.app_code,
      profile_id: app.profile_id,
      path_id: chatPathData?.path_id,
      user_pay_coin: coinSelect?.coinSymbol,
      pay_amount: isTokenValue ? undefined : parseFloat(coinValue),
      tokens_amount: isTokenValue ? parseFloat(tokenValue) : undefined,
      stats: preview,
    })
      .then(({ data }) => {
        if (data.status) {
          if (preview) {
            setPreviewRes(data);
          } else {
            setSuccessRes(data);
            updateBalance();
            vaultContext.updateBalance();
          }
        } else {
          tostShowOn(data.message);
        }
      })
      .finally(() => setLoading(false));
  }

  const { setCoinSelected } = useContext(InvestmentVaultContext);

  function changeValue(value, key) {
    if (value) {
      switch (key) {
        case 'from':
          setCoinValue(value);
          setCovertValue(
            FormatCurrency(
              (value * coinSelect?.price.USD) /
                coinListObject[chatPathData.asset].price.USD,
              (coinListObject && coinListObject[chatPathData.asset])?.coinSymbol
            ).replace(',', '')
          );
          setTokenValue(
            FormatCurrency(
              (value * coinSelect?.price.USD) /
                coinListObject[chatPathData.asset].price.USD /
                chatPathData?.token_price,
              ''
            ).replace(',', '')
          );
          setIsTokenValue(false);
          break;
        case 'convert':
          setCovertValue(value);
          setCoinValue(
            FormatCurrency(
              (value * coinListObject[chatPathData.asset].price.USD) /
                coinSelect?.price.USD,
              coinSelect?.coinSymbol
            ).replace(',', '')
          );
          setTokenValue(
            FormatCurrency(value / chatPathData?.token_price, '').replace(
              ',',
              ''
            )
          );
          setIsTokenValue(false);
          break;
        case 'to':
          setTokenValue(value);
          setCovertValue(
            FormatCurrency(
              value * chatPathData?.token_price,
              (coinListObject && coinListObject[chatPathData.asset])?.coinSymbol
            ).replace(',', '')
          );
          setCoinValue(
            FormatCurrency(
              (value *
                chatPathData?.token_price *
                coinListObject[chatPathData.asset].price.USD) /
                coinSelect?.price.USD,
              coinSelect?.coinSymbol
            ).replace(',', '')
          );
          setIsTokenValue(true);
          break;

        default:
          break;
      }
    } else {
      setCoinValue('');
      setCovertValue('');
      setTokenValue('');
    }
  }

  return (
    <>
      {successRes ? (
        <div className="succes">
          <div className="succesHead">Success</div>
          <div className="succesDesc">
            You Just Exchanged{' '}
            {FormatCurrency(coinValue, coinSelect?.coinSymbol)}{' '}
            {coinSelect.coinName} For {FormatCurrency(tokenValue)}{' '}
            {chatPathData?.token_profile_data?.coinName}
          </div>
          <div
            className="goToMyInvestments"
            onClick={() => {
              history.push('/investmentVault');
              setCoinSelected(chatPathData?.token_profile_data);
            }}
          >
            {chatPathData?.token_profile_data?.coinSymbol} Vault
          </div>
        </div>
      ) : (
        <div className="addCoinComponent">
          {!coinType && (
            <div className="headTitle">
              What Type Of Asset Do You Want To Use To Buy{' '}
              {chatPathData?.token_profile_data?.coinName}?
            </div>
          )}
          {!coinSelect && (
            <div className="actionBtns">
              <Scrollbars
                className="botOptionWrapper"
                autoHide
                renderView={(props) => (
                  <div {...props} className="botOptionList" />
                )}
                renderThumbHorizontal={() => <div />}
                renderThumbVertical={() => <div />}
              >
                <div
                  className={`option ${coinType === 'crypto'} n-5`}
                  onClick={() =>
                    setCoinType((prev) => (prev === 'crypto' ? '' : 'crypto'))
                  }
                >
                  <img src={crypto} alt="" />
                  <span>Crypto</span>
                </div>
                <div
                  className={`option ${coinType === 'fiat'} n-5`}
                  onClick={() =>
                    setCoinType((prev) => (prev === 'fiat' ? '' : 'fiat'))
                  }
                >
                  <img src={fiat} alt="" />
                  <span>Fiat</span>
                </div>
              </Scrollbars>
            </div>
          )}
          {coinType && (
            <>
              {!coinSelect && (
                <div className="headTitle">
                  Choose One Of The Following Coins
                </div>
              )}
              <div className="actionBtns">
                <Scrollbars
                  className="botOptionWrapper"
                  autoHide
                  renderView={(props) => (
                    <div {...props} className="botOptionList" />
                  )}
                  renderThumbHorizontal={() => <div />}
                  renderThumbVertical={() => <div />}
                >
                  {coinList
                    .filter((coin) => coin.type === coinType)
                    .map((coin) => (
                      <div
                        className={`option ${coinSelect === coin} n-5`}
                        onClick={() => {
                          setCoinSelect(coin);
                        }}
                      >
                        <img src={coin.coinImage} alt="" />
                        <span>{coin.coinName}</span>
                      </div>
                    ))}
                </Scrollbars>
              </div>
            </>
          )}
          {coinSelect && (
            <div className={`selectVault ${Boolean(app?.app_code)}`}>
              {app?.app_code ? (
                <>
                  <div className="app" onClick={() => setApp('')}>
                    <div className="label">
                      <img src={app?.app_icon} alt="" className="appIcn" />
                      <span>{app?.app_name}</span>
                    </div>
                  </div>
                  <hr />
                </>
              ) : (
                <>
                  <div className="headTitle">
                    What App Do You Want Withdraw The {coinSelect.coinName}{' '}
                    From?
                  </div>
                  <Scrollbars className="vaultList">
                    {appBalanceList
                      .filter((app) => {
                        if (chatPathData?.app_codes)
                          for (
                            let i = 0;
                            i < chatPathData?.app_codes?.length;
                            i++
                          ) {
                            if (app.app_code === chatPathData?.app_codes[i])
                              return true;
                          }
                      })
                      .map((app) => (
                        <div
                          className="app"
                          key={app.app_code}
                          onClick={() => setApp(app)}
                        >
                          <div className="label">
                            <img
                              src={app?.app_icon}
                              alt=""
                              className="appIcn"
                            />
                            <span>{app?.app_name}</span>
                          </div>
                        </div>
                      ))}
                  </Scrollbars>
                </>
              )}
            </div>
          )}
          {app?.app_code && (
            <Scrollbars className="genarateQuote">
              <div className="title">Generate Quote</div>
              <div className="group">
                <div className="label">You Are Investing</div>
                <div className="inpWrap">
                  <input
                    type="number"
                    placeholder={FormatCurrency(0, coinSelect?.coinSymbol)}
                    value={previewRes?.finalFromAmount || coinValue}
                    min={0}
                    onChange={(e) => {
                      const { value } = e.target;
                      changeValue(value, 'from');
                    }}
                  />
                  <div className="btns">
                    <div
                      className="btnPercent"
                      onClick={() => {
                        const value = coinSelect?.coinValue / 4;
                        changeValue(value, 'from');
                      }}
                    >
                      25%
                    </div>
                    <div
                      className="btnPercent"
                      onClick={() => {
                        const value = coinSelect?.coinValue / 2;
                        changeValue(value, 'from');
                      }}
                    >
                      50%
                    </div>
                    <div
                      className="btnPercent"
                      onClick={() => {
                        const value = coinSelect?.coinValue;
                        changeValue(value, 'from');
                      }}
                    >
                      100%
                    </div>
                  </div>
                  <div className="coinBox">
                    <img src={coinSelect?.coinImage} alt="" />
                    <span>{coinSelect?.coinSymbol}</span>
                  </div>
                </div>
              </div>
              <div className="group">
                <div className="label">Converting To</div>
                <div className="inpWrap">
                  <input
                    type="number"
                    min={0}
                    value={previewRes?.finalToAmount || covertValue}
                    onChange={(e) => {
                      const { value } = e.target;
                      changeValue(value, 'convert');
                    }}
                    placeholder={FormatCurrency(
                      0,
                      (coinListObject && coinListObject[chatPathData.asset])
                        ?.coinSymbol
                    )}
                  />
                  <div className="coinBox">
                    <img
                      src={
                        (coinListObject && coinListObject[chatPathData.asset])
                          ?.coinImage
                      }
                      alt=""
                    />
                    <span>
                      {
                        (coinListObject && coinListObject[chatPathData.asset])
                          ?.coinSymbol
                      }
                    </span>
                  </div>
                </div>
              </div>
              <div className="group">
                <div className="label">You Will Receive</div>
                <div className="inpWrap">
                  <input
                    type="number"
                    min={0}
                    placeholder={FormatCurrency(0)}
                    value={
                      previewRes?.investmentAsset_stats?.tokens_amount ||
                      tokenValue
                    }
                    onChange={(e) => {
                      const { value } = e.target;
                      changeValue(value, 'to');
                    }}
                  />
                  <div className="coinBox">
                    <img
                      src={chatPathData?.token_profile_data?.coinImage}
                      alt=""
                    />
                    <span>{chatPathData?.token_profile_data?.coinSymbol}</span>
                  </div>
                </div>
              </div>
              <div className="group">
                <button
                  disabled={!(tokenValue || coinValue)}
                  className="btBuy"
                  onClick={() => {
                    buyCoin(false);
                  }}
                >
                  Buy {FormatCurrency(tokenValue)}{' '}
                  {chatPathData?.token_profile_data?.coinSymbol}{' '}
                </button>
              </div>
            </Scrollbars>
          )}
          {loading && (
            <div className="loadingAnim">
              <LoadingAnim />
            </div>
          )}
        </div>
      )}
    </>
  );
}

export default AddMarketAssetComponent;
