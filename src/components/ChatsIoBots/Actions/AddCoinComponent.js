import React, { useContext, useEffect, useState } from 'react';
import Scrollbars from 'react-custom-scrollbars';
import Axios from 'axios';

import fiat from '../../../static/images/clipIcons/fiat.svg';
import crypto from '../../../static/images/clipIcons/crypto.svg';
import investIcon from '../../../static/images/clipIcons/invest.svg';
import qr from '../../../static/images/clipIcons/qr.svg';
import paste from '../../../static/images/clipIcons/paste.svg';
import blockCheck from '../../../static/images/clipIcons/blockCheck.svg';
import vaults from '../../../static/images/clipIcons/vaults.svg';
import moneyMarket from '../../../static/images/clipIcons/moneyMarket.svg';
import { BankContext } from '../../../context/Context';
import FundVault from '../../VaultsPage/VaultFundWithdraw/FundVault';
import { APP_CODE } from '../../../config/appConfig';

function AddCoinComponent({ setActionType, setShow }) {
  const [coinType, setCoinType] = useState('');
  const [coinFrom, setCoinFrom] = useState('');
  const [coinSelect, setCoinSelect] = useState('');
  const [depositModal, setDepositModal] = useState(false);
  const { coinList, email, tostShowOn } = useContext(BankContext);
  const [txnHash, setTxnHash] = useState('');
  useEffect(() => {
    setCoinFrom('');
  }, [coinType]);
  useEffect(() => {
    setCoinSelect('');
  }, [coinFrom]);
  useEffect(() => {
    if (coinType) setShow(false);
    else setShow(true);
  }, [coinType]);
  const [trxAddress, setTrxAddress] = useState('');
  useEffect(() => {
    Axios.post(
      'https://comms.globalxchange.com/coin/vault/service/crypto/address/request',
      {
        email: email,
        app_code: APP_CODE,
        coin: 'TRX',
      }
    ).then(({ data }) => {
      setTrxAddress(data.address);
    });
  }, []);
  useEffect(() => {
    if (coinSelect.coinSymbol === 'TRX' && !coinSelect.coin_address) {
      setCoinSelect({ ...coinSelect, ...{ coin_address: trxAddress } });
    }
  }, [trxAddress, coinSelect]);
  const saveTxn = () => {
    if (coinSelect.coinSymbol === 'TRX') {
      Axios.post(
        'https://comms.globalxchange.com/coin/vault/service/deposit/trx/external/request',
        {
          app_code: APP_CODE,
          email: email,
          txn_hash: txnHash,
        }
      ).then(({ data }) => {
        if (data.status) setActionType(false);
        tostShowOn(data.message);
      });
    } else {
      Axios.post(
        'https://comms.globalxchange.com/coin/vault/service/deposit/eth/coins/request',
        {
          app_code: APP_CODE,
          email: email,
          coin: coinSelect.coinSymbol, // any one of the ETH coins
          txn_hash: txnHash,
        }
      ).then(({ data }) => {
        if (data.status) setActionType(false);
        tostShowOn(data.message);
      });
    }
  };
  return (
    <>
      {coinSelect && coinFrom === 'External' ? (
        <div className="addCoinComponent final position-absolute">
          <div className="headCoin">
            <img src={coinSelect && coinSelect.coinImage} alt="" />{' '}
            {coinSelect.coinName}
          </div>
          <div className="steps">
            <h5>Step 1</h5>
            <p>
              Copy this {coinSelect.coinName} address and input it as the
              destination address into which ever wallet you are currently using
            </p>
            <div className="boxView">
              <input
                type="text"
                className="ipVal"
                value={coinSelect.coin_address}
                readOnly
              />
              <div
                className="boxBtn"
                onClick={() =>
                  navigator.clipboard.writeText(coinSelect.coin_address)
                }
              >
                <img src={qr} alt="" />
              </div>
            </div>
            <h5>Step 2</h5>
            <p>
              Enter the transaction hash for the transfer for it to reflect into
              your InstaCrypto Vault.
            </p>
            <div className="boxView">
              <input
                type="text"
                className="ipVal"
                placeholder="Enter Hash Here"
                value={txnHash}
                onChange={(e) => setTxnHash(e.target.value)}
              />
              <div
                className="boxBtn"
                onClick={() => {
                  navigator.clipboard
                    .readText()
                    .then((clipText) => setTxnHash(clipText));
                }}
              >
                <img src={paste} alt="" />
              </div>
            </div>
            <div className="btnSubmit" onClick={saveTxn}>
              Submit Hash
            </div>
          </div>
        </div>
      ) : (
        <div className="addCoinComponent">
          {!coinType && (
            <div className="headTitle">
              What Type Of Asset Do You Want To Add?
            </div>
          )}
          {!coinFrom && (
            <div className="actionBtns">
              <Scrollbars
                className="botOptionWrapper"
                autoHide
                renderView={(props) => (
                  <div {...props} className="botOptionList" />
                )}
                renderThumbHorizontal={() => <div />}
                renderThumbVertical={() => <div />}
              >
                <div
                  className={`option ${coinType === 'crypto'} n-5`}
                  onClick={() =>
                    setCoinType((prev) => (prev === 'crypto' ? '' : 'crypto'))
                  }
                >
                  <img src={crypto} alt="" />
                  <span>Crypto</span>
                </div>
                <div
                  className={`option ${coinType === 'fiat'} n-5`}
                  onClick={() =>
                    setCoinType((prev) => (prev === 'fiat' ? '' : 'fiat'))
                  }
                >
                  <img src={fiat} alt="" />
                  <span>Fiat</span>
                </div>
              </Scrollbars>
            </div>
          )}
          {coinType && (
            <>
              {!coinFrom && (
                <div className="headTitle">
                  Where Are You Sending The Crypto From?
                </div>
              )}
              <div className="actionBtns">
                <Scrollbars
                  className="botOptionWrapper"
                  autoHide
                  renderView={(props) => (
                    <div {...props} className="botOptionList" />
                  )}
                  renderThumbHorizontal={() => <div />}
                  renderThumbVertical={() => <div />}
                >
                  {coinType === 'crypto' && (
                    <div
                      className={`option ${coinFrom === 'External'} n-5`}
                      onClick={() =>
                        setCoinFrom((prev) =>
                          prev === 'External' ? '' : 'External'
                        )
                      }
                    >
                      <img src={blockCheck} alt="" />
                      <span>BlockCheck</span>
                    </div>
                  )}
                  <div
                    className={`option ${coinFrom === 'gxapp'} n-5`}
                    onClick={() => {
                      setCoinFrom((prev) => (prev === 'gxapp' ? '' : 'gxapp'));
                    }}
                  >
                    <img src={vaults} alt="" />
                    <span>Vault</span>
                  </div>
                  <div
                    className={`option ${coinFrom === 'MoneyMarkets'} n-5`}
                    onClick={() =>
                      setCoinFrom((prev) =>
                        prev === 'MoneyMarkets' ? '' : 'MoneyMarkets'
                      )
                    }
                  >
                    <img src={moneyMarket} alt="" />
                    <span>MoneyMarkets</span>
                  </div>
                  <div
                    className={`option ${coinFrom === 'Bonds'} n-5`}
                    onClick={() =>
                      setCoinFrom((prev) => (prev === 'Bonds' ? '' : 'Bonds'))
                    }
                  >
                    <img src={investIcon} alt="" />
                    <span>Ice</span>
                  </div>
                </Scrollbars>
              </div>
            </>
          )}
          {coinFrom && (
            <>
              {!coinSelect && (
                <div className="headTitle">
                  Choose One Of The Following Coins
                </div>
              )}
              <div className="actionBtns">
                <Scrollbars
                  className="botOptionWrapper"
                  autoHide
                  renderView={(props) => (
                    <div {...props} className="botOptionList" />
                  )}
                  renderThumbHorizontal={() => <div />}
                  renderThumbVertical={() => <div />}
                >
                  {coinList
                    .filter((coin) => coin.type === coinType)
                    .filter((coin) =>
                      coinFrom === 'External' ? coin.native_deposit : true
                    )
                    .map((coin) => (
                      <div
                        className={`option ${coinSelect === coin} n-5`}
                        onClick={() => {
                          setCoinSelect(coin);
                          if (coinFrom === 'gxapp') {
                            setDepositModal(true);
                          }
                        }}
                      >
                        <img src={coin.coinImage} alt="" />
                        <span>{coin.coinName}</span>
                      </div>
                    ))}
                </Scrollbars>
              </div>
            </>
          )}
        </div>
      )}
      <FundVault
        coinSymbol={coinSelect.coinSymbol}
        key={`${depositModal}`}
        isDeposit={true}
        openModal={depositModal}
        setOpenModal={setDepositModal}
      />
    </>
  );
}

export default AddCoinComponent;
