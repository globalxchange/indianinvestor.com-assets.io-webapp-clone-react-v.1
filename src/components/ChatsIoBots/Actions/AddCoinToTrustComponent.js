import React, { useContext, useEffect, useState } from 'react';
import QRCode from 'qrcode.react';
import Axios from 'axios';
import qr from '../../../static/images/clipIcons/qr.svg';
import { BankContext } from '../../../context/Context';
import { ChatContext } from '../../../context/ChatContext';
import Skeleton from 'react-loading-skeleton';

function AddCoinToTrustComponent({ setUncover }) {
  const { coinListObject, email, tostShowOn, setChatOn } = useContext(
    BankContext
  );
  const { setTrustDepositOpen } = useContext(ChatContext);
  const [btcAddress, setbtcAddress] = useState('');
  const [qrShow, setQrShow] = useState(false);
  useEffect(() => {
    Axios.post(
      'https://comms.globalxchange.com/coin/vault/service/coin/request/address',
      {
        email: email,
        app_code: 'icetray',
      }
    ).then(({ data }) => {
      if (data.coin === 'BTC') {
        setbtcAddress(data.address);
      }
    });
  }, []);
  return (
    <div className="addCoinComponent final">
      <div className="headCoin">
        <img
          src={coinListObject?.BTC && coinListObject?.BTC.coinImage}
          alt=""
        />{' '}
        {coinListObject?.BTC.coinName}
      </div>
      <div className="steps">
        <h5>Step 1</h5>
        <p>
          Copy this Bitcoin address and input it as the destination address into
          which ever wallet you are currently using
        </p>
        <div className="boxView">
          {btcAddress ? (
            <input type="text" className="ipVal" value={btcAddress} readOnly />
          ) : (
            <Skeleton width="100%" height="100%" />
          )}
          <div
            className={`boxBtn ${btcAddress && qrShow}`}
            onClick={() => {
              setQrShow(!qrShow);
            }}
          >
            <div className="qrClose" />
            {btcAddress ? (
              <QRCode
                className="qr-svg"
                value={btcAddress}
                renderAs="svg"
                size={300}
              />
            ) : (
              <img src={qr} alt="" />
            )}
          </div>
        </div>
        <div className="buttonsList">
          <div
            className="btnClose"
            onClick={() => {
              setTrustDepositOpen(false);
              setChatOn(false);
            }}
          >
            Close
          </div>
          <div
            className="btnCopy"
            onClick={() =>
              navigator.clipboard.writeText(btcAddress).then(() => {
                tostShowOn('BTC Address Copied To Clipboard !');
              })
            }
          >
            Copy
          </div>
          <div
            className="btnUnCover"
            onClick={() => {
              setUncover(true);
            }}
          >
            I Hae More Questions
          </div>
        </div>
      </div>
    </div>
  );
}

export default AddCoinToTrustComponent;
