import React, { useContext } from 'react';
import Scrollbars from 'react-custom-scrollbars';
import depositIcon from '../../../static/images/clipIcons/deposit.svg';
import thisPageIcon from '../../../static/images/clipIcons/thisPage.svg';
import appIcon from '../../../static/images/clipIcons/app.svg';
import { ChatsIoContext } from '../../../context/ChatsIoContext';
import { BankContext } from '../../../context/Context';

function BotActionRefresh() {
  const { page } = useContext(ChatsIoContext);
  const { setRefreshPage } = useContext(BankContext);
  return (
    <div className="actionBtns">
      <div className="head">What Do You Want To Refresh?</div>
      <Scrollbars
        className="botOptionWrapper"
        renderView={(props) => <div {...props} className="botOptionList"></div>}
        renderTrackHorizontal={() => <div />}
        renderThumbHorizontal={() => <div />}
        renderTrackVertical={() => <div />}
        renderThumbVertical={() => <div />}
      >
        <div
          className="option"
          onClick={() => {
            setRefreshPage({ balanceUpdate: true });
            setTimeout(() => {
              setRefreshPage('');
            }, 200);
          }}
        >
          <img src={depositIcon} alt="" />
          <span>Balances</span>
        </div>
        <div
          className="option"
          onClick={() => {
            setRefreshPage(page);
            setTimeout(() => {
              setRefreshPage('');
            }, 200);
          }}
        >
          <img src={thisPageIcon} alt="" />
          <span>This Page</span>
        </div>
        <div className="option" onClick={() => window.location.reload()}>
          <img src={appIcon} alt="" />
          <span>App</span>
        </div>
      </Scrollbars>
    </div>
  );
}

export default BotActionRefresh;
