import React, { useState } from 'react';
import deposit from '../../static/images/clipIcons/deposit.svg';
import send from '../../static/images/clipIcons/send.svg';
import trade from '../../static/images/clipIcons/trade.svg';
import invest from '../../static/images/clipIcons/invest.svg';
import refresh from '../../static/images/clipIcons/refresh.svg';
import BotActionRefresh from './Actions/BotActionRefresh';
import AddCoinComponent from './Actions/AddCoinComponent';
import AddMarketAssetComponent from './Actions/AddMarketAssetComponent';
import AddCoinToTrustComponent from './Actions/AddCoinToTrustComponent';
import depositIcon from '../../static/images/clipIcons/deposit.svg';
import sendIcon from '../../static/images/clipIcons/send.svg';
import FundVault from '../VaultsPage/VaultFundWithdraw/FundVault';
import { useContext } from 'react';
import { ChatContext } from '../../context/ChatContext';
import { useEffect } from 'react';

function BotActions() {
  const [actionType, setActionType] = useState('');
  const [show, setShow] = useState(true);
  const {
    trustDepositOpen,
    setTrustDepositOpen,
    defiTrustProfileId,
  } = useContext(ChatContext);
  const [depositModal, setDepositModal] = useState(false);
  useEffect(() => {
    return () => {
      setTrustDepositOpen(false);
    };
  }, []);
  function getComponent() {
    switch (true) {
      case actionType === 'ADD':
        return <AddCoinComponent setShow={setShow} />;
      case actionType === 'refresh':
        return <BotActionRefresh />;
      case actionType === 'INVEST':
        return <AddMarketAssetComponent setShow={setShow} />;
      case actionType === 'DefiExternal':
        return <AddCoinToTrustComponent />;
      default:
        break;
    }
  }
  return (
    <>
      {show &&
        (!trustDepositOpen ? (
          <div className="actionBtns">
            <div className="head">What Are You Looking To Do?</div>
            <div className="botOptionWrapper">
              <div className="botOptionList">
                <div
                  className={`option n-5 ${actionType === 'ADD'}`}
                  onClick={() => setActionType('ADD')}
                >
                  <img src={deposit} alt="" />
                  <span>Add</span>
                </div>
                <div
                  className={`option n-5 ${actionType === 'SEND'}`}
                  onClick={() => setActionType('SEND')}
                >
                  <img src={send} alt="" />
                  <span>Send</span>
                </div>
                <div
                  className={`option n-5 ${actionType === 'TRADE'}`}
                  onClick={() => setActionType('TRADE')}
                >
                  <img src={trade} alt="" />
                  <span>Trade</span>
                </div>
                <div
                  className={`option n-5 ${actionType === 'INVEST'}`}
                  onClick={() => setActionType('INVEST')}
                >
                  <img src={invest} alt="" />
                  <span>Invest</span>
                </div>
                <div
                  className={`option n-5 ${actionType === 'refresh'}`}
                  onClick={() => setActionType('refresh')}
                >
                  <img src={refresh} alt="" />
                  <span>Refresh</span>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="actionBtns">
            <div className="head">How Do You Want To Capitalize Your Trust</div>
            <div className="botOptionWrapper">
              <div className="botOptionList">
                <div
                  className={`option n-5 ${actionType === 'DefiExternal'}`}
                  onClick={() => {
                    setActionType('DefiExternal');
                  }}
                >
                  <img src={depositIcon} alt="" />
                  <span>External</span>
                </div>
                <div
                  className="option  n-5"
                  onClick={() => {
                    setDepositModal(true);
                  }}
                >
                  <img src={sendIcon} alt="" />
                  <span>Connect</span>
                </div>
              </div>
            </div>
          </div>
        ))}
      {getComponent()}
      <FundVault
        key={`${depositModal}`}
        isDeposit={true}
        openModal={depositModal}
        setOpenModal={setDepositModal}
        defiTrustProfileId={defiTrustProfileId || true}
      />
    </>
  );
}

export default BotActions;
