import React, { useState, useEffect } from 'react';
import ChatLearnSelectCategory from './ChatLearnSelectCategory';
import ChatLearnShowContent from './ChatLearnShowContent';

function ChatLearn({ categories }) {
  const [category, setCategory] = useState('');
  useEffect(() => {
    setCategory('');
  }, [categories]);
  return (
    <>
      {category ? (
        <ChatLearnShowContent category={category} setCategory={setCategory} />
      ) : (
        <ChatLearnSelectCategory
          categories={categories}
          setCategory={setCategory}
        />
      )}
    </>
  );
}

export default ChatLearn;
