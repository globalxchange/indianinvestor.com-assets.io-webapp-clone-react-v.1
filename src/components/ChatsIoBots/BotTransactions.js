import React, { useContext, useEffect, useState } from 'react';
import Scrollbars from 'react-custom-scrollbars';
import depositIcon from '../../static/images/clipIcons/deposit.svg';
import sendIcon from '../../static/images/clipIcons/send.svg';
import tradeIcon from '../../static/images/clipIcons/trade.svg';
import SelectCoinTransaction from './Transaction/SelectCoinTransaction';
import { BankContext } from '../../context/Context';
import { ICON_LOGO } from '../../config/appConfig';

function BotTransactions() {
  const { bondDetail } = useContext(BankContext);
  const [transactionType, setTransactionType] = useState('');
  const [hideOptions, setHideOptions] = useState(true);
  useEffect(() => {
    setTransactionType('Bonds');
  }, [bondDetail]);
  return (
    <>
      {hideOptions && (
        <div className="actionBtns">
          <div className="head">Select Transaction Type</div>
          <Scrollbars
            className="botOptionWrapper"
            renderView={(props) => (
              <div {...props} className="botOptionList"></div>
            )}
            renderTrackHorizontal={() => <div />}
            renderThumbHorizontal={() => <div />}
            renderTrackVertical={() => <div />}
            renderThumbVertical={() => <div />}
          >
            <div
              className={`option ${transactionType === 'Deposit'}`}
              onClick={() => {
                setTransactionType('');
                setTimeout(() => {
                  setTransactionType('Deposit');
                }, 100);
              }}
            >
              <img src={depositIcon} alt="" />
              <span>Deposit</span>
            </div>
            <div
              className={`option ${transactionType === 'Withdraw'}`}
              onClick={() => {
                setTransactionType('');
                setTimeout(() => {
                  setTransactionType('Withdraw');
                }, 100);
              }}
            >
              <img src={sendIcon} alt="" />
              <span>Withdraw</span>
            </div>
            <div
              className={`option ${transactionType === 'Bonds'}`}
              onClick={() => {
                setTransactionType('');
                setTimeout(() => {
                  setTransactionType('Bonds');
                }, 100);
              }}
            >
              <img src={ICON_LOGO} alt="" />
              <span>Bonds</span>
            </div>
            <div
              className={`option ${transactionType === 'Trade'}`}
              onClick={() => {
                setTransactionType('');
                setTimeout(() => {
                  setTransactionType('Trade');
                }, 100);
              }}
            >
              <img src={tradeIcon} alt="" />
              <span>Trade</span>
            </div>
          </Scrollbars>
        </div>
      )}
      <SelectCoinTransaction
        transactionType={transactionType}
        setHideOptions={setHideOptions}
      />
    </>
  );
}

export default BotTransactions;
