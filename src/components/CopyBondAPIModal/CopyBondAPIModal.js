import { faCopy } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import { ICON_LOGO } from '../../config/appConfig';

function CopyBondAPIModal({ onClose, api }) {
  const [copied, setCopied] = useState(false);
  return (
    <div className="moreActionsWrapper">
      <div className="overlayClose" onClick={() => onClose()}></div>
      <div className="contentCard">
        <div className="head">
          <img src={ICON_LOGO} alt="" />
          Bond Actions
        </div>
        <div className="content copyContent">
          <div className="labelEndpoint">Endpoint</div>
          <div
            className="valueCpy"
            onClick={() => {
              navigator.clipboard.writeText(api).then(() => {
                setCopied(true);
                setTimeout(() => {
                  setCopied(false);
                }, 2000);
              });
            }}
          >
            <div className="value">
              {copied ? 'Copied To Clipboard...' : api}
            </div>
            <FontAwesomeIcon icon={faCopy} className="cpy" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default CopyBondAPIModal;
