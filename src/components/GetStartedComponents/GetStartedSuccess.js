import Axios from 'axios';
import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { BankContext } from '../../context/Context';

function GetStartedSuccess({ logo, mailId, password, loading, setLoading }) {
  const history = useHistory();
  const { login } = useContext(BankContext);
  function loginValidate() {
    setLoading(true);
    Axios.post('https://gxauth.apimachine.com/gx/user/login', {
      email: mailId,
      password,
    })
      .then((response) => {
        const { data } = response;
        if (data.status) {
          login(mailId, data.accessToken, data.idToken);
          history.push('/app');
        }
      })
      .finally(() => setLoading(false));
  }
  return (
    <div className="stepWrapper stepForm">
      <img src={logo} alt="assetLogo" className="assetLogo" />
      <div className="stepDesc">
        You Have Successfully Completed Your Registration
      </div>
      <div className="btns">
        <div className="btnBig" onClick={loginValidate}>
          Login To My Account
        </div>
        <div className="btnNext" onClick={() => history.push('/app')}>
          Close
        </div>
      </div>
    </div>
  );
}

export default GetStartedSuccess;
