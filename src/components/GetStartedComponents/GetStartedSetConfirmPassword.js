import { faEye, faEyeSlash } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect, useRef, useState } from 'react';

function GetStartedSetConfirmPassword({
  setStepName,
  logo,
  confirmPassword,
  setConfirmPassword,
  onSubmit,
  validateCircle,
  isValid,
}) {
  const [eye, setEye] = useState(false);
  const ref = useRef();
  useEffect(() => {
    setTimeout(() => {
      ref && ref.current.focus();
    }, 200);
  }, []);
  return (
    <div className="stepWrapper stepForm">
      <img src={logo} alt="assetLogo" className="assetLogo" />
      <div className="stepDesc">Great Job. Now Confirm Your Password</div>
      <div className="group">
        <input
          ref={ref}
          type={eye ? 'text' : 'password'}
          className="input password"
          placeholder="Password"
          value={confirmPassword}
          onChange={(e) => setConfirmPassword(e.target.value)}
          onKeyDown={(e) => {
            if (e.key === 'Enter') {
              setStepName('success');
            }
          }}
        />
        {validateCircle(isValid)}
        <FontAwesomeIcon
          icon={eye ? faEye : faEyeSlash}
          className="eye"
          onClick={() => setEye(!eye)}
        />
      </div>
      <div className="btns">
        <div className="btnBig" onClick={() => setStepName('setPassword')}>
          Change Password
        </div>
        <div
          className="btnNext"
          onClick={() => {
            setStepName('success');
            try {
              onSubmit();
            } catch (error) {}
          }}
        >
          Next
        </div>
      </div>
    </div>
  );
}

export default GetStartedSetConfirmPassword;
