import React from 'react';
import ChatArea from './ChatView/ChatArea';
import ChatInputs from './ChatView/ChatInputs';

function ChatsIoSupport() {
  return (
    <div className="supportChat">
      <ChatArea />
      <ChatInputs />
    </div>
  );
}

export default ChatsIoSupport;
