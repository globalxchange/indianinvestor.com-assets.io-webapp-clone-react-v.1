import React, { useContext, useEffect, useRef } from 'react';
import moment from 'moment';
import Scrollbars from 'react-custom-scrollbars';

import guest from '../../../static/images/guest.jpg';
import FilePreviewOnChat from './FilePreviewOnChat';
import { BankContext } from '../../../context/Context';
import { SupportChatContext } from '../../../context/SupportChatContext';
import { ICON_LOGO } from '../../../config/appConfig';

function ChatArea({}) {
  const { messageArray, typingFlag, chatUserObject } = useContext(
    SupportChatContext
  );
  const currentUser = chatUserObject?.username;
  const ref = useRef();
  useEffect(() => {
    ref.current.scrollToBottom();
    return () => {};
  }, [messageArray, typingFlag]);
  return (
    <Scrollbars
      autoHide
      className="chatView"
      ref={ref}
      renderTrackHorizontal={() => <div />}
      renderThumbHorizontal={() => <div />}
      renderTrackVertical={() => <div />}
      renderThumbVertical={() => <div />}
    >
      {messageArray.map((item) => {
        return item.type !== 'group' ? (
          <Message
            key={item.timestamp}
            time={item.timestamp}
            messageObj={item}
            isSent={currentUser === item.sender}
          />
        ) : (
          ''
          // <InChatInfo key={item.timestamp} message={item.message} />
        );
      })}
      {typingFlag && <TypingIndicator />}
    </Scrollbars>
  );
}
const Message = ({ messageObj, isSent }) => {
  const { profileImg } = useContext(BankContext);
  return (
    <>
      {isSent ? (
        <div className="answer">
          <div className="question-wrap">
            {messageObj.location && (
              <FilePreviewOnChat
                link={messageObj.location}
                type={messageObj.type}
              />
            )}
            <p className="question-text">{messageObj.message}</p>
          </div>
          <div className="bot">
            <img
              className="img-logo"
              src={profileImg || guest}
              alt={messageObj.sender}
            />
            <p>{moment(messageObj.timestamp).format('hh:mm A')}</p>
          </div>
        </div>
      ) : (
        <div className="question">
          <div className="bot">
            <img className="img-logo" src={ICON_LOGO} alt={messageObj.sender} />
            <p>{moment(messageObj.timestamp).format('hh:mm A')}</p>
          </div>
          <div className="question-wrap">
            {messageObj.location && (
              <FilePreviewOnChat
                link={messageObj.location}
                type={messageObj.type}
              />
            )}
            <p className="question-text">{messageObj.message}</p>
          </div>
        </div>
      )}
    </>
  );
};

const TypingIndicator = () => {
  return (
    <div className="question">
      <div className="bot">
        <img className="img-logo" src={ICON_LOGO} alt="" />
        <p>{moment().format('hh:mm A')}</p>
      </div>
      <div className="question-loading">
        <div class="lds-ellipsis">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    </div>
  );
};

export default ChatArea;
