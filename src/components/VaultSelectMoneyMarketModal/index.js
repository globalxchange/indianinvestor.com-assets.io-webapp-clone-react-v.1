import React, { useContext, useState } from 'react';
import classNames from './vaultSelectModal.module.scss';
import { useUserApps, useUserMoneMarketsList } from '../../queryHooks';
import { FormatCurrency } from '../../utils/FunctionTools';
import Skeleton from 'react-loading-skeleton';
import { VaultPageContext } from '../../context/VaultPageContext';
import { BankContext } from '../../context/Context';
import { DISPLAY_CURRENCY } from '../../config';

function VaultSelectMoneyMarketModal({
  onClose = () => {},
  onSuccess = () => {},
  email,
}) {
  const {
    appSelected,
    setAppSelected,
    assetClass,
    vaultSelected,
    setVaultSelected,
  } = useContext(VaultPageContext);
  const { coinListObject } = useContext(BankContext);
  const [appSearch, setAppSearch] = useState('');
  const [vaultSearch, setVaultSearch] = useState('');
  const { data: appList = [], isLoading: appListLoadinng } = useUserApps(email);
  const { data: vaultsList = [], isLoading: vaultListLoading } =
    useUserMoneMarketsList(email, appSelected?.app_code);
  return (
    <div className={classNames.vaultSelectModal}>
      <div
        className={classNames.overlayClose}
        onClick={() => {
          try {
            onClose();
          } catch (error) {}
        }}
      />
      <div className={classNames.vaultSelectCard}>
        <div className={classNames.header}>
          <img src={assetClass.icon} alt="" className={classNames.headIcon} />
        </div>
        <div className={classNames.headBar}>
          <input
            type="text"
            name="search"
            className={classNames.search}
            placeholder="Search Your Apps..."
            value={appSearch}
            onChange={(e) => setAppSearch(e.target.value)}
          />
          <div className={classNames.appList}>
            {appListLoadinng
              ? Array(6)
                  .fill('')
                  .map((_, i) => (
                    <div
                      className={`${classNames.appCard} ${classNames.true}`}
                      key={i}
                    >
                      <Skeleton className={classNames.icon} />
                    </div>
                  ))
              : appList
                  .filter(
                    (app) =>
                      app.app_name
                        .toLowerCase()
                        .includes(appSearch.toLowerCase()) ||
                      app.app_code
                        .toLowerCase()
                        .includes(appSearch.toLowerCase())
                  )
                  .map((app) => (
                    <div
                      className={`${classNames.appCard} ${
                        classNames[appSelected?.app_code === app.app_code]
                      }`}
                      key={app.app_code}
                      onClick={() => {
                        setAppSelected(app);
                      }}
                    >
                      <img
                        src={app.app_icon}
                        alt={app.app_name}
                        className={classNames.icon}
                      />
                    </div>
                  ))}
          </div>
          <input
            type="text"
            name="search"
            className={classNames.search}
            style={{
              paddingLeft: '40px',
            }}
            placeholder="Search Your Vaults..."
            onChange={(e) => setVaultSearch(e.target.value)}
            value={vaultSearch}
          />
        </div>
        <div className={classNames.tableHeader}>
          <div className={classNames.asset}>Asset</div>
          <div className={classNames.balance}>Balance</div>
          <div className={classNames.value}>Value</div>
        </div>
        <div className={classNames.tableView}>
          {vaultListLoading
            ? Array(10)
                .fill('')
                .map((_, i) => (
                  <div key={i} className={classNames.tableItem}>
                    <div className={classNames.asset}>
                      <Skeleton className={classNames.img} circle />
                      <Skeleton width={200} />
                    </div>
                    <div className={classNames.balance}>
                      <Skeleton width={150} />
                    </div>
                    <div className={classNames.value}>
                      <Skeleton width={150} />
                    </div>
                  </div>
                ))
            : vaultsList
                .filter(
                  (vault) =>
                    vault.coinSymbol
                      .toLowerCase()
                      .includes(vaultSearch.toLowerCase()) ||
                    vault.coinName
                      .toLowerCase()
                      .includes(vaultSearch.toLowerCase())
                )
                .map((vault) => (
                  <div
                    key={vault.coinSymbol}
                    className={`${classNames.tableItem} ${
                      classNames[vault === vaultSelected]
                    }`}
                    onClick={() => {
                      setVaultSelected(vault);
                      onSuccess();
                    }}
                  >
                    <div className={classNames.asset}>
                      <img
                        className={classNames.img}
                        src={vault.coinImage}
                        alt=""
                      />
                      <span>{vault.coinName}</span>
                    </div>
                    <div className={classNames.balance}>
                      <span>
                        {FormatCurrency(vault.coinValue, vault.coinSymbol)}
                        <small>{vault.coinSymbol}</small>
                      </span>
                    </div>
                    <div className={classNames.value}>
                      <span>
                        {FormatCurrency(
                          vault.coinValueUSD /
                            coinListObject?.[DISPLAY_CURRENCY]?.coinPriceUSD,
                          DISPLAY_CURRENCY
                        )}
                        <small>{DISPLAY_CURRENCY}</small>
                      </span>
                    </div>
                  </div>
                ))}
        </div>
      </div>
    </div>
  );
}

export default VaultSelectMoneyMarketModal;
