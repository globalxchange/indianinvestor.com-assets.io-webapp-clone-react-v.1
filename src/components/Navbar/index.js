import { motion, AnimatePresence } from 'framer-motion/dist/framer-motion';
import React, { useMemo, useState } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import classNames from './Navbar.module.scss';
import { FULL_LOGO } from '../../config';
import ios from '../../static/images/platforms/ios.svg';
import play from '../../static/images/platforms/play.svg';
import { Link } from 'react-router-dom';
import SendInviteModal from '../SendInviteModal/SendInviteModal';
import SendInviteModalPlatform from '../SendInviteModal/SendInviteModalPlatform';
import LoginModal from '../LoginModalNew';
import { ReactComponent as ArrowRight } from '../../static/images/arrowRight.svg';
import NavbarAppsSubMenu from '../NavbarAppsSubMenu';
import { ReactComponent as InvestorIcon } from '../../static/images/investorIcon.svg';
import { ReactComponent as BusinessIcon } from '../../static/images/businessIcon.svg';
import { ReactComponent as EducatorsIcon } from '../../static/images/educatorsIcon.svg';

function Navbar({ className }) {
  const { pathname } = useLocation();
  const [appMenuOpen, setAppMenuOpen] = useState(false);
  const [loginModalOpen, setLoginModalOpen] = useState(false);
  const [navOpen, setNavOpen] = useState(false);
  const history = useHistory();

  const [platform, setPlatform] = useState('');
  const [inviteModal, setInviteModal] = useState(false);

  const mainNavMenu = useMemo(() => {
    switch (true) {
      case pathname.includes('/businesses'):
        return (
          <div className={classNames.navMenu}>
            <Link
              to="/businesses/about/startups"
              className={`${classNames.navItem} ${
                classNames[pathname === '/businesses/about/startups']
              }`}
            >
              About
            </Link>
            <div className={classNames.navItem} onClick={() => {}}>
              ShareTokens
            </div>
            <div className={classNames.navItem} onClick={() => {}}>
              Features
            </div>
            <div className={classNames.navItem} onClick={() => {}}>
              Pricing
            </div>
            <div
              className={`${classNames.navItem} ${classNames.btn} ${classNames.login}`}
              onClick={() => setLoginModalOpen(true)}
            >
              <span className={classNames.btnIn}>Login</span>
            </div>
            <div
              className={`${classNames.navItem} ${classNames.btn} ${classNames.appMenu}`}
              onClick={() => setAppMenuOpen(!appMenuOpen)}
            >
              <span className={classNames.btnIn}>Apps</span>
            </div>
          </div>
        );

      default:
        return (
          <div className={classNames.navMenu}>
            <Link
              to="/investors/about"
              className={`${classNames.navItem} ${
                classNames[pathname === '/investors/about']
              }`}
            >
              About
            </Link>
            <Link
              to="/market"
              className={`${classNames.navItem} ${
                classNames[pathname === '/market']
              }`}
              onClick={() => {}}
            >
              Markets
            </Link>
            <div className={classNames.navItem} onClick={() => {}}>
              Plugins
            </div>
            <div className={classNames.navItem} onClick={() => {}}>
              Affiliates
            </div>
            <div
              className={`${classNames.navItem} ${classNames.btn} ${classNames.login}`}
              onClick={() => setLoginModalOpen(true)}
            >
              <span className={classNames.btnIn}>Login</span>
            </div>
            <div
              className={`${classNames.navItem} ${classNames.btn} ${classNames.appMenu}`}
              onClick={() => setAppMenuOpen(!appMenuOpen)}
            >
              <span className={classNames.btnIn}>Apps</span>
            </div>
          </div>
        );
    }
  }, [pathname]);

  return (
    <>
      <div className={`${className} ${classNames.navWrap}`}>
        <div className={classNames.topBar}>
          <div className={classNames.menu}>
            <Link
              to="/investors/about"
              className={`${classNames.menuItem} ${
                classNames[pathname.includes('/investors')]
              }`}
            >
              <InvestorIcon />
              For Investors
            </Link>
            <Link
              to="/businesses/about/startups"
              className={`${classNames.menuItem} ${
                classNames[pathname.includes('/businesses')]
              }`}
            >
              <BusinessIcon />
              For Businesses
            </Link>
            <div className={`${classNames.menuItem} ${classNames.false}`}>
              <EducatorsIcon />
              For Educators
            </div>
          </div>
        </div>
        <nav className={classNames.mainNavbar}>
          <div
            onClick={() => setNavOpen(!navOpen)}
            className={`${classNames.hamburger} ${
              classNames.hamburgerSqueeze
            } ${navOpen ? classNames.isActive : ''}`}
          >
            <span className={classNames.hamburgerBox}>
              <span className={classNames.hamburgerInner}></span>
            </span>
          </div>
          <Link to="/" className={classNames.logo}>
            <img src={FULL_LOGO} alt="" />
          </Link>
          {mainNavMenu}
        </nav>
        {pathname.includes('/businesses/about/') && (
          <div className={classNames.btmBar}>
            <div className={classNames.menu}>
              <div className={`${classNames.menuItem} ${classNames.true}`}>
                Startups
              </div>
              <div className={`${classNames.menuItem} ${classNames.false}`}>
                SME’s
              </div>
              <div className={`${classNames.menuItem} ${classNames.false}`}>
                Firms
              </div>
              <div className={`${classNames.menuItem} ${classNames.false}`}>
                Parlours
              </div>
              <div className={`${classNames.menuItem} ${classNames.false}`}>
                Blue Chip
              </div>
            </div>
          </div>
        )}
      </div>
      {inviteModal &&
        (platform ? (
          <SendInviteModal
            onClose={() => {
              setPlatform('');
              setInviteModal(false);
            }}
            platform={platform}
          />
        ) : (
          <SendInviteModalPlatform
            onClose={() => {
              setPlatform('');
              setInviteModal(false);
            }}
            setPlatform={setPlatform}
          />
        ))}
      {loginModalOpen && (
        <LoginModal
          onClose={() => setLoginModalOpen(false)}
          onSuccess={() => {
            setLoginModalOpen(false);
            history.push('/app');
          }}
        />
      )}
      <NavbarAppsSubMenu openTab={appMenuOpen} setOpenTab={setAppMenuOpen} />
      <AnimatePresence exitBeforeEnter initial={false}>
        {navOpen && (
          <SidebarMobile
            onClose={() => setNavOpen(false)}
            setPlatform={setPlatform}
            setInviteModal={setInviteModal}
          />
        )}
      </AnimatePresence>
    </>
  );
}

function SidebarMobile({ onClose, setPlatform, setInviteModal }) {
  const { pathname } = useLocation();
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      className={classNames.sidebarMobile}
      transition={{ ease: 'easeOut', duration: 0.5 }}
    >
      <div className={classNames.overlay} onClick={() => onClose()} />
      <motion.div
        initial={{ x: '-100%' }}
        animate={{ x: 0 }}
        exit={{ x: '-100%' }}
        transition={{ ease: 'easeOut', duration: 0.5 }}
        className={classNames.menuWrap}
      >
        <Link to="/signup" className={classNames.header}>
          Get Started
          <ArrowRight className={classNames.icon} />
        </Link>
        <div className={classNames.navMenu}>
          <Link
            to="/"
            className={`${classNames.navItem} ${classNames[pathname === '/']}`}
          >
            Home
          </Link>
          <Link
            to="/about"
            className={`${classNames.navItem} ${
              classNames[pathname === '/about']
            }`}
          >
            About
          </Link>
          <Link
            to="/markets"
            className={`${classNames.navItem} ${
              classNames[pathname === '/markets']
            }`}
          >
            Markets
          </Link>
          <Link
            to="/investors"
            className={`${classNames.navItem} ${
              classNames[pathname === '/investors']
            }`}
          >
            For Investors
          </Link>
          <div
            className={`${classNames.navItem} ${
              classNames['' === 'countries']
            }`}
            onClick={() => {}}
          >
            For Startups
          </div>
          <div className={classNames.navItem} onClick={() => {}}>
            For Educators
          </div>

          <div
            className={classNames.btnIos}
            onClick={() => {
              setPlatform('ios');
              setInviteModal(true);
            }}
          >
            <img src={ios} alt="" className={classNames.icon} />
            <div className={classNames.texts}>
              <div className={classNames.downOn}>Download On</div>
              <div className={classNames.sorce}>App Store</div>
            </div>
          </div>
          <div
            className={classNames.btnAndroid}
            onClick={() => {
              setPlatform('android');
              setInviteModal(true);
            }}
          >
            <img src={play} alt="" className={classNames.icon} />
            <div className={classNames.texts}>
              <div className={classNames.downOn}>Download On</div>
              <div className={classNames.sorce}>Google Play</div>
            </div>
          </div>
        </div>
      </motion.div>
    </motion.div>
  );
}

export default Navbar;
