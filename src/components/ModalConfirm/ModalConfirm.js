import React from 'react';
import { APP_NAME, ICON_LOGO } from '../../config/appConfig';

function ModalConfirm({ onClose, onConfirm, text, setOpenModal }) {
  return (
    <div className="modalConfirm">
      <div
        className="overlayClose"
        onClick={() => {
          try {
            onClose();
          } catch (error) {}
          setOpenModal(false);
        }}
      />
      <div className="modalContent">
        <div className="head">
          <img src={ICON_LOGO} alt="" />
          <h5>{APP_NAME}</h5>
        </div>
        <div className="contents">
          <div className="text">{text}</div>
          <div className="buttons">
            <div
              className="btn-confirm"
              onClick={() => {
                try {
                  onConfirm();
                } catch (error) {}
                setOpenModal(false);
              }}
            >
              Confirm
            </div>
            <div
              className="btn-cancel"
              onClick={() => {
                try {
                  onClose();
                } catch (error) {}
                setOpenModal(false);
              }}
            >
              Never Mind
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ModalConfirm;
