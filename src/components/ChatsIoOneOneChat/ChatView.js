import React, { useContext } from 'react';
import { ChatsIoContext } from '../../context/ChatsIoContext';
import MessageList from './MessageList';
import backIcon from '../../static/images/back.svg';

const ChatView = () => {
  const { selectedUser, setSelectedUser } = useContext(ChatsIoContext);

  return (
    <div className="flex-fill d-flex flex-column chat-layout-container">
      <div className="chat-header">
        <img
          src={backIcon}
          alt=""
          className="chat-back-button"
          onClick={() => setSelectedUser()}
        />
        <img src={selectedUser?.avatar} alt="" className="chat-user-avatar" />
        <div className="chat-user-container">
          <div className="chat-user-name">{selectedUser?.first_name}</div>
          <div className="chat-user-bio">{selectedUser?.bio}</div>
        </div>
      </div>
      <MessageList />
    </div>
  );
};

export default ChatView;
