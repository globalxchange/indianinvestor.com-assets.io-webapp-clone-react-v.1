import React, { useContext, useEffect } from 'react';
import moment from 'moment-timezone';
import guest from '../../static/images/guest.jpg';
// import FilePreviewOnChat from '../FilePreviewOnChat';

import LoadingAnim from '../LoadingAnim/LoadingAnim';
import { ChatsIoContext } from '../../context/ChatsIoContext';
import ChatInputArea from './ChatInputArea';
import { BankContext } from '../../context/Context';
import { ICON_LOGO } from '../../config/appConfig';

const MessageList = () => {
  const {
    messageArray,
    isUserTyping,
    userObject,
    selectedUser,
    sendMessage,
    notifyTyping,
    setSelectedUser,
  } = useContext(ChatsIoContext);

  const { tostShowOn } = useContext(BankContext);

  useEffect(() => {
    if (messageArray?.length > 1) {
      const objDiv = document.getElementById('chat-area');
      // console.log('top: ', objDiv.scrollTop, 'height: ', objDiv.scrollHeight);
      try {
        objDiv.scrollTop = objDiv.scrollHeight;
      } catch (error) {}

      // console.log(objDiv);
    }
  }, [messageArray, isUserTyping]);

  const handleSubmitMessage = (inputText, setInputText) => {
    const message = inputText.trim();

    if (!message) {
      tostShowOn('Please Input A Message');
    }

    sendMessage(message);
    setInputText('');
  };

  return (
    <div className="chat-area-wrapper d-flex flex-column">
      {messageArray ? (
        <>
          {messageArray?.length > 0 ? (
            <div
              id="chat-area"
              className="chats flex-grow-1"
              style={{ height: 0, overflowY: 'auto' }}
            >
              {messageArray?.map((item) => {
                return item.type !== 'group' ? (
                  <Message
                    key={item.timestamp}
                    messageObj={item}
                    isSent={userObject?.username === item.sender?.username}
                  />
                ) : (
                  <InChatInfo key={item.timestamp} message={item.message} />
                );
              })}
            </div>
          ) : (
            <div className="d-flex flex-fill flex-column justify-content-center">
              <div className="text-center">
                Start Your Conversation with {selectedUser?.first_name}
              </div>
            </div>
          )}
          <br />
          {isUserTyping && (
            <div className="typing-indicator">
              {`${selectedUser?.first_name?.trim()} is Typing`}
              &nbsp;&nbsp;&nbsp;
              <div className="ticontainer">
                <div className="tiblock">
                  <div className="tidot" />
                  <div className="tidot" />
                  <div className="tidot" />
                </div>
              </div>
            </div>
          )}
          <ChatInputArea
            onTypingHandler={notifyTyping}
            onSubmit={handleSubmitMessage}
            isFileLoading={false}
            initiateSendImage={() => null}
            disabledFilePicker
          />
        </>
      ) : (
        <div className="d-flex flex-fill flex-column justify-content-center">
          <LoadingAnim />
        </div>
      )}
    </div>
  );
};

const InChatInfo = ({ message }) => {
  return (
    <div className="in-chat-info">
      <span>{message}</span>
    </div>
  );
};

const Message = ({ messageObj, isSent }) => {
  const { profileImg } = useContext(BankContext);

  return (
    <>
      {isSent ? (
        <div className="answer">
          <div className="question-wrap">
            {/* {messageObj.location && (
              <FilePreviewOnChat
                link={messageObj.location}
                type={messageObj.type}
              />
            )} */}
            <p className="question-text">{messageObj.message}</p>
          </div>
          <div className="bot">
            <img
              className="img-logo"
              src={profileImg || guest}
              alt={messageObj.sender}
            />
            <p>{moment(messageObj.timestamp).format('hh:mm A')}</p>
          </div>
        </div>
      ) : (
        <div className="question">
          <div className="bot">
            <div className="app-logo-container">
              <img
                className="app-logo"
                src={ICON_LOGO}
                alt={messageObj.sender}
              />
            </div>

            <p>{moment(messageObj.timestamp).format('hh:mm A')}</p>
          </div>
          <div className="question-wrap">
            {/* {messageObj.location && (
              <FilePreviewOnChat
                link={messageObj.location}
                type={messageObj.type}
              />
            )} */}
            <p className="question-text">{messageObj.message}</p>
          </div>
        </div>
      )}
    </>
  );
};

export default MessageList;
