import Axios from 'axios';
import React, { Fragment, useContext, useState } from 'react';
import countryCodes from 'country-codes-list';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

import emailIcn from '../../static/images/clipIcons/mail.svg';
import phoneIcn from '../../static/images/clipIcons/phone.svg';
import LoadingAnim from '../LoadingAnim/LoadingAnim';
import { BankContext } from '../../context/Context';
import { APP_CODE } from '../../config/appConfig';

function SendAppLink({ platform, type }) {
  const { email, tostShowOn } = useContext(BankContext);
  const myCountryCodesObject = countryCodes.customList(
    'countryCode',
    '+{countryCallingCode}'
  );
  const myCountryNamesObject = countryCodes.customList(
    'countryCode',
    '{countryNameEn}'
  );
  const [country, setCountry] = useState('IN');
  const [dropOpen, setDropOpen] = useState(false);
  const [search, setSearch] = useState('');
  const [mobile, setMobile] = useState('');
  const [mailId, setEmail] = useState('');
  const [loading, setLoading] = useState(false);
  const [res, setRes] = useState('');
  function getLink() {
    setLoading(true);
    Axios.post(
      'https://comms.globalxchange.com/coin/vault/service/send/app/links/invite',
      {
        userEmail: email || mailId,
        app_code: APP_CODE,
        email: mailId,
        mobile: `${myCountryCodesObject[country]}${mobile}`,
        app_type: platform,
        custom_message: 'Download Now!',
      }
    )
      .then(({ data }) => {
        if (data.status) {
          setRes(data);
        } else {
          tostShowOn(data.message || 'Something Went Wrong');
        }
      })
      .finally(() => setLoading(false));
  }
  function getCountry() {
    return (
      <div className="selectCountry">
        <div className="title">Get Country Code</div>
        <label className="searchWrapper">
          <input
            type="text"
            value={search}
            className="number"
            placeholder="Enter The Country"
            onChange={(e) => setSearch(e.target.value)}
          />
          <FontAwesomeIcon icon={faSearch} />
        </label>
        <div className="dropList">
          {Object.keys(myCountryCodesObject)
            .filter(
              (key) =>
                myCountryCodesObject[key].includes(search) ||
                myCountryNamesObject[key]
                  .toLowerCase()
                  .includes(search.toLowerCase()) ||
                key.includes(search.toUpperCase())
            )
            .map((key) => (
              <div
                className="item"
                key={key}
                onClick={() => {
                  setCountry(key);
                  setDropOpen(false);
                }}
              >
                <img
                  src={`https://www.countryflags.io/${key}/shiny/64.png`}
                  alt=""
                  className="img"
                />
                <span className="name">{myCountryNamesObject[key]}</span>
                <span className="code">{myCountryCodesObject[key]}</span>
              </div>
            ))}
        </div>
      </div>
    );
  }
  const getData = () => {
    switch (true) {
      case loading:
        return (
          <div className="contentInvite">
            <LoadingAnim />
          </div>
        );
      case type === 'mail':
        return (
          <Fragment key="0">
            <div className="contentInvite">
              <div className="title">Input Email</div>
              <label htmlFor="">
                Enter The Email You Want To Send The Invitation Link To
              </label>
              <div className="inputBox">
                <input
                  type="text"
                  value={mailId}
                  onChange={(e) => setEmail(e.target.value)}
                  className="number"
                  placeholder="Enter Email"
                />
                <div className="countryFlag" onClick={getLink}>
                  <img className="flag" src={emailIcn} alt="" />
                </div>
              </div>
            </div>
          </Fragment>
        );
      case type === 'phone':
        return (
          <Fragment key="1">
            <div className="contentInvite">
              <div className="title">Phone Number</div>
              <label>
                Enter The Phone Number You Want To Send The Invitation Link To
              </label>
              <div className={`inputBox ${dropOpen}`}>
                <div
                  className="dropDown"
                  onClick={() => setDropOpen(!dropOpen)}
                >
                  <img
                    className="flag"
                    src={`https://www.countryflags.io/${country}/shiny/64.png`}
                    alt=""
                  />
                </div>
                <input
                  type="number"
                  value={mobile}
                  onChange={(e) => setMobile(e.target.value)}
                  className="number"
                  placeholder="Enter Phone Number"
                />
                <div className="countryFlag" onClick={getLink}>
                  <img className="flag" src={phoneIcn} alt="" />
                </div>
              </div>
              {dropOpen && getCountry()}
            </div>
          </Fragment>
        );
    }
  };

  return (
    <div className="sendAppLink">
      {res ? (
        <div className="contentInvite">
          <div className="title">Congratulations</div>
          <label>
            You Were Just Sent The Latest Version Of The {platform} Asset.io
            App.
          </label>
        </div>
      ) : (
        getData()
      )}
    </div>
  );
}

export default SendAppLink;
