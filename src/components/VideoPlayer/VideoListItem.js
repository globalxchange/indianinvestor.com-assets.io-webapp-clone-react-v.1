import Axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import Skeleton from 'react-loading-skeleton';
import { BankContext } from '../../context/Context';
import videHolder from '../../static/images/placeHolder/videoHolderGrey.svg';

function VideoListItem({ videoItm }) {
  const { setVideoShow, videoShow } = useContext(BankContext);
  const [videoUrl, setVideoUrl] = useState('');
  useEffect(() => {
    Axios.post(
      'https://vod-backend.globalxchange.io/get_user_profiled_video_stream_link',
      {
        video_id: videoItm.video,
      }
    ).then(({ data }) => setVideoUrl(data));
  }, []);
  return (
    <div className="vidItm">
      <div className="vhSmall">
        <img className="vhHolder" src={videHolder} alt="" />
        {videoUrl ? (
          <img
            className="videoThumb"
            src={videoItm.image}
            width="100%"
            height="100%"
            alt=""
            onClick={() =>
              setVideoShow({
                ...videoItm,
                videoUrl,
              })
            }
          />
        ) : (
          <Skeleton className="videoThumb" width="100%" height="100%" />
        )}
      </div>
      <div className="textContent">
        <div className="cardTitle">{videoItm.title}</div>
        <div className="cardDesc">{videoItm.desc}</div>
      </div>
    </div>
  );
}

export default VideoListItem;
