import React, { memo } from 'react';
import classNames from './navbarAppsSubMenu.module.scss';
import bharatTrustIcon from '../../static/images/appsIcons/bharatTrust.svg';
import indiaCoinsIcon from '../../static/images/appsIcons/indiaCoins.svg';
import indianInvestorIcon from '../../static/images/appsIcons/indianInvestor.svg';
import indianMarketIcon from '../../static/images/appsIcons/indianMarket.svg';
import indianOtcIcon from '../../static/images/appsIcons/indianOtc.svg';
import taxChainsIcon from '../../static/images/appsIcons/taxChains.svg';

const APPS_LIST = [
  {
    name: 'IndianOTC',
    desc: 'Digital Asset Trading Hub',
    icon: indianOtcIcon,
    link: 'https://indianotc.com/',
  },
  {
    name: 'IndianInvestor',
    desc: 'Wealth Management System',
    icon: indianInvestorIcon,
    link: 'https://www.indianinvestor.com',
  },
  {
    name: 'TaxChains',
    desc: 'Crypto Tax Software',
    icon: taxChainsIcon,
    link: 'https://www.taxchains.com',
  },
  {
    name: 'IndiaCoins',
    desc: 'India’s NFT Marketplace',
    icon: indiaCoinsIcon,
    link: 'https://www.indiacoins.com',
  },
  {
    name: 'BharatTrust',
    desc: 'Social Investing Platform',
    icon: bharatTrustIcon,
    link: 'https://www.bharattrust.com',
  },
  {
    name: 'IndianMarket',
    desc: 'Financial Media & News',
    icon: indianMarketIcon,
    link: 'https://www.indian.market',
  },
];

function NavbarAppsSubMenu({ openTab, setOpenTab }) {
  return (
    <div
      className={`${classNames.topNavbarAppsSubMenuWrap} ${
        classNames[openTab.toString()]
      }`}
    >
      {openTab && (
        <div className={classNames.overlay} onClick={() => setOpenTab(false)} />
      )}
      <div className={classNames.topNavbarAppsSubMenu}>
        <div className={classNames.tabDetailContent}>
          <div className={classNames.titlle}>
            One Account. Mutiple Advantages
          </div>
          <div className={classNames.subtitle}>
            With An IndianInvestor Account You Also Get Access To The INR Group
            Apps
          </div>
          <div className={classNames.actionList}>
            {APPS_LIST.map((app, i) => (
              <a
                href={app.link}
                target="_blank"
                key={i}
                className={classNames.action}
                rel="noreferrer"
              >
                <img src={app.icon} alt="" className={classNames.icon} />
                <div className={classNames.detail}>
                  <div className={classNames.actName}>{app.name}</div>
                  <div className={classNames.actDesc}>{app.desc}</div>
                </div>
              </a>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default memo(NavbarAppsSubMenu);
