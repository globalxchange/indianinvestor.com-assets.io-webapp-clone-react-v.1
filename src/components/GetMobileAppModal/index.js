import React, { Fragment, useContext, useState } from 'react';
import countryCodes from 'country-codes-list';
import Axios from 'axios';
import classNames from './getMobileAppModal.module.scss';
import { FULL_LOGO } from '../../config';
import arrowGo from '../../static/images/clipIcons/arrowGo.svg';
import android from '../../static/images/clipIcons/androidColor.svg';
import ios from '../../static/images/clipIcons/iosColor.svg';
import { BankContext } from '../../context/Context';
import { APP_CODE } from '../../config/appConfig';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCopy } from '@fortawesome/free-regular-svg-icons';

import emailIcn from '../../static/images/clipIcons/email.svg';
import phoneIcn from '../../static/images/clipIcons/phone.svg';
import bothIcn from '../../static/images/clipIcons/both.svg';

import LoadingAnim from '../LoadingAnim/LoadingAnim';

function GetMobileAppModal({ onClose = () => {}, onSuccess = () => {} }) {
  const { email } = useContext(BankContext);
  const [device, setDevice] = useState('');
  const [platform, setPlatform] = useState('');

  function getContent() {
    switch (true) {
      case platform === 'ios' || platform === 'android':
        return (
          <SendInviteMobile onClose={() => setDevice('')} platform={platform} />
        );
      case device === 'mobile':
        return (
          <SelectPlatformMobile
            onClose={() => setDevice('')}
            setPlatform={setPlatform}
          />
        );
      default:
        return <SelectDevice onClose={onSuccess} setDevice={setDevice} />;
    }
  }

  return (
    <div className={classNames.getMobileAppModal}>
      <div
        className={classNames.overlayClose}
        onClick={() => {
          try {
            onClose();
          } catch (error) {}
        }}
      />
      {getContent()}
    </div>
  );
}

function SelectDevice({ onClose, setDevice }) {
  return (
    <div className={classNames.settingsCard}>
      <div className={classNames.inCard}>
        <img src={FULL_LOGO} alt="" className={classNames.logo} />
        <div className={classNames.settingCards}>
          <div
            className={classNames.settingCard}
            onClick={() => setDevice('mobile')}
          >
            <span>Mobile</span>
            <img src={arrowGo} alt="" className={classNames.icon} />
          </div>
          <div
            className={classNames.settingCard}
            onClick={() => {}}
            style={{
              opacity: 0.25,
            }}
          >
            <span>Desktop</span>
            <img src={arrowGo} alt="" className={classNames.icon} />
          </div>
          <div
            className={classNames.settingCard}
            onClick={() => {}}
            style={{
              opacity: 0.25,
            }}
          >
            <span>Extentions</span>
            <img src={arrowGo} alt="" className={classNames.icon} />
          </div>
        </div>
      </div>
      <div className={classNames.footerBtns}>
        <div
          className={classNames.btnSettings}
          onClick={() => {
            onClose();
          }}
        >
          <span>Go Back</span>
        </div>
      </div>
    </div>
  );
}

function SelectPlatformMobile({ onClose, setPlatform }) {
  return (
    <div className={classNames.settingsCard}>
      <div className={classNames.inCard}>
        <img src={FULL_LOGO} alt="" className={classNames.logo} />
        <div className={classNames.settingCards}>
          <div
            className={classNames.settingCard}
            onClick={() => setPlatform('ios')}
          >
            <img src={ios} alt="" className={classNames.logoPlat} />
            <span>IOS</span>
            <img src={arrowGo} alt="" className={classNames.icon} />
          </div>
          <div
            className={classNames.settingCard}
            onClick={() => setPlatform('android')}
          >
            <img src={android} alt="" className={classNames.logoPlat} />
            <span>Android</span>
            <img src={arrowGo} alt="" className={classNames.icon} />
          </div>
        </div>
      </div>
      <div className={classNames.footerBtns}>
        <div
          className={classNames.btnSettings}
          onClick={() => {
            onClose();
          }}
        >
          <span>Go Back</span>
        </div>
      </div>
    </div>
  );
}

function SendInviteMobile({ onClose, platform }) {
  const myCountryCodesObject = countryCodes.customList(
    'countryCode',
    '+{countryCallingCode}'
  );
  const myCountryNamesObject = countryCodes.customList(
    'countryCode',
    '{countryNameEn}'
  );
  const [country, setCountry] = useState('IN');
  const [dropOpen, setDropOpen] = useState(false);
  const [search, setSearch] = useState('');
  const [mobile, setMobile] = useState('');
  const [mailId, setEmail] = useState('');
  const [loading, setLoading] = useState(false);
  const [res, setRes] = useState('');
  const [resLink, setResLink] = useState('');
  const [copied, setCopied] = useState(false);
  function getLink() {
    setLoading(true);
    Axios.post(
      'https://comms.globalxchange.com/coin/vault/service/send/app/links/invite',
      {
        userEmail: mailId,
        app_code: APP_CODE,
        email: mailId,
        mobile: `${myCountryCodesObject[country]}${mobile}`,
        app_type: platform,
        custom_message: 'Download Now!',
      }
    )
      .then(({ data }) => {
        if (data.status) {
          setRes(data);
        } else {
          alert(data.message || 'Something Went Wrong');
        }
      })
      .finally(() => setLoading(false));
    Axios.get(
      `https://comms.globalxchange.com/gxb/apps/mobile/app/links/logs/get?app_code=${APP_CODE}`
    ).then(({ data }) => {
      setResLink(data.logs[0]);
    });
  }
  const [step, setStep] = useState(0);
  const [tempStep, setTempStep] = useState(null);
  const [emailOnly, setEmailOnly] = useState(false);

  const getData = () => {
    switch (true) {
      case loading:
        return (
          <div className={classNames.contentInvite}>
            <LoadingAnim />
          </div>
        );
      case step === 1:
        return (
          <Fragment key="0">
            <div className={classNames.contentInvite}>
              <label htmlFor="">What Is Your Recipient’s Email Address</label>
              <div className={classNames.inputBox}>
                <input
                  type="text"
                  value={mailId}
                  onChange={(e) => setEmail(e.target.value)}
                  className={classNames.number}
                  placeholder="Enter Email"
                />
                <div className={classNames.countryFlag}>
                  <img className={classNames.flag} src={emailIcn} alt="" />
                </div>
              </div>
            </div>
            {emailOnly ? (
              <div className={classNames.footer} onClick={getLink}>
                Get Link
              </div>
            ) : (
              <div className={classNames.footer} onClick={() => setStep(2)}>
                <span>Next</span>
              </div>
            )}
          </Fragment>
        );
      case step === 2:
        return (
          <Fragment key="1">
            <div className={classNames.contentInvite}>
              <label htmlFor="">
                {!dropOpen
                  ? 'What Is Your Recipient’s Phone Number'
                  : 'Find Your Country Code'}
              </label>
              <div className={`${classNames.inputBox} ${classNames[dropOpen]}`}>
                {!dropOpen && (
                  <label className={classNames.dropDown}>
                    <div onClick={() => setDropOpen(!dropOpen)}>
                      <span>{myCountryCodesObject[country]}</span>
                    </div>
                  </label>
                )}
                {dropOpen ? (
                  <input
                    type="text"
                    value={search}
                    className={classNames.number}
                    placeholder="Enter The Country"
                    onChange={(e) => setSearch(e.target.value)}
                  />
                ) : (
                  <input
                    type="number"
                    value={mobile}
                    onChange={(e) => setMobile(e.target.value)}
                    className={classNames.number}
                    placeholder="Enter Phone Number"
                  />
                )}
                <div
                  className={classNames.countryFlag}
                  onClick={() => setDropOpen(!dropOpen)}
                >
                  <img
                    className={classNames.flag}
                    src={`https://www.countryflags.io/${country}/shiny/64.png`}
                    alt=""
                  />
                </div>
                {dropOpen && (
                  <div className={classNames.dropList}>
                    {Object.keys(myCountryCodesObject)
                      .filter(
                        (key) =>
                          myCountryCodesObject[key].includes(search) ||
                          myCountryNamesObject[key]
                            .toLowerCase()
                            .includes(search.toLowerCase()) ||
                          key.includes(search.toUpperCase())
                      )
                      .map((key) => (
                        <div
                          className={classNames.item}
                          key={key}
                          onClick={() => {
                            setCountry(key);
                            setDropOpen(false);
                          }}
                        >
                          <span>{myCountryNamesObject[key]}</span>
                          <span>{myCountryCodesObject[key]}</span>
                        </div>
                      ))}
                  </div>
                )}
              </div>
            </div>
            <div className={classNames.footer} onClick={getLink}>
              <span>Get Link</span>
            </div>
          </Fragment>
        );
      default:
        return (
          <Fragment key="0">
            <div className={classNames.contentInvite}>
              <label htmlFor="">
                How Do You Want To Recieve The Download Link?
              </label>
              <div className={classNames.links}>
                <div
                  className={`${classNames.link} ${
                    classNames[tempStep === 1 && emailOnly]
                  }`}
                  onClick={() => {
                    setTempStep(1);
                    setEmailOnly(true);
                  }}
                >
                  <img src={emailIcn} alt="" />
                  <span>Email</span>
                </div>
                <div
                  className={`${classNames.link} ${classNames[tempStep === 2]}`}
                  onClick={() => {
                    setTempStep(2);
                  }}
                >
                  <img src={phoneIcn} alt="" />
                  <span>Text</span>
                </div>
                <div
                  className={`${classNames.link} ${
                    classNames[tempStep === 1 && !emailOnly]
                  }`}
                  onClick={() => {
                    setTempStep(1);
                    setEmailOnly(false);
                  }}
                >
                  <img src={bothIcn} alt="" />
                  <span>Both</span>
                </div>
              </div>
            </div>
            <div
              className={classNames.footer}
              onClick={() => setStep(tempStep)}
            >
              <span>Next</span>
            </div>
          </Fragment>
        );
    }
  };

  return (
    <div className={classNames.settingsCard}>
      <div className={classNames.inCard}>
        <img src={FULL_LOGO} alt="" className={classNames.logo} />
        {res ? (
          <div className={classNames.contentInvite}>
            <div className={classNames.hCongrats}>Congratulations</div>
            <p className={classNames.message}>
              We Have Texted &amp; Emailed You A Download Link For The
              Discussion.app App For {platform}
            </p>
            <div
              className={`${classNames.inputBox} ${classNames.res}`}
              onClick={() => {
                navigator.clipboard
                  .writeText(resLink && resLink[`${platform}_app_link`])
                  .then(() => {
                    setCopied(true);
                    setTimeout(() => {
                      setCopied(false);
                    }, 2000);
                  });
              }}
            >
              <span>
                {copied
                  ? 'Copied To Clipboard'
                  : resLink && resLink[`${platform}_app_link`]}
              </span>
              <FontAwesomeIcon icon={faCopy} />
            </div>
          </div>
        ) : (
          getData()
        )}
      </div>
    </div>
  );
}

export default GetMobileAppModal;
