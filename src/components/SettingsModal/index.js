import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import classNames from './settingsModal.module.scss';
import { FULL_LOGO } from '../../config';
import arrowGo from '../../static/images/clipIcons/arrowGo.svg';
import { BankContext } from '../../context/Context';
import { ChatContext } from '../../context/ChatContext';
import GetMobileAppModal from '../GetMobileAppModal';

function SettingsModal({
  onClose = () => {},
  onSuccess = () => {},
  logoParam,
}) {
  const { setTabSelected, setChatOn } = useContext(ChatContext);
  const { email, login } = useContext(BankContext);
  const [downloadModal, setDownloadModal] = useState(false);
  const history = useHistory();
  return (
    <>
      {downloadModal ? (
        <GetMobileAppModal
          onClose={() => {
            setDownloadModal(false);
            onClose();
          }}
          onSuccess={() => setDownloadModal(false)}
        />
      ) : (
        <div className={classNames.settingsModal}>
          <div
            className={classNames.overlayClose}
            onClick={() => {
              try {
                onClose();
              } catch (error) {}
            }}
          />
          <div className={classNames.settingsCard}>
            <div className={classNames.inCard}>
              <img
                src={logoParam || FULL_LOGO}
                alt=""
                className={classNames.logo}
              />
              <div className={classNames.settingCards}>
                <div
                  className={classNames.settingCard}
                  onClick={() => {
                    setTabSelected('profile');
                    setChatOn(true);
                    onClose();
                  }}
                >
                  <span>Open Settings</span>
                  <img src={arrowGo} alt="" className={classNames.icon} />
                </div>
                <a
                  href="https://affiliate.app/"
                  target="_blank"
                  className={classNames.settingCard}
                  onClick={() => {}}
                >
                  <span>For Affiliates</span>
                  <img src={arrowGo} alt="" className={classNames.icon} />
                </a>
                <div
                  className={classNames.settingCard}
                  onClick={() => setDownloadModal(true)}
                >
                  <span>Download Apps</span>
                  <img src={arrowGo} alt="" className={classNames.icon} />
                </div>
              </div>
            </div>
            <div className={classNames.footerBtns}>
              <div
                className={classNames.btnSettings}
                onClick={() => {
                  try {
                    if (email) {
                      history.push('/');
                      login();
                      onClose();
                    } else onSuccess();
                  } catch (error) {}
                }}
              >
                <span>{email ? 'Logout' : 'Login'}</span>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default SettingsModal;
