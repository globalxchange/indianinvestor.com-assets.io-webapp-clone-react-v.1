import React, { useState } from 'react';
import FundVault from './VaultFundWithdraw/FundVault';
import DepositCrypto from './DepositCrypto';
import DepositWithdrawActionPanel from '../DepositWithdrawActionPanel/DepositWithdrawActionPanel';
import { useHistory, useParams } from 'react-router-dom';
import { useEffect } from 'react';

function VaultFab() {
  const history = useHistory();
  const { action, type } = useParams();
  const [openModal, setOpenModal] = useState(false);
  const [isDeposit, setisDeposit] = useState(true);
  const [openCrypto, setOpenCrypto] = useState(false);
  const [depositMenuOpen, setDepositMenuOpen] = useState(false);
  useEffect(() => {
    if (action === 'do') {
      setDepositMenuOpen(true);
      if (type === 'deposit') {
        setisDeposit(true);
      } else {
        setisDeposit(false);
      }
      setOpenModal(true);
      history.push('/vault');
    }
  }, [action]);
  return (
    <>
      <div
        className={`fabBtn ${depositMenuOpen}`}
        onClick={() => setDepositMenuOpen(!depositMenuOpen)}
      ></div>
      {depositMenuOpen && !openModal && (
        <DepositWithdrawActionPanel
          setisDeposit={setisDeposit}
          setOpenModal={setOpenModal}
          setDepositMenuOpen={setDepositMenuOpen}
        />
      )}
      <FundVault
        key={`${openModal}`}
        fundOrWithdraw={isDeposit ? 'Deposit' : 'Withdraw'}
        isDeposit={isDeposit}
        openModal={openModal}
        setOpenModal={setOpenModal}
      />
      <DepositCrypto openModal={openCrypto} setOpenModal={setOpenCrypto} />
    </>
  );
}

export default VaultFab;
