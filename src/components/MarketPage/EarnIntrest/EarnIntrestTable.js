import React from 'react';
import { Scrollbars } from 'react-custom-scrollbars';

function EarnIntrestTable() {
  return (
    <Scrollbars autoHide className="table-scroll-wrapper">
      <table>
        <thead className="table-head">
          <tr class="coin-head">
            <th>
              <div className="img-spacer" />
              <h6>Platform</h6>
            </th>
            <th>
              <img
                src="https://loanscan.io/static/media/usdc-icon.8287751b.svg"
                alt=""
              />
              <h6>Platform</h6>
            </th>
            <th>
              <img
                src="https://loanscan.io/static/media/usdc-icon.8287751b.svg"
                alt=""
              />
              <h6>Platform</h6>
            </th>
            <th>
              <img
                src="https://loanscan.io/static/media/usdc-icon.8287751b.svg"
                alt=""
              />
              <h6>Platform</h6>
            </th>
            <th>
              <img
                src="https://loanscan.io/static/media/usdc-icon.8287751b.svg"
                alt=""
              />
              <h6>Platform</h6>
            </th>
            <th>
              <img
                src="https://loanscan.io/static/media/usdc-icon.8287751b.svg"
                alt=""
              />
              <h6>Platform</h6>
            </th>
            <th>
              <img
                src="https://loanscan.io/static/media/usdc-icon.8287751b.svg"
                alt=""
              />
              <h6>Platform</h6>
            </th>
            <th>
              <img
                src="https://loanscan.io/static/media/usdc-icon.8287751b.svg"
                alt=""
              />
              <h6>Platform</h6>
            </th>
            <th>
              <img
                src="https://loanscan.io/static/media/usdc-icon.8287751b.svg"
                alt=""
              />
              <h6>Platform</h6>
            </th>
            <th>
              <img
                src="https://loanscan.io/static/media/usdc-icon.8287751b.svg"
                alt=""
              />
              <h6>Platform</h6>
            </th>
            <th>
              <img
                src="https://loanscan.io/static/media/usdc-icon.8287751b.svg"
                alt=""
              />
              <h6>Platform</h6>
            </th>
          </tr>
          <tr class="price-n-change">
            <th>
              <h5>USD Price</h5>
              <h6>24h change</h6>
            </th>
            <th>
              <h5>$1.00</h5>
              <h6>+0.4%</h6>
            </th>
            <th>
              <h5>$1.00</h5>
              <h6>+0.4%</h6>
            </th>
            <th>
              <h5>$1.00</h5>
              <h6>+0.4%</h6>
            </th>
            <th>
              <h5>$1.00</h5>
              <h6>+0.4%</h6>
            </th>
            <th>
              <h5>$1.00</h5>
              <h6>+0.4%</h6>
            </th>
            <th>
              <h5>$1.00</h5>
              <h6>+0.4%</h6>
            </th>
            <th>
              <h5>$1.00</h5>
              <h6>+0.4%</h6>
            </th>
            <th>
              <h5>$1.00</h5>
              <h6>+0.4%</h6>
            </th>
            <th>
              <h5>$1.00</h5>
              <h6>+0.4%</h6>
            </th>
            <th>
              <h5>$1.00</h5>
              <h6>+0.4%</h6>
            </th>
          </tr>
        </thead>
        <tbody className="table-body">
          <tr className="table-row">
            <td className="property">
              <div className="property-container">
                <img
                  src="https://loanscan.io/static/media/cryptoCom-icon.ae9a4852.svg"
                  alt=""
                />
                <div className="prop-name">Crypto.com</div>
              </div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
          </tr>
          <tr className="table-row">
            <td className="property">
              <div className="property-container">
                <img
                  src="https://loanscan.io/static/media/cryptoCom-icon.ae9a4852.svg"
                  alt=""
                />
                <div className="prop-name">Crypto.com</div>
              </div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
          </tr>
          <tr className="table-row">
            <td className="property">
              <div className="property-container">
                <img
                  src="https://loanscan.io/static/media/cryptoCom-icon.ae9a4852.svg"
                  alt=""
                />
                <div className="prop-name">Crypto.com</div>
              </div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
          </tr>
          <tr className="table-row">
            <td className="property">
              <div className="property-container">
                <img
                  src="https://loanscan.io/static/media/cryptoCom-icon.ae9a4852.svg"
                  alt=""
                />
                <div className="prop-name">Crypto.com</div>
              </div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
          </tr>
          <tr className="table-row">
            <td className="property">
              <div className="property-container">
                <img
                  src="https://loanscan.io/static/media/cryptoCom-icon.ae9a4852.svg"
                  alt=""
                />
                <div className="prop-name">Crypto.com</div>
              </div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
          </tr>
          <tr className="table-row">
            <td className="property">
              <div className="property-container">
                <img
                  src="https://loanscan.io/static/media/cryptoCom-icon.ae9a4852.svg"
                  alt=""
                />
                <div className="prop-name">Crypto.com</div>
              </div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
          </tr>
          <tr className="table-row">
            <td className="property">
              <div className="property-container">
                <img
                  src="https://loanscan.io/static/media/cryptoCom-icon.ae9a4852.svg"
                  alt=""
                />
                <div className="prop-name">Crypto.com</div>
              </div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
          </tr>
          <tr className="table-row">
            <td className="property">
              <div className="property-container">
                <img
                  src="https://loanscan.io/static/media/cryptoCom-icon.ae9a4852.svg"
                  alt=""
                />
                <div className="prop-name">Crypto.com</div>
              </div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
          </tr>
          <tr className="table-row">
            <td className="property">
              <div className="property-container">
                <img
                  src="https://loanscan.io/static/media/cryptoCom-icon.ae9a4852.svg"
                  alt=""
                />
                <div className="prop-name">Crypto.com</div>
              </div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
          </tr>
          <tr className="table-row">
            <td className="property">
              <div className="property-container">
                <img
                  src="https://loanscan.io/static/media/cryptoCom-icon.ae9a4852.svg"
                  alt=""
                />
                <div className="prop-name">Crypto.com</div>
              </div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
          </tr>
          <tr className="table-row">
            <td className="property">
              <div className="property-container">
                <img
                  src="https://loanscan.io/static/media/cryptoCom-icon.ae9a4852.svg"
                  alt=""
                />
                <div className="prop-name">Crypto.com</div>
              </div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
          </tr>
          <tr className="table-row">
            <td className="property">
              <div className="property-container">
                <img
                  src="https://loanscan.io/static/media/cryptoCom-icon.ae9a4852.svg"
                  alt=""
                />
                <div className="prop-name">Crypto.com</div>
              </div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
            <td>
              <div className="prop-value">8.84%</div>
            </td>
          </tr>
        </tbody>
      </table>
    </Scrollbars>
  );
}

export default EarnIntrestTable;
