import React from 'react';

function IceIndices() {
  return (
    <div className="iceIndices">
      <div className="title">Indicies</div>
      <div className="subTitle">Coming Soon</div>
      <p>
        Indicies Will Allow You To Create Your Own Index Funds In A Matter Of
        Seconds. Tap Into The Biggest Trends In The World Without The Risk Of
        Choosing The Right Asset.
      </p>
    </div>
  );
}

export default IceIndices;
