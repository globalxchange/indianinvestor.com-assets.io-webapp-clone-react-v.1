import React from 'react';
import { APP_NAME } from '../../../config/appConfig';

function IceAssets() {
  return (
    <div className="iceIndices">
      <div className="title">Assets</div>
      <div className="subTitle">Coming Soon</div>
      <p>
        {APP_NAME} Has Created The Ice Protocol Which Enables Your To Digitalize
        Any Asset. Once Your Have Turned Your Assets Into Iced Assets, They Can
        Now Be Used As An Instrument On The Platform
      </p>
    </div>
  );
}

export default IceAssets;
