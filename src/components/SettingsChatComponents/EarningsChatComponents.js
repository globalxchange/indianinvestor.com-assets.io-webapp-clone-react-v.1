import { faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Axios from 'axios';
import React, { useCallback, useContext, useEffect, useState } from 'react';
import Scrollbars from 'react-custom-scrollbars';
import { BankContext } from '../../context/Context';

import iceLogo from '../../static/images/logo.svg';
import moneyMarkets from '../../static/images/logoIcons/moneyMarkets.svg';
import LoadingAnim from '../LoadingAnim/LoadingAnim';

function EarningsChatComponents() {
  const { email, token } = useContext(BankContext);
  const [step, setStep] = useState('');
  const [isLiquid, setIsLiquid] = useState(true);
  const [destinations, setDestinations] = useState({});
  const [userApps, setUserApps] = useState([]);
  const [appsObj, setAppsObj] = useState({});
  const [loading, setLoading] = useState(false);

  const getDestinationData = useCallback(() => {
    Axios.get(
      `https://comms.globalxchange.com/coin/iced/get/user/interest/payout/destination?email=${email}`
    ).then(({ data }) => {
      if (data.status) {
        setDestinations(data.destinations);
      }
    });
  }, []);

  const getAppList = useCallback(() => {
    Axios.get(
      `https://comms.globalxchange.com/gxb/apps/registered/user?email=${email}`
    ).then(({ data }) => {
      if (data.status) {
        setUserApps(data.userApps);
        let tempObj = {};
        data.userApps.forEach((app) => {
          tempObj[app.app_code] = app;
        });
        setAppsObj(tempObj);
      }
    });
  }, []);

  function updateDestination(app_code) {
    setLoading(true);
    Axios.post(
      'https://comms.globalxchange.com/coin/iced/set/user/interest/payout/destination',
      {
        token: token,
        email: email,
        icedData: isLiquid
          ? undefined
          : {
              payoutFrequency:
                (destinations.length > 0 &&
                  destinations[destinations.length - 1]?.icedData
                    ?.payoutFrequency) ||
                'daily', // daily, weekly, monthly
              payoutDestination: 'vault',
              payoutDestination_data: {
                app_code,
              },
            },
        liquidData: isLiquid
          ? {
              payoutFrequency:
                (destinations.length > 0 &&
                  destinations[destinations.length - 1]?.liquidData
                    ?.payoutFrequency) ||
                'daily', // daily, weekly, monthly
              payoutDestination: 'vault',
              payoutDestination_data: {
                app_code,
              },
            }
          : undefined,
      }
    ).finally(() => {
      setLoading(false);
      getDestinationData();
      setStep('earningsDetail');
    });
  }

  function updateFrequency(frequency) {
    setLoading(true);
    Axios.post(
      'https://comms.globalxchange.com/coin/iced/set/user/interest/payout/destination',
      {
        token: token,
        email: email,
        icedData: isLiquid
          ? undefined
          : {
              payoutFrequency: frequency,
              payoutDestination: 'vault',
              payoutDestination_data: {
                app_code:
                  destinations.length > 0 &&
                  destinations[destinations.length - 1]?.icedData
                    ?.payoutDestination_data?.app_code,
              },
            },
        liquidData: isLiquid
          ? {
              payoutFrequency: frequency,
              payoutDestination: 'vault',
              payoutDestination_data: {
                app_code:
                  destinations.length > 0 &&
                  destinations[destinations.length - 1]?.liquidData
                    ?.payoutDestination_data?.app_code,
              },
            }
          : undefined,
      }
    ).finally(() => {
      setLoading(false);
      getDestinationData();
      setStep('earningsDetail');
    });
  }

  useEffect(() => {
    getDestinationData();
    getAppList();
  }, []);

  function getContent() {
    switch (step) {
      case 'earningsDetail':
        return (
          <>
            <div className="settingTitle">Earnings</div>
            <div className="subtext">Manage Your Earnings</div>
            <Scrollbars className="settingsList">
              {isLiquid ? (
                <div className="earningDetail">
                  <div className="earningTitle">
                    <img src={moneyMarkets} alt="" className="icn" />
                    <span>MoneyMarket Earnings</span>
                  </div>
                  <div className="earningContent">
                    <div className="item">
                      <div className="label">Frequency</div>
                      <div className="value">
                        {destinations.length > 0 &&
                          destinations[destinations.length - 1]?.liquidData
                            ?.payoutFrequency}
                      </div>
                    </div>
                    <div className="item">
                      <div className="label">Destination</div>
                      <div className="value">
                        <img
                          src={
                            appsObj &&
                            destinations.length > 0 &&
                            destinations[destinations.length - 1]?.liquidData
                              ?.payoutDestination_data?.app_code &&
                            appsObj[
                              destinations.length > 0 &&
                                destinations[destinations.length - 1]
                                  ?.liquidData?.payoutDestination_data?.app_code
                            ]?.app_icon
                          }
                          alt=""
                          className="icon"
                        />
                        <span>
                          {appsObj &&
                            destinations.length > 0 &&
                            destinations[destinations.length - 1]?.liquidData
                              ?.payoutDestination_data?.app_code &&
                            appsObj[
                              destinations.length > 0 &&
                                destinations[destinations.length - 1]
                                  ?.liquidData?.payoutDestination_data?.app_code
                            ].app_name}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                <div className="earningDetail">
                  <div className="earningTitle">
                    <img src={iceLogo} alt="" className="icn" />
                    <span>Bond Earnings</span>
                  </div>
                  <div className="earningContent">
                    <div className="item">
                      <div className="label">Frequency</div>
                      <div className="value">
                        {destinations.length > 0 &&
                          destinations[destinations.length - 1]?.icedData
                            ?.payoutFrequency}
                      </div>
                    </div>
                    <div className="item">
                      <div className="label">Destination</div>
                      <div className="value">
                        <img
                          src={
                            appsObj &&
                            destinations.length > 0 &&
                            destinations[destinations.length - 1]?.icedData
                              ?.payoutDestination_data?.app_code &&
                            appsObj[
                              destinations.length > 0 &&
                                destinations[destinations.length - 1]?.icedData
                                  ?.payoutDestination_data?.app_code
                            ]?.app_icon
                          }
                          alt=""
                          className="icon"
                        />
                        <span>
                          {appsObj &&
                            destinations.length > 0 &&
                            destinations[destinations.length - 1]?.icedData
                              ?.payoutDestination_data?.app_code &&
                            appsObj[
                              destinations.length > 0 &&
                                destinations[destinations.length - 1]?.icedData
                                  ?.payoutDestination_data?.app_code
                            ].app_name}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              )}
              <div
                className="listItem"
                onClick={() => {
                  setStep('editFrequency');
                }}
              >
                <div className="mainText">Edit Frequency</div>
                <div className="subText">Update Frequency Of Earning</div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
              <div
                className="listItem"
                onClick={() => {
                  setStep('editDestination');
                }}
              >
                <div className="mainText">Edit Destination</div>
                <div className="subText">
                  Update Destination The Earning Goes
                </div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
            </Scrollbars>
          </>
        );
      case 'editDestination':
        return (
          <>
            <div className="settingTitle">Earnings</div>
            <div className="subtext">Select The Type Of Destination</div>
            <Scrollbars className="settingsList">
              <div
                className="listItem"
                onClick={() => {
                  setStep('gxAppList');
                }}
              >
                <div className="mainText">Another GX App</div>
                <div className="subText">Another GX App Vault</div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
              <div
                className="listItem disable"
                onClick={() => {
                  setStep('');
                }}
              >
                <div className="mainText">External Wallet</div>
                <div className="subText">External Wallet Coming Soon</div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
            </Scrollbars>
          </>
        );
      case 'editFrequency':
        return (
          <>
            <div className="settingTitle">Earnings</div>
            <div className="subtext">Select The Type Frequency</div>
            <Scrollbars className="settingsList">
              <div
                className="listItem"
                onClick={() => {
                  updateFrequency('daily');
                }}
              >
                <div className="mainText">Daily</div>
                <div className="subText">Add Earnings Daily To Wallet</div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
              <div
                className="listItem"
                onClick={() => {
                  updateFrequency('weekly');
                }}
              >
                <div className="mainText">Weekly</div>
                <div className="subText">Add Earnings Weekly To Wallet</div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
              <div
                className="listItem"
                onClick={() => {
                  updateFrequency('monthly');
                }}
              >
                <div className="mainText">Monthly</div>
                <div className="subText">Add Earnings Monthly To Wallet</div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
            </Scrollbars>
          </>
        );
      case 'gxAppList':
        return (
          <>
            <div className="settingTitle">Earnings</div>
            <div className="subtext">Which Of Your GX App’s?</div>
            <Scrollbars className="settingsList">
              {userApps.map((app) => (
                <div
                  className="listItem app"
                  key={app?.app_code}
                  onClick={() => updateDestination(app?.app_code)}
                >
                  <img src={app?.app_icon} alt="" className="icon" />
                  <span>{app?.app_name}</span>
                </div>
              ))}
            </Scrollbars>
          </>
        );
      default:
        return (
          <>
            <div className="settingTitle">Earnings</div>
            <div className="subtext">Manage Your Earnings</div>
            <Scrollbars className="settingsList">
              <div
                className="listItem"
                onClick={() => {
                  setStep('earningsDetail');
                  setIsLiquid(true);
                }}
              >
                <div className="mainText">Liquid Interest</div>
                <div className="subText">
                  Update First, Last, & Your Nick Name
                </div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
              <div
                className="listItem"
                onClick={() => {
                  setStep('earningsDetail');
                  setIsLiquid(false);
                }}
              >
                <div className="mainText">Bond Earnings</div>
                <div className="subText">
                  Update First, Last, & Your Nick Name
                </div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
            </Scrollbars>
          </>
        );
    }
  }
  return (
    <div className="myAccountContent">
      {getContent()}
      {loading && (
        <div className="loadingAnim">
          <LoadingAnim />
        </div>
      )}
    </div>
  );
}

export default EarningsChatComponents;
