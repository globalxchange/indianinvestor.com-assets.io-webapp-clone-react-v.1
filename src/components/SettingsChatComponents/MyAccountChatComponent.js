import React, { useState } from 'react';
import EarningsChatComponents from './EarningsChatComponents';
import SettingsChatComponents from './SettingsChatComponents';

function MyAccountChatComponent() {
  const [tab, setTab] = useState('Settings');
  function getComponent() {
    switch (tab) {
      case 'Settings':
        return <SettingsChatComponents />;
      case 'Earnings':
        return <EarningsChatComponents />;
      default:
        break;
    }
  }
  return (
    <div className="chatComponentSettings">
      {getComponent()}
      <div className="tabs">
        <div
          className={`tabItm ${tab === 'XID'}`}
          onClick={() => setTab('XID')}
        >
          XID
        </div>
        <div
          className={`tabItm ${tab === 'Settings'}`}
          onClick={() => setTab('Settings')}
        >
          Settings
        </div>
        <div
          className={`tabItm ${tab === 'Earnings'}`}
          onClick={() => setTab('Earnings')}
        >
          Earnings
        </div>
        <div
          className={`tabItm ${tab === 'Brain'}`}
          onClick={() => setTab('Brain')}
        >
          Brain
        </div>
      </div>
    </div>
  );
}

export default MyAccountChatComponent;
