import Axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import OtpInput from 'react-otp-input';
import { BankContext } from '../../context/Context';
import LoadingAnim from '../LoadingAnim/LoadingAnim';

const capRegex = new RegExp(/^.*[A-Z].*/);
const numRegex = new RegExp(/^.*[0-9].*/);
const speRegex = new RegExp(/^.*[!@#$%^&*()+=].*/);

function ResetPasswordComponent({ onClose }) {
  const trueCircle = (
    <svg className="eye" viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg">
      <circle cx="7" cy="7" r="6.5" fill="#002A51" stroke="#2F72AE" />
    </svg>
  );
  const falseCircle = (
    <svg className="eye" viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg">
      <circle cx="7" cy="7" r="7" fill="#BE241A" />
    </svg>
  );
  const { email, tostShowOn } = useContext(BankContext);
  const [pin, setPin] = useState('');
  const [step, setStep] = useState(0);
  const [loading, setLoading] = useState(false);
  const [isValid, setIsValid] = useState({});
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  useEffect(() => {
    setIsValid({
      password:
        capRegex.test(password) &&
        numRegex.test(password) &&
        speRegex.test(password) &&
        password.length >= 8,
      confirmPassword: confirmPassword === password,
      pin: String(pin).length === 6,
    });
  }, [email, password, confirmPassword, pin]);

  function forgotPassword() {
    setLoading(true);
    Axios.post(
      'https://gxauth.apimachine.com/gx/user/password/forgot/request',
      {
        email,
      }
    ).finally(() => setLoading(false));
  }
  function changePassword() {
    setLoading(true);
    Axios.post(
      'https://gxauth.apimachine.com/gx/user/password/forgot/confirm',
      {
        email,
        code: pin,
        newPassword: password,
      }
    )
      .then(({ data }) => {
        if (data.status) {
          setStep(3);
        } else {
          tostShowOn(data.message || 'Something Went Wrong');
        }
      })
      .finally(() => {
        setLoading(false);
      });
  }
  const steps = [
    <>
      <div className="resetTitle">Change Password</div>
      <div className="subTitle">
        Once You Click Initiate Password Change. You Will Recieve A Six Digit
        Code In Your Email Which You Have To Enter To Complete The Password
        Change.
      </div>
      <div
        className="btnInit"
        onClick={() => {
          setStep(1);
          forgotPassword();
        }}
      >
        Initiate
      </div>
    </>,
    <>
      <div className="resetTitle">Enter Code</div>
      <OtpInput
        containerStyle="otp-input-wrapper"
        value={pin}
        onChange={(otp) => setPin(otp)}
        numInputs={6}
        separator={<span> </span>}
        inputStyle="otp-input"
      />
      <div
        className="btnInit"
        onClick={() => {
          setStep(2);
        }}
      >
        Confirm
      </div>
    </>,
    <>
      <div className="resetTitle">Enter New Password</div>
      <div className="password">
        <input
          type="password"
          placeholder="New Password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        {isValid.password ? trueCircle : falseCircle}
      </div>
      <div className="password">
        <input
          type="password"
          placeholder="Confirm Password"
          value={confirmPassword}
          onChange={(e) => setConfirmPassword(e.target.value)}
        />
        {isValid.confirmPassword ? trueCircle : falseCircle}
      </div>
      <div
        className="btnInit"
        onClick={() => {
          if (isValid.password && isValid.confirmPassword) changePassword();
          else tostShowOn('Check Passwords');
        }}
      >
        Confirm
      </div>
    </>,
    <>
      <div className="resetTitle">Congratulations</div>
      <div className="subTitle">
        Your Password Has Been Successfully Updated
      </div>
      <div
        className="btnInit"
        onClick={() => {
          try {
            onClose();
          } catch (error) {}
        }}
      >
        Back To Profile
      </div>
    </>,
  ];
  return (
    <div className="resetPassword">
      {steps[step]}
      {loading && (
        <div className="loading">
          <LoadingAnim />
        </div>
      )}
    </div>
  );
}

export default ResetPasswordComponent;
