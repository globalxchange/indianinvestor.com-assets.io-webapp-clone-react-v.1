import {
  faCheckCircle,
  faCircle,
  faCopy,
} from '@fortawesome/free-regular-svg-icons';
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Axios from 'axios';
import OtpInput from 'react-otp-input';
import QRCode from 'qrcode.react';
import React, { useContext, useState } from 'react';
import Scrollbars from 'react-custom-scrollbars';
import { BankContext } from '../../context/Context';

import twoFaIcon from '../../static/images/clipIcons/twoFa.svg';
import ResetPasswordComponent from './ResetPasswordComponent';
import LoadingAnim from '../LoadingAnim/LoadingAnim';
import { APP_CODE, APP_NAME } from '../../config/appConfig';
import allPlatforms from '../../static/images/allPlatforms.svg';
import EnterPinUnlock from '../EnterPinUnlock/EnterPinUnlock';
import { DISPLAY_CURRENCY } from '../../config';

function SettingsChatComponents({ handleSubmitMessage, setUncover }) {
  const {
    email,
    accessToken,
    setChatOn,
    updateInterval,
    setUpdateInterval,
    defaultPrecission,
    setDefaultPrecission,
    defaultCoin,
    setDefaultCoin,
    coinList,
    setEnterPin,
  } = useContext(BankContext);

  const [step, setStep] = useState('');
  const [mfaStatus, setMfaStatus] = useState(false);
  const [secretCode, setSecretCode] = useState('');
  const [qrCode, setQrCode] = useState('');
  const [pin, setPin] = useState('');
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);

  function getMfaStatus() {
    setLoading(true);
    Axios.post('https://gxauth.apimachine.com/gx/user/mfa/status', {
      email,
    })
      .then(({ data }) => {
        if (data.status) {
          setMfaStatus(data.mfa_enabled);
        }
      })
      .finally(() => setLoading(false));
  }

  function enable2fa() {
    setLoading(true);
    Axios.post('https://gxauth.apimachine.com/gx/user/mfa/set', {
      email,
      accessToken: accessToken,
      client_app: APP_CODE,
    })
      .then(({ data }) => {
        if (data.status) {
          setSecretCode(data.SecretCode);
          setQrCode(data.qrcodeValue);
        }
      })
      .finally(() => setLoading(false));
  }

  function verifyCode() {
    setLoading(true);
    Axios.post('https://gxauth.apimachine.com/gx/user/mfa/set/verify', {
      email: email,
      accessToken: accessToken,
      code: pin,
    })
      .then(({ data }) => {
        if (data.status) {
          setStep('success');
        } else {
          setError(true);
        }
      })
      .finally(() => setLoading(false));
  }

  function disable2fa() {
    setLoading(true);
    Axios.post('https://gxauth.apimachine.com/gx/user/mfa/disable', {
      email: email,
      accessToken: accessToken,
    })
      .then(({ data }) => {
        if (data.status) {
          setStep('2faDisabled');
        } else {
          setStep('2faDisableFail');
        }
      })
      .finally(() => setLoading(false));
  }

  function getContent() {
    switch (step) {
      case '2fa':
        return (
          <>
            <div className="settingTitle">Settings</div>
            <div className="subtext">Settings -> Configure 2FA</div>
            <Scrollbars className="settingsList">
              {mfaStatus ? (
                <div
                  className="listItem"
                  onClick={() => {
                    setStep('disable2fa');
                  }}
                >
                  <div className="mainText">Disable 2FA</div>
                  <div className="subText">Google Authenticator</div>
                  <FontAwesomeIcon icon={faAngleRight} className="angle" />
                </div>
              ) : (
                <div
                  className="listItem"
                  onClick={() => {
                    enable2fa();
                    setStep('enable2fa');
                  }}
                >
                  <div className="mainText">Enable 2FA</div>
                  <div className="subText">Google Authenticator</div>
                  <FontAwesomeIcon icon={faAngleRight} className="angle" />
                </div>
              )}
            </Scrollbars>
          </>
        );
      case 'enable2fa':
        return (
          <>
            <div className="settingTitle">Settings</div>
            <div className="subtext">Settings -> Configure 2FA</div>
            <Scrollbars className="settingsList">
              <div className="listItem" onClick={() => setStep('scanQr')}>
                <div className="mainText">Scan QR Code</div>
                <div className="subText">Using Google Authenticator App</div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
              <div className="listItem" onClick={() => setStep('copyCode')}>
                <div className="mainText">Use Key</div>
                <div className="subText">
                  Enter Into Google Authenticator App
                </div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
            </Scrollbars>
          </>
        );
      case 'scanQr':
        return (
          <div className="twofa">
            <div className="twofaTitle">
              <img src={twoFaIcon} alt="" className="icon" />
              <span>Scan This QR Code</span>
            </div>
            {qrCode ? (
              <QRCode
                className="qr-svg"
                value={qrCode}
                renderAs="svg"
                size={300}
              />
            ) : (
              ''
            )}
            <div className="btns">
              <div className="btnGoBack">Go Back</div>
              <div className="imDone" onClick={() => setStep('verification')}>
                Im Done
              </div>
            </div>
          </div>
        );
      case 'copyCode':
        return (
          <div className="twofa">
            <div className="twofaTitle">
              <img src={twoFaIcon} alt="" className="icon" />
              <span>Enter This Key</span>
            </div>
            <div className="inpBox">
              <div className="textVal">{secretCode}</div>
              <div className="btCopy">
                <FontAwesomeIcon icon={faCopy} className="copy" />
              </div>
            </div>
            <div className="btns">
              <div className="btnGoBack">Go Back</div>
              <div className="imDone" onClick={() => setStep('verification')}>
                Im Done
              </div>
            </div>
          </div>
        );
      case 'verification':
        return (
          <div className="twofa">
            <div className="texts">
              <div className="twofaTitle">
                <img src={twoFaIcon} alt="" className="icon" />
                <span>Verification</span>
              </div>
              <div className="twofaSubtitle">
                {error
                  ? 'You Have Entered The Wrong Verification Code. Please Try Again'
                  : 'Enter The One Time Code Form Your Google Authenticator App'}
              </div>
            </div>
            <OtpInput
              containerStyle="otp-input-wrapper"
              value={pin}
              onChange={(otp) => {
                setPin(otp);
                setError(false);
              }}
              numInputs={6}
              separator={<span> </span>}
              inputStyle="otp-input"
            />
            <div className="btns">
              <div className="btnGoBack">Go Back</div>
              <div className="imDone" onClick={() => verifyCode()}>
                Im Done
              </div>
            </div>
          </div>
        );
      case 'success':
        return (
          <div className="twofa">
            <div className="texts">
              <div className="twofaTitle">
                <span>Success</span>
              </div>
              <div className="twofaSubtitle">
                You Have Successfully Enabled 2FA For This Account
              </div>
            </div>
            <div className="btns">
              <div className="btnGoBack" onClick={() => setStep('')}>
                Settings
              </div>
              <div className="imDone" onClick={() => setChatOn(false)}>
                Home
              </div>
            </div>
          </div>
        );
      case 'disable2fa':
        return (
          <div className="twofa">
            <div className="texts">
              <div className="twofaTitle">
                <span>Disable 2FA</span>
              </div>
              <div className="twofaSubtitle">
                Are You Sure You Want To Remove 2FA For This Account. This May
                Expose Your Accounts To Higher Levels Of Risk
              </div>
            </div>
            <div className="btns">
              <div className="btnGoBack" onClick={() => setStep('')}>
                Go Back
              </div>
              <div className="imDone" onClick={() => disable2fa()}>
                Disable
              </div>
            </div>
          </div>
        );
      case '2faDisabled':
        return (
          <div className="twofa">
            <div className="texts">
              <div className="twofaTitle">
                <span>Success</span>
              </div>
              <div className="twofaSubtitle">
                You Have Successfully Disabled 2FA For This Account
              </div>
            </div>
            <div className="btns">
              <div className="btnGoBack" onClick={() => setStep('')}>
                Settings
              </div>
              <div className="imDone" onClick={() => setChatOn(false)}>
                Home
              </div>
            </div>
          </div>
        );
      case '2faDisableFail':
        return (
          <div className="twofa">
            <div className="texts">
              <div className="twofaTitle">
                <span>Unsuccessful</span>
              </div>
              <div className="twofaSubtitle">
                We Are Unable To Disable Your 2FA At This TIme. Please Contact
                Support Or Try Again Later
              </div>
            </div>
            <div className="btns">
              <div
                className="btnGoBack"
                onClick={() => {
                  handleSubmitMessage(
                    'I Need To Disable My 2FA. And The System Is Not Allowing Me',
                    () => {}
                  );
                  setUncover(true);
                }}
              >
                Support
              </div>
              <div className="imDone" onClick={() => setChatOn(false)}>
                Home
              </div>
            </div>
          </div>
        );
      case 'changePassword':
        return <ResetPasswordComponent onClose={() => setStep('')} />;
      case 'pref':
        return (
          <>
            <div className="settingTitle">Settings</div>
            <div className="subtext">
              <span onClick={() => setStep('')}>Settings</span> -&gt;
              Preferences
            </div>
            <Scrollbars className="settingsList">
              <div className="listItem" onClick={() => setStep('prefRefresh')}>
                <div className="mainText">Refresh Rate</div>
                <div className="subText">
                  Change Frequency Of Values Updating
                </div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
              <div className="listItem" onClick={() => setStep('prefDecimal')}>
                <div className="mainText">Decimal Places</div>
                <div className="subText">Precision Decimal Places To Show</div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
              <div className="listItem" onClick={() => setStep('prefCurrency')}>
                <div className="mainText">Change Default Currency</div>
                <div className="subText">Change Default Viewing Currency</div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
              <div className="listItem" onClick={() => setEnterPin(true)}>
                <div className="mainText">Admin</div>
                <div className="subText">Go To Admin Mode</div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
            </Scrollbars>
          </>
        );
      case 'prefRefresh':
        return (
          <>
            <div className="settingTitle">Settings</div>
            <div className="subtext">
              <span onClick={() => setStep('')}>Settings</span> -&gt;
              <span onClick={() => setStep('pref')}>Preferences</span> -&gt;
              Refresh Rate
            </div>
            <Scrollbars className="settingsList">
              <div className="listItem" onClick={() => setUpdateInterval(0)}>
                <div className="mainText">Manual</div>
                <FontAwesomeIcon
                  icon={updateInterval === 0 ? faCheckCircle : faCircle}
                  className="angle"
                />
              </div>
              <div className="listItem" onClick={() => setUpdateInterval(5)}>
                <div className="mainText">5 Seconds</div>
                <FontAwesomeIcon
                  icon={updateInterval === 5 ? faCheckCircle : faCircle}
                  className="angle"
                />
              </div>
              <div className="listItem" onClick={() => setUpdateInterval(10)}>
                <div className="mainText">10 Seconds</div>
                <FontAwesomeIcon
                  icon={updateInterval === 10 ? faCheckCircle : faCircle}
                  className="angle"
                />
              </div>
              <div className="listItem" onClick={() => setUpdateInterval(15)}>
                <div className="mainText">15 Seconds</div>
                <FontAwesomeIcon
                  icon={updateInterval === 15 ? faCheckCircle : faCircle}
                  className="angle"
                />
              </div>
            </Scrollbars>
          </>
        );
      case 'prefDecimal':
        return (
          <>
            <div className="settingTitle">Settings</div>
            <div className="subtext">
              <span onClick={() => setStep('')}>Settings</span> -&gt;
              <span onClick={() => setStep('pref')}>Preferences</span> -&gt;
              Decimal Places
            </div>
            <Scrollbars className="settingsList">
              <div className="listItem" onClick={() => setDefaultPrecission(0)}>
                <div className="mainText">Default</div>
                <FontAwesomeIcon
                  icon={defaultPrecission === 0 ? faCheckCircle : faCircle}
                  className="angle"
                />
              </div>
              <div className="listItem" onClick={() => setDefaultPrecission(5)}>
                <div className="mainText">Max 5 Places After Decimal</div>
                <FontAwesomeIcon
                  icon={defaultPrecission === 5 ? faCheckCircle : faCircle}
                  className="angle"
                />
              </div>
              <div className="listItem" onClick={() => setDefaultPrecission(7)}>
                <div className="mainText">Max 7 Places After Decimal</div>
                <FontAwesomeIcon
                  icon={defaultPrecission === 7 ? faCheckCircle : faCircle}
                  className="angle"
                />
              </div>
              <div className="listItem" onClick={() => setDefaultPrecission(9)}>
                <div className="mainText">Max 9 Places After Decimal</div>
                <FontAwesomeIcon
                  icon={defaultPrecission === 9 ? faCheckCircle : faCircle}
                  className="angle"
                />
              </div>
            </Scrollbars>
          </>
        );
      case 'prefCurrency':
        return (
          <>
            <div className="settingTitle">Settings</div>
            <div className="subtext">
              <span onClick={() => setStep('')}>Settings</span> -&gt;
              <span onClick={() => setStep('pref')}>Preferences</span> -&gt;
              Change Default Currency
            </div>
            <Scrollbars className="settingsList">
              <div
                className="listItem"
                onClick={() =>
                  setDefaultCoin({
                    coin: null,
                    name: 'Default Coin',
                    img: allPlatforms,
                  })
                }
              >
                <div className="d-flex align-items-center">
                  <img className="icon" src={allPlatforms} alt="" />
                  <div className="mainText">Default Currency</div>
                </div>
                <FontAwesomeIcon
                  icon={defaultCoin.coin === null ? faCheckCircle : faCircle}
                  className="angle"
                />
              </div>
              {coinList
                .filter(
                  (coin) =>
                    coin.coinSymbol === 'USD' ||
                    coin.coinSymbol === DISPLAY_CURRENCY
                )
                .map((coin) => (
                  <div
                    key={coin.coinName}
                    className="listItem"
                    onClick={() =>
                      setDefaultCoin({
                        coin: coin.coinSymbol,
                        name: coin.coinName,
                        img: coin.coinImage,
                      })
                    }
                  >
                    <div className="d-flex align-items-center">
                      <img className="icon" src={coin.coinImage} alt="" />
                      <div className="mainText">{coin.coinName}</div>
                    </div>
                    <FontAwesomeIcon
                      icon={
                        defaultCoin.coin === coin.coinSymbol
                          ? faCheckCircle
                          : faCircle
                      }
                      className="angle"
                    />
                  </div>
                ))}
            </Scrollbars>
          </>
        );
      default:
        return (
          <>
            <div className="settingTitle">Settings</div>
            <div className="subtext">
              Select The Information You Want To Update
            </div>
            <Scrollbars className="settingsList">
              <div className="listItem">
                <div className="mainText">Change Name</div>
                <div className="subText">
                  Update First, Last, & Your Nick Name
                </div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
              <div
                className="listItem"
                onClick={() => setStep('changePassword')}
              >
                <div className="mainText">Change Password</div>
                <div className="subText">Modify Your Currency Password</div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
              <div
                className="listItem"
                onClick={() => {
                  setStep('2fa');
                  getMfaStatus();
                }}
              >
                <div className="mainText">Configure 2FA</div>
                <div className="subText">
                  Enable Or Disable 2FA Login Authentication
                </div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
              <div
                className="listItem"
                onClick={() => {
                  setStep('pref');
                }}
              >
                <div className="mainText">Preferences</div>
                <div className="subText">Edit The View Settings</div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
              <div className="listItem disable">
                <div className="mainText">Update Address</div>
                <div className="subText">Change Your Home Address</div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
              <div className="listItem disable">
                <div className="mainText">Images And Bio</div>
                <div className="subText">Update Your {APP_NAME} Profile</div>
                <FontAwesomeIcon icon={faAngleRight} className="angle" />
              </div>
            </Scrollbars>
          </>
        );
    }
  }
  return (
    <>
      <div className="myAccountContent">
        {getContent()}
        {loading && (
          <div className="loading">
            <LoadingAnim />
          </div>
        )}
      </div>
    </>
  );
}

export default SettingsChatComponents;
