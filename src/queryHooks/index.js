import axios from 'axios';
import { useQuery } from 'react-query';
import CryptoJS from 'crypto-js';
import { APP_CODE, COUNTRY, DISPLAY_CURRENCY } from '../config';

const getUserApps = async ({ queryKey }) => {
  const [_key, email] = queryKey;
  const { data } = await axios.get(
    `https://comms.globalxchange.com/gxb/apps/registered/user?email=${email}`
  );
  return data.userApps;
};

export const useUserApps = (email) => {
  const query = useQuery(['userApps', email], getUserApps);
  return query;
};

const getUserVaults = async ({ queryKey }) => {
  const [_key, { email, type, appCode, getAllCoins, order }] = queryKey;
  const { data } = await axios.post(
    'https://comms.globalxchange.com/coin/vault/service/coins/get',
    {
      app_code: appCode,
      email: email,
      type: type,
      displayCurrency: DISPLAY_CURRENCY,
      post_app_prices: true,
      getAllCoins,
      orderby_dsc: order,
    }
  );
  return data.coins_data;
};

export const useUserVaults = (
  email,
  type,
  appCode,
  getAllCoins = false,
  order = false
) => {
  const query = useQuery(
    ['userVaults', { email, type, appCode, getAllCoins, order }],
    getUserVaults
  );
  return query;
};

const getVaultTxns = async ({ queryKey }) => {
  const [_key, { email, appCode, coin, profileId }] = queryKey;
  const { data } = await axios.post(
    'https://comms.globalxchange.com/coin/vault/service/txns/get',
    {
      app_code: appCode,
      email: email,
      coin: coin,
      profile_id: profileId,
      displayCurrency: DISPLAY_CURRENCY,
    }
  );
  return data.txns || [];
};

export const useVaultTxns = (email, appCode, coin, profileId) => {
  const query = useQuery(
    ['vaultTxns', { email, appCode, coin, profileId }],
    getVaultTxns
  );
  return query;
};

const getVaultTxnsDetail = async ({ queryKey }) => {
  const [_key, { identifier, email, deposit }] = queryKey;
  const { data } = await axios.get(
    `https://comms.globalxchange.com/coin/vault/service/get/complete/txn?identifier=${identifier}`
  );
  const temp = data.txns.filter(
    (txn) => txn[deposit ? 'toUser' : 'fromUser']?.email === email
  );
  console.log(`data.txns`, data.txns, temp);

  return temp[0] || data.txns[0];
};

export const useVaultTxnDetails = (identifier, email, deposit) => {
  const query = useQuery(
    ['vaultTxnsDetail', { identifier, email, deposit }],
    getVaultTxnsDetail
  );
  return query;
};

const getUserDetails = async ({ queryKey }) => {
  const [_key, email] = queryKey;
  const { data } = await axios.post(
    'https://comms.globalxchange.com/get_affiliate_data_no_logs',
    {
      email: email,
    }
  );
  return data?.[0];
};

export const useUserDetails = (email) => {
  const query = useQuery(['getUserDetails', email], getUserDetails);
  return query;
};

const getUserBondsList = async ({ queryKey }) => {
  const [_key, email] = queryKey;
  const { data } = await axios.get(
    `https://comms.globalxchange.com/coin/iced/interest/balances/get?app_code=ice&displayCurrency=${DISPLAY_CURRENCY}&email=${email}&with_balances=true`
  );
  return data?.result[0];
};

export const useUserBondsList = (email) => {
  const query = useQuery(['getUserBondsList', email], getUserBondsList);
  return query;
};

const getUserBondsTxns = async ({ queryKey }) => {
  const [_key, { email, coin }] = queryKey;
  const { data } = await axios.get(
    `https://comms.globalxchange.com/coin/iced/interest/logs/get?email=${email}&coin=${coin}`
  );
  return data?.interestLogs;
};

export const useUserBondsTxns = (email, coin) => {
  const query = useQuery(
    ['getUserBondsTxns', { email, coin }],
    getUserBondsTxns
  );
  return query;
};

const getUserMoneMarketsList = async ({ queryKey }) => {
  const [_key, { email, appCode }] = queryKey;
  const { data } = await axios.get(
    `https://comms.globalxchange.com/coin/vault/service/user/app/interest/balances/get?app_code=${appCode}&email=${email}&displayCurrency=${DISPLAY_CURRENCY}&with_balances=true`
  );
  return data?.result[0]?.balances[0]?.liquid_balances;
};

export const useUserMoneMarketsList = (email, appCode) => {
  const query = useQuery(
    ['getUserMoneMarketsList', { email, appCode }],
    getUserMoneMarketsList
  );
  return query;
};

const getUserMoneyMarketsTxns = async ({ queryKey }) => {
  const [_key, { email, coin, appCode }] = queryKey;
  const { data } = await axios.get(
    `https://comms.globalxchange.com/coin/vault/service/user/app/interest/logs/get?email=${email}&app_code=${appCode}&coin=${coin}`
  );
  return data?.logs[0]?.logs;
};

export const useUserMoneyMarketsTxns = (email, coin, appCode) => {
  const query = useQuery(
    ['getUserMoneyMarketsTxns', { email, coin, appCode }],
    getUserMoneyMarketsTxns
  );
  return query;
};

const getMarketCoinsList = async ({ queryKey }) => {
  const [_key, type] = queryKey;
  const { data } = await axios.post(
    'https://comms.globalxchange.com/coin/vault/service/coins/get',
    {
      app_code: 'indianinvestor',
      type,
      displayCurrency: DISPLAY_CURRENCY,
    }
  );
  return data?.coins_data;
};

export const useMarketCoinsList = (type) => {
  const query = useQuery(['getMarketCoinsList', type], getMarketCoinsList);
  return query;
};

const getShareTokensList = async () => {
  const { data } = await axios.get(
    `https://comms.globalxchange.com/coin/investment/path/get?country=${COUNTRY}&investmentType=EQT`
  );
  return data?.paths;
};

export const useShareTokensList = () => {
  const query = useQuery(['getShareTokens'], getShareTokensList);
  return query;
};

const getShareTokensDetail = async ({ queryKey }) => {
  const [_key, symbol] = queryKey;
  const { data } = await axios.get(
    `https://comms.globalxchange.com/coin/investment/path/get?token=${symbol}`
  );
  return data.paths[0];
};

export const useShareTokensDetail = (symbol) => {
  const query = useQuery(
    ['getShareTokensDetail', symbol],
    getShareTokensDetail
  );
  return query;
};

const getAppDetail = async ({ queryKey }) => {
  const [_key, appCode] = queryKey;
  const { data } = await axios.get(
    `https://comms.globalxchange.com/gxb/apps/get?app_code=${appCode}`
  );
  return data.apps[0];
};

export const useAppDetail = (appCode) => {
  const query = useQuery(['getAppDetail', appCode], getAppDetail);
  return query;
};

const getTokenDetailByStatus = async ({ queryKey }) => {
  const [_key, { coin, status, type }] = queryKey;
  const { data } = await axios.get(
    `https://comms.globalxchange.com/coin/investment/path/tokens/resale/order/get?token=${coin}&status=${status}${
      type ? '&type=' + type : ''
    }`
  );
  return data;
};

export const useTokenDetailByStatus = (coin, status, type) => {
  const query = useQuery(
    ['getTokenDetailByStatus', { coin, status, type }],
    getTokenDetailByStatus
  );
  return query;
};

const getSingleCoinBalance = async ({ queryKey }) => {
  const [_key, { appCode, email, investmentCoin, coin }] = queryKey;
  const { data } = await axios.post(
    'https://comms.globalxchange.com/coin/vault/service/coins/get',
    {
      app_code: appCode,
      email: email,
      investmentCoin: investmentCoin,
      include_coins: [coin],
      displayCurrency: DISPLAY_CURRENCY,
    }
  );

  return data?.coins_data && data?.coins_data[0];
};

export const useSingleCoinBalance = (appCode, email, investmentCoin, coin) => {
  const query = useQuery(
    ['getSingleCoinBalance', { appCode, email, investmentCoin, coin }],
    getSingleCoinBalance
  );
  return query;
};

const getCustomBondsList = async ({ queryKey }) => {
  const [_key, { params }] = queryKey;
  const { data } = await axios.post(
    'https://comms.globalxchange.com/coin/iced/banker/custom/bond/get',
    params
  );

  return data;
};

export const useCustomBondsList = (params) => {
  const query = useQuery(
    ['getCustomBondsList', { params }],
    getCustomBondsList
  );
  return query;
};

const getBankerList = async () => {
  const { data } = await axios.get(
    'https://teller2.apimachine.com/admin/allBankers'
  );

  return data?.data;
};

export const useBankerList = () => {
  const query = useQuery(['getBankerList'], getBankerList);
  return query;
};

const getMMList = async () => {
  const { data } = await axios.get(
    `https://comms.globalxchange.com/coin/iced/get/liquid/interest?app_code=${APP_CODE}&getChangeData=true&displayCurrency=${DISPLAY_CURRENCY}`
  );

  return data?.interest_rates;
};

export const useMMList = () => {
  const query = useQuery(['getMMList'], getMMList);
  return query;
};

const getShareTokensVaultsList = async ({ queryKey }) => {
  const [_key, { email, appCode }] = queryKey;
  const { data } = await axios.get(
    `https://comms.globalxchange.com/coin/investment/path/user/vault/balances/get?email=${email}&investmentType=EQT${
      appCode ? '&app_code=' + appCode : ''
    }`
  );
  return data;
};

export const useShareTokensVaultsList = (email, appCode) => {
  const query = useQuery(
    ['getShareTokensVaultsList', { email, appCode }],
    getShareTokensVaultsList
  );
  return query;
};

const getFXSendMethods = async ({ queryKey }) => {
  const [_key, { toCoin, fromCoin, type, paymentMethod }] = queryKey;
  const { data } = await axios.get(
    `https://comms.globalxchange.com/coin/vault/service/payment/stats/get?select_type=${type}&to_currency=${toCoin}&from_currency=${fromCoin}&banker=shorupan@indianotc.com${
      paymentMethod ? '&paymentMethod=' + paymentMethod : ''
    }`
  );
  return data?.pathData;
};

export const useFXSendMethods = (
  toCoin,
  fromCoin,
  type = 'fund',
  paymentMethod
) => {
  const query = useQuery(
    ['getFXSendMethods', { toCoin, fromCoin, type, paymentMethod }],
    getFXSendMethods
  );
  return query;
};

const getRegisterdUsers = async () => {
  const { data } = await axios.get(
    'https://comms.globalxchange.com/listUsernames'
  );
  if (data.status) {
    let bytes = CryptoJS.Rabbit.decrypt(data.payload, 'gmBuuQ6Er8XqYBd');
    let jsonString = bytes.toString(CryptoJS.enc.Utf8);
    let result_obj = JSON.parse(jsonString);
    return result_obj;
  }
};

export const useRegisterdUsers = () =>
  useQuery(['getRegisterdUsers'], getRegisterdUsers);
