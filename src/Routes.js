import React, { useContext } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Vault from './pages/Vault';
import Portfolio from './pages/Portfolio';
import MobileApps from './pages/MobileApps';
import IcedIndex from './pages/IcedIndex';
import { BankContext } from './context/Context';
import Earnings from './pages/Earnings';
import NetWorthPage from './pages/NetWorthPage';
import IceMechineMobile from './pages/IceMechineMobile';
import MoneyMarkets from './pages/MoneyMarkets';
import BondOverview from './pages/BondOverview';
import Options from './pages/Options';
import DesktopApps from './pages/DesktopApps';
import DesktopAppDetail from './pages/DesktopAppDetail';
import WhatIsAssets from './pages/WhatIsAssets';
import WhatIsAssetsIoPlayList from './pages/WhatIsAssetsIoPlayList';
import AdminLogin from './pages/AdminLogin';
import InvestmentVaults from './pages/InvestmentVaults';
import GetStartedPage from './pages/GetStartedPage';
import HomePage from './pages/HomePage';
import VaultsPage from './pages/VaultsPage';
import MetaverseAppsPage from './pages/MetaverseAppsPage';
import RetiredAppsPage from './pages/RetiredAppsPage';
import TradeStreamPage from './pages/TradeStreamPage';
import TaxChainsPage from './pages/TaxChainsPage';
import MarketsPage from './pages/MarketsPage';
import InvestorPage from './pages/InvestorPage';
import PreRegisterMobile from './pages/PreRegisterMobile';
import AboutPage from './pages/AboutPage';
import AboutBusinessPage from './pages/AboutBusinessPage';
import PreRegBusinessPage from './pages/PreRegBusinessPage';

function Routes() {
  const { email, refreshPage } = useContext(BankContext);
  return (
    <Switch>
      <Route
        exact
        path="/iceAsset"
        component={Portfolio}
        key={refreshPage === 'portfolio'.toString()}
      />
      <Route exact path="/investors/about" component={AboutPage} />
      <Route
        exact
        path="/businesses/about/startups"
        component={AboutBusinessPage}
      />
      <Route
        exact
        path="/businesses/pre-register"
        component={PreRegBusinessPage}
      />
      <Route
        exact
        path="/iceAssetMobile/:coin"
        component={IceMechineMobile}
        key={refreshPage === 'iceMecine'.toString()}
      />
      <Route
        exact
        path="/net-worth"
        component={NetWorthPage}
        key={refreshPage === 'networth'.toString()}
      />
      <Route
        exact
        path="/net-worth/:assetClass/:assetCoin/:liquidity"
        component={NetWorthPage}
        key={refreshPage === 'networth'.toString()}
      />
      {/* <Route exact path="/login" component={Login} /> */}
      <Route
        exact
        path="/assets/:coinParam"
        key={refreshPage === 'assets'.toString()}
      >
        <IcedIndex />
      </Route>
      <Route
        exact
        path="/legacymarkets"
        key={refreshPage === 'index'.toString()}
      >
        <IcedIndex />
      </Route>
      <Route exact path="/market/:assetClassName" component={MarketsPage} />
      <Route
        exact
        path="/market/:assetClassName/:coin"
        component={MarketsPage}
      />
      <Route exact path="/market" component={MarketsPage} />
      <Route exact path="/investors" component={InvestorPage} />
      <Route
        exact
        path="/legacymarkets/:marketTab"
        key={refreshPage === 'bridge'.toString()}
      >
        <IcedIndex />
      </Route>
      <Route
        exact
        path="/legacymarkets/:marketTab/:coinSymbol"
        key={refreshPage === 'bridge'.toString()}
      >
        <IcedIndex />
      </Route>
      <Route exact path="/bonds" key={refreshPage === 'bonds'.toString()}>
        <IcedIndex activeTab={'bonds'} />
      </Route>
      <Route
        exact
        path="/globalPayments"
        key={refreshPage === 'globalPayments'.toString()}
      >
        <IcedIndex activeTab={'globalPayments'} />
      </Route>
      <Route exact path="/" key={refreshPage === 'home'.toString()}>
        <Redirect to="/investors/about" />
      </Route>
      {email ? (
        <Route
          exact
          path="/app"
          key={refreshPage === 'networth'.toString()}
          component={NetWorthPage}
        />
      ) : (
        <Route
          exact
          path="/app"
          key={refreshPage === 'index'.toString()}
          component={IcedIndex}
        />
      )}
      {email ? (
        <Route
          exact
          path="/app/:assetClass/:assetCoin/:liquidity"
          key={refreshPage === 'networth'.toString()}
          component={NetWorthPage}
        />
      ) : (
        <Route
          exact
          path="/app/:assetClass/:assetCoin/:liquidity"
          key={refreshPage === 'index'.toString()}
          component={IcedIndex}
        />
      )}
      <Route
        exact
        path="/Terminal"
        component={Options}
        key={refreshPage === 'Terminal'.toString()}
      />
      <Route
        exact
        path="/Terminal/:from"
        component={Options}
        key={refreshPage === 'Terminal'.toString()}
      />
      <Route
        exact
        path="/Classrooms"
        component={WhatIsAssets}
        key={refreshPage === 'Classrooms'.toString()}
      />
      <Route
        exact
        path="/tradestream"
        component={TradeStreamPage}
        key={refreshPage === 'tradestream'.toString()}
      />
      <Route
        exact
        path="/taxchains"
        component={TaxChainsPage}
        key={refreshPage === 'taxchains'.toString()}
      />
      <Route
        exact
        path="/Classrooms/video/:videoId"
        component={WhatIsAssets}
        key={refreshPage === 'Classrooms'.toString()}
      />
      <Route
        exact
        path="/Classrooms/:any"
        component={WhatIsAssetsIoPlayList}
        key={refreshPage === 'Classrooms'.toString()}
      />
      <Route
        exact
        path="/metaverse"
        component={MetaverseAppsPage}
        key={refreshPage === 'metaverse'.toString()}
      />
      <Route
        exact
        path="/retired"
        component={RetiredAppsPage}
        key={refreshPage === 'retired'.toString()}
      />
      <Route
        exact
        path="/vault/:assetClassParam?/:appCodeParam?/:coinParam?"
        component={VaultsPage}
        key={refreshPage === 'vaults'.toString()}
      />
      <Route
        exact
        path="/investmentVault"
        component={InvestmentVaults}
        key={refreshPage === 'investmentVault'.toString()}
      />
      <Route
        exact
        path="/earning"
        component={Earnings}
        key={refreshPage === 'earn'.toString()}
      />
      <Route
        exact
        path="/vault/:type/:action"
        component={Vault}
        key={refreshPage === 'vaults'.toString()}
      />
      <Route
        exact
        path="/mobile-apps"
        component={MobileApps}
        key={refreshPage === 'mobileApps'.toString()}
      />
      <Route
        exact
        path="/desktopapps"
        component={DesktopApps}
        key={refreshPage === 'desktopApps'.toString()}
      />
      <Route
        exact
        path="/desktopapps/:os"
        component={DesktopAppDetail}
        key={refreshPage === 'desktopApps'.toString()}
      />
      <Route
        exact
        path="/moneyMarkets"
        component={MoneyMarkets}
        key={refreshPage === 'moneyMarkets'.toString()}
      />
      <Route exact path="/pre-register" component={PreRegisterMobile} />
      <Route exact path="/signup" component={GetStartedPage} />
      <Route exact path="/login" component={GetStartedPage} />
      <Route exact path="/reset" component={GetStartedPage} />
      <Route
        exact
        path="/bonds/:contractId"
        component={BondOverview}
        key={refreshPage === 'moneyMarkets'.toString()}
      />
      <Route
        exact
        path="/adminLogin/:email/:token/:userEmail"
        component={AdminLogin}
      />
    </Switch>
  );
}

export default Routes;
