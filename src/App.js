import React, { useContext, Fragment } from 'react';
import { BrowserRouter } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-loading-skeleton/dist/skeleton.css';

import Routes from './Routes';
import PortfolioContextProvider from './context/PortfolioContext';
import NetWorthContextProvider from './context/NetWorthContext';
import { BankContext } from './context/Context';
import ChatContextProvider from './context/ChatContext';
import DefiContextProvider from './context/DefiContext';
import VideoPlayer from './components/VideoPlayer/VideoPlayer';
import ChatsIoContextProvider from './context/ChatsIoContext';
import SupportChatContextProvider from './context/SupportChatContext';
import useWindowDimensions from './utils/WindowSize';
import GetMobileAppPage from './pages/GetMobileAppPage';
import InvestmentVaultContextProvider from './context/InvestmentVaultContext';
import { QueryClient, QueryClientProvider } from 'react-query';

function App() {
  const { refreshPage } = useContext(BankContext);
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        refetchOnWindowFocus: true,
      },
    },
  });
  return (
    <QueryClientProvider client={queryClient}>
      <PortfolioContextProvider key={refreshPage === 'portfolio'.toString()}>
        <NetWorthContextProvider key={refreshPage === 'networth'.toString()}>
          <ChatContextProvider>
            <ChatsIoContextProvider>
              <SupportChatContextProvider>
                <DefiContextProvider>
                  <InvestmentVaultContextProvider>
                    <BrowserRouter>
                      <Routes />
                    </BrowserRouter>
                  </InvestmentVaultContextProvider>
                </DefiContextProvider>
              </SupportChatContextProvider>
            </ChatsIoContextProvider>
          </ChatContextProvider>
        </NetWorthContextProvider>
      </PortfolioContextProvider>
    </QueryClientProvider>
  );
}

export default App;
