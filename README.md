**Folder Structuure**

├── Layout<br />
│ └── Layouts Used For Pages<br />
├── components<br />
│ └── Components Used In The App<br />
├── config<br />
│ └── Values For the apps and icons<br />
├── context<br />
│ └── Contexts For States & refs<br />
├── pages<br />
│ └── Individual Pages On Routes<br />
├── queryHooks<br />
│ └── Queries Saved As hooks to get vaules<br />
├── static<br />
│ ├── animations<br />
│ │ └── Animation json & gifs<br />
│ ├── images<br />
│ │ └── Image files<br />
│ └── scss<br />
│ └── style fiiles and mixins<br />
└── utils<br />
└── Js utility functions used<br />
